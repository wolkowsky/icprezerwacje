﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace TaskProcessor.TestXLApi
{
    class Logger
    {

        public static void LogMessage(string msg)
        {
            var dt = DateTime.Now;

            Console.WriteLine("[{0} {1}] {2}", dt.ToShortDateString(), dt.ToString("HH:mm:ss", CultureInfo.InvariantCulture), msg);

            string logDir = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), "Logs"); // albo z konfiguracji...

            if (!Directory.Exists(logDir))
            {
                try
                {
                    Directory.CreateDirectory(logDir);
                }
                catch
                {
                }
            }

            // wykorzystanie globalnego dla systemu muteksu w celu eliminacji bledow dostepu
            // przy zapisie do jednego pliku loga z wielu instancji aplikacji jednoczesnie
            using (var mutex = new Mutex(false, "Global\\NazwaTwojejAplikacjiLogMutex"))
            {
                var hasHandle = false;
                try
                {
                    try
                    {
                        hasHandle = mutex.WaitOne(5000, false);
                        if (hasHandle == false)
                        {
                            throw new TimeoutException("Przekroczono czas oczekiwania na możliwość zapisu do pliku loga.");
                        }
                    }
                    catch (AbandonedMutexException)
                    {
                        hasHandle = true;
                    }

                    File.AppendAllText(Path.Combine(logDir, string.Format("Log_{0}.txt", DateTime.Today.ToShortDateString())), string.Format("[{0} {1}] {2}{3}", dt.ToShortDateString(), dt.ToString("HH:mm:ss.fff", CultureInfo.InvariantCulture), msg, Environment.NewLine));
                }
                finally
                {
                    if (hasHandle)
                    {
                        mutex.ReleaseMutex();
                    }
                }
            }
        }
    }
}
