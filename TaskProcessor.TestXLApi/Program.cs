﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using TaskProcessor.Plugin.ERPXL.Common;

namespace TaskProcessor.TestXLApi
{
    class Program
    {
        public static string connStr = ConfigurationManager.ConnectionStrings["MainConnectionString"].ConnectionString;

        static void Main(string[] args)
        {
            var idSesji = Plugin.ERPXL.Plugin.SesjaXLUtworz(Guid.NewGuid());
            Console.WriteLine();

            DateTime DoKiedy = DateTime.Today;
            DoKiedy = DoKiedy.AddDays(Int32.Parse(ConfigurationManager.AppSettings["IleDniRezerwacji"]));

            var Data = DateTime.Today;
            int Rok = Data.Year;
            int Miesiac = Data.Month;
            int Dzien = DateTime.DaysInMonth(Rok, Miesiac);

            DateTime miesiac = new DateTime(Rok, Miesiac, Dzien);
            var poprzedniMiesiac = miesiac.AddMonths(-1);

            var PierwszyDzienMiesiaca = new DateTime(Rok, poprzedniMiesiac.Month, 1).ToXLDataInt();
            var OstatniDzienMiesiaca = new DateTime(Rok, Miesiac, Dzien).ToXLDataInt();
            var StareRezerwacje = new List<XLRezerwacja>();
            var NoweRezerwacje = new List<XLRezerwacja>();

            
            

            if (idSesji <= 0)
            {
                throw new Exception("Wystąpił błąd przy logowaniu do ERP XL. Id sesji = " + idSesji + ".");
            }

            try
            {

                using (var conn = new SqlConnection(connStr))
                {
                    var queryZaczytaj = new SqlCommand(@"
                                            SELECT 
                                                Rez_GIDNumer,
                                                Rez_GIDTyp,
                                                Rez_GIDFirma,
                                                Rez_GIDLp,
                                                Rez_TwrNumer,      
                                                Rez_KntNumer      
                                            FROM CDN.Rezerwacje
                                            WHERE Rez_KntNumer = @KntNumer AND Rez_DataAktywacji between @PierwszyDzienMiesiaca AND @OstatniDzienMiesiaca",
                        conn);

                    queryZaczytaj.Parameters.AddWithValue("@KntNumer", ConfigurationManager.AppSettings["GIDNumerKnt"]);
                    queryZaczytaj.Parameters.AddWithValue("@PierwszyDzienMiesiaca", PierwszyDzienMiesiaca);
                    queryZaczytaj.Parameters.AddWithValue("@OstatniDzienMiesiaca", OstatniDzienMiesiaca);

                    var cmd = new SqlCommand(@"exec NX_IloscDoRez @GrONumer, @AtkId", conn);

                    cmd.Parameters.AddWithValue("@GrONumer", ConfigurationManager.AppSettings["GIDNumerGrupa"]);
                    cmd.Parameters.AddWithValue("@AtkId", ConfigurationManager.AppSettings["AtkId"]);


                    conn.Open();

                    int i = 0;

                    using (var read = queryZaczytaj.ExecuteReader())
                    {
                        if (read.HasRows)
                        {
                            while (read.Read())
                            {
                                var StaraRez = new XLRezerwacja();
                                StaraRez.Towar.GIDNumer = Int32.Parse(read["Rez_TwRNumer"].ToString());
                                StaraRez.Kontrahent.GIDNumer = Int32.Parse(read["Rez_KnTNumer"].ToString());
                                StaraRez.GIDFirma = Int32.Parse(read["Rez_GIDFirma"].ToString());
                                StaraRez.GIDLp = Int32.Parse(read["Rez_GIDLp"].ToString());
                                StaraRez.GIDTyp = Int32.Parse(read["Rez_GIDTyp"].ToString());
                                StaraRez.GIDNumer = Int32.Parse(read["Rez_GIDNumer"].ToString());
                                StareRezerwacje.Add(StaraRez);
                                i++;
                            }
                        }
                    }
                    Logger.LogMessage("Ilosc rekordow:" + i);

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var NowaRez = new XLRezerwacja();

                                if (Decimal.Parse(reader["Suma"].ToString()) != 0)
                                {
                                    NowaRez.Kontrahent.GIDNumer =
                                        Int32.Parse(ConfigurationManager.AppSettings["GIDNumerKnt"]);
                                    NowaRez.Kontrahent.GIDTyp = 32;
                                    NowaRez.Towar.GIDTyp = 16;
                                    NowaRez.Towar.GIDNumer = Int32.Parse(reader["Twr_GIDNumer"].ToString());
                                    NowaRez.Towar.TwrKod = reader["Twr_Kod"].ToString();
                                    NowaRez.Magazyn = ConfigurationManager.AppSettings["Magazyn"] != ""
                                        ? ConfigurationManager.AppSettings["Magazyn"]
                                        : "MGIC";
                                    NowaRez.Ilosc = Decimal.Parse(reader["Suma"].ToString());
                                    NowaRez.DataRealizacji = DoKiedy;
                                    NowaRez.DataWaznosci = DoKiedy;
                                    NowaRez.Priorytet = Int32.Parse(ConfigurationManager.AppSettings["Priorytet"]);
                                    NoweRezerwacje.Add(NowaRez);
                                    Logger.LogMessage("Zaczytano towar o GIDNumer: " + NowaRez.Towar.GIDNumer + ", TwrKod: " + NowaRez.Towar.TwrKod + ", Ilosc: " + NowaRez.Ilosc);
                                }
                                else
                                {
                                    NowaRez.Towar.GIDNumer = Int32.Parse(reader["Twr_GIDNumer"].ToString());
                                    Logger.LogMessage("Ilosc dla towaru o GIDNumerze: " + NowaRez.Towar.GIDNumer + ", rowna zero. Pomijam towar." );
                                }
                            }
                        }
                    }
                }

                foreach (var sRez in StareRezerwacje)
                {
                    foreach (var nRez in NoweRezerwacje)
                    {
                        if (sRez.Towar.GIDNumer == nRez.Towar.GIDNumer &&
                            sRez.Kontrahent.GIDNumer == nRez.Kontrahent.GIDNumer)
                        {
                            if (Plugin.ERPXL.Plugin.RezerwacjaUsun(idSesji, sRez) > 0)
                            {
                                Console.WriteLine("Pomyslnie usunieto rezerwacje reczna dla towaru o GidNumerze: {0}",
                                    sRez.GIDNumer);
                                Logger.LogMessage("Pomyslnie usunieto rezerwacje reczna dla towaru o GidNumerze: " + sRez.GIDNumer);
                            }
                        }
                    }
                }

                foreach (var nowaRezerwacja in NoweRezerwacje)
                {
                    if (Plugin.ERPXL.Plugin.RezerwacjaUtworz(idSesji, nowaRezerwacja) > 0)
                    {
                        Console.WriteLine(string.Format( "Utworzono rezerwacje dla towaru o kodzie: {0}, na ilosc: {1}, priorytet: {2}", nowaRezerwacja.Towar.TwrKod, nowaRezerwacja.Ilosc, nowaRezerwacja.Priorytet));
                        Logger.LogMessage(string.Format( "Utworzono rezerwacje dla towaru o kodzie: {0}, na ilosc: {1}, priorytet: {2}", nowaRezerwacja.Towar.TwrKod, nowaRezerwacja.Ilosc, nowaRezerwacja.Priorytet));
                    }
                }




                if (idSesji > 0)
                {
                    Console.WriteLine();
                    Plugin.ERPXL.Plugin.SesjaXLZakoncz(idSesji);
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine("Wystapil blad: " + ex.Message);
                Logger.LogMessage("Wystąpił błąd: " + ex.Message);
            }

            Environment.Exit(0);
        }



        private static string NarzedziaPobierzNumerDok(int gidTyp, int gidNumer)
        {
            using (var conn = new SqlConnection(connStr))
            {
                var cmd = new SqlCommand(@"SELECT CDN.NazwaObiektu(@GIDTyp, @GIDNumer, 0, 2)", conn);

                cmd.Parameters.AddWithValue("@GIDTyp", gidTyp);
                cmd.Parameters.AddWithValue("@GIDNumer", gidNumer);

                conn.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    return reader.GetString(0);
                }
            }
        }
    }
}
