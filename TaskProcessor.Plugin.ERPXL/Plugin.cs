﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using TaskProcessor.Common;
using TaskProcessor.Plugin.ERPXL.Common;

namespace TaskProcessor.Plugin.ERPXL
{
    [Export(typeof (ITaskProcessorPlugin))]
    public partial class Plugin : ITaskProcessorPlugin
    {
        public Guid PluginGuid
        {
            get { return PluginDataInfo.PluginGuid; }
        }

        public string PluginName
        {
            get { return PluginDataInfo.PluginName; }
        }

        public Guid InstanceGuid { get; set; }

        public void PreparePlugin()
        {
            IdSesji = SesjaXLUtworz(InstanceGuid);
            var dt = DateTime.Now;
            OstatnieOdswiezenieLicencji = dt;
            OstatniePrzelogowanieSesji = dt;
        }

        public void UnloadPlugin()
        {
            SesjaXLZakoncz(IdSesji, InstanceGuid);
        }

        public void DoBeforeLoop()
        {
            var dt = DateTime.Now;

            // co 15 minut
            if (OstatnieOdswiezenieLicencji.AddMinutes(15) < dt)
            {
                SesjaXLOdswiezWszystkieLicencje(InstanceGuid);
                OstatnieOdswiezenieLicencji = dt;
            }

            // codziennie o pierwszej w nocy
            if ((OstatniePrzelogowanieSesji.AddDays(1) < dt) && (dt.Hour == 1))
            {
                IdSesji = SesjaXLPrzeloguj(IdSesji, InstanceGuid);
                OstatniePrzelogowanieSesji = dt;
            }
        }

        public void DoAfterLoop()
        {
            
        }

        public DateTime OstatnieOdswiezenieLicencji { get; set; }
        public DateTime OstatniePrzelogowanieSesji { get; set; }

        public int IdSesji { get; set; }

        private const string XLApiAplikacja = "TP";
        private const int XLApiWersja = 20141;
        public const int XLApiBlednaSesja = -1;

        public TaskResponseInfo WorkDispatcher(string methodName, string objJson)
        {
            var ret = new TaskResponseInfo { TaskResultCode = TaskResultCode.FinishedOk };
            try
            {
                switch (methodName)
                {
                    case "DokZamowienieUtworz":
                        {
                            ZapewnijSesjeXLApi();
                            ret.Obj = DokZamowienieUtworz(IdSesji, Helpers.DeserializeJson<XLDokZamowienieNag>(objJson));
                            break;
                        }
                    case "DokZamowienieWczytaj":
                        {
                            var z = Helpers.DeserializeJson<XLDokZamowienieNag>(objJson);
                            ret.Obj = DokZamowienieWczytaj((int)z.GIDNumer);
                            break;
                        }
                    case "DokZamowienieModyfikuj":
                        {
                            ZapewnijSesjeXLApi();
                            DokZamowienieModyfikuj(IdSesji, Helpers.DeserializeJson<XLDokZamowienieNag>(objJson));
                            break;
                        }
                    case "DokZamowienieUsun":
                        {
                            ZapewnijSesjeXLApi();
                            DokZamowienieUsun(IdSesji, Helpers.DeserializeJson<XLDokZamowienieNag>(objJson));
                            break;
                        }

                    // **********************

                    case "DokHandlowyUtworz":
                        {
                            ZapewnijSesjeXLApi();
                            ret.Obj = DokHandlowyUtworz(IdSesji, Helpers.DeserializeJson<XLDokHandlowyNag>(objJson));
                            break;
                        }
                    case "DokHandlowyWczytaj":
                        {
                            var z = Helpers.DeserializeJson<XLDokHandlowyNag>(objJson);
                            ret.Obj = DokHandlowyWczytaj((int)z.GIDNumer);
                            break;
                        }
                    case "DokHandlowyModyfikuj":
                        {
                            ZapewnijSesjeXLApi();
                            DokHandlowyModyfikuj(IdSesji, Helpers.DeserializeJson<XLDokHandlowyNag>(objJson));
                            break;
                        }
                    case "DokHandlowyUsun":
                        {
                            ZapewnijSesjeXLApi();
                            DokHandlowyUsun(IdSesji, Helpers.DeserializeJson<XLDokHandlowyNag>(objJson));
                            break;
                        }

                    // **********************
                    
                    case "DokZlecenieProdUtworz":
                        {
                            ZapewnijSesjeXLApi();
                            ret.Obj = DokZlecenieProdUtworz(IdSesji, Helpers.DeserializeJson<XLDokZlecenieProdNag>(objJson));
                            break;
                        }
                    case "DokZlecenieProdWczytaj":
                        {
                            var z = Helpers.DeserializeJson<XLDokZlecenieProdNag>(objJson);
                            ret.Obj = DokZlecenieProdWczytaj((int)z.GIDNumer);
                            break;
                        }
                    case "DokZlecenieProdModyfikuj":
                        {
                            ZapewnijSesjeXLApi();
                            DokZlecenieProdModyfikuj(IdSesji, Helpers.DeserializeJson<XLDokZlecenieProdNag>(objJson));
                            break;
                        }
                    case "DokZlecenieProdUsun":
                        {
                            ZapewnijSesjeXLApi();
                            DokZlecenieProdUsun(IdSesji, Helpers.DeserializeJson<XLDokZlecenieProdNag>(objJson));
                            break;
                        }

                    // **********************

                    case "ProdTechnologiaUtworz":
                        {
                            ZapewnijSesjeXLApi();
                            var z = Helpers.DeserializeJson<XLProdukcjaTechnologia>(objJson);
                            ret.Obj = ProdTechnologiaUtworz(IdSesji, z);
                            break;
                        }

                    // **********************

                    case "KontrahentAdresUtworz":
                        {
                            ZapewnijSesjeXLApi();
                            ret.Obj = KontrahentAdresUtworz(IdSesji, Helpers.DeserializeJson<XLKontrahentAdres>(objJson));
                            break;
                        }
                    case "KontrahentAdresModyfikuj":
                        {
                            ret.Obj = KontrahentAdresModyfikuj(Helpers.DeserializeJson<XLKontrahentAdres>(objJson));
                            break;
                        }

                    // **********************

                    case "AtrybutDodaj":
                        {
                            ZapewnijSesjeXLApi();
                            ret.Obj = AtrybutDodaj(IdSesji, Helpers.DeserializeJson<XLAtrybut>(objJson));
                            break;
                        }
                    case "AtrybutDodajListe":
                        {
                            ZapewnijSesjeXLApi();
                            ret.Obj = AtrybutDodajListe(IdSesji, Helpers.DeserializeJson<List<XLAtrybut>>(objJson));
                            break;
                        }
                    case "AtrybutPobierzWartosc":
                        {
                            ret.Obj = AtrybutPobierzWartosc(Helpers.DeserializeJson<XLAtrybut>(objJson));
                            break;
                        }
                    case "AtrybutWczytajListe":
                        {
                            ret.Obj = AtrybutWczytajListe(Helpers.DeserializeJson<XLAtrybut>(objJson));
                            break;
                        }
                    case "AtrybutModyfikuj":
                        {
                            ret.Obj = AtrybutModyfikuj(Helpers.DeserializeJson<XLAtrybut>(objJson));
                            break;
                        }
                    case "AtrybutModyfikujListe":
                        {
                            ret.Obj = AtrybutModyfikujListe(Helpers.DeserializeJson<List<XLAtrybut>>(objJson));
                            break;
                        }

                    // **********************

                    case "ZalacznikDodajListe":
                        {
                            ret.Obj = ZalacznikiDodajDoObiektu(Helpers.DeserializeJson<List<XLZalacznik>>(objJson));
                            break;
                        }
                    case "ZalacznikWczytajListe":
                        {
                            ret.Obj = ZalacznikiWczytajListe(Helpers.DeserializeJson<XLObiektOgolny>(objJson));
                            break;
                        }
                    case "ZalacznikModyfikuj":
                        {
                            ret.Obj = ZalacznikModyfikuj(Helpers.DeserializeJson<XLZalacznik>(objJson));
                            break;
                        }

                    // **********************

                    case "TowarUtworz":
                        {
                            ZapewnijSesjeXLApi();
                            ret.Obj = TowarUtworz(IdSesji, Helpers.DeserializeJson<XLTowar>(objJson));
                            break;
                        }
                    case "TowarWczytaj":
                        {
                            var z = Helpers.DeserializeJson<XLTowar>(objJson);
                            ret.Obj = TowarWczytaj((int)z.GIDNumer);
                            break;
                        }

                    // **********************

                    case "RezerwacjaWczytaj":
                        {
                            var z = Helpers.DeserializeJson<XLRezerwacja>(objJson);
                            ret.Obj = RezerwacjaWczytaj((int)z.GIDNumer);
                            break;
                        }
                    case "RezerwacjaUtworz":
                        {
                            ZapewnijSesjeXLApi();
                            ret.Obj = RezerwacjaUtworz(IdSesji, Helpers.DeserializeJson<XLRezerwacja>(objJson));
                            break;
                        }
                    case "RezerwacjaUsun":
                        {
                            ZapewnijSesjeXLApi();
                            ret.Obj = RezerwacjaUsun(IdSesji, Helpers.DeserializeJson<XLRezerwacja>(objJson));
                            break;
                        }

                    // **********************

                    case "NarzedziaPobierzNumerDok":
                        {
                            var daneDok = Helpers.DeserializeJson<XLObiektOgolny>(objJson);
                            ret.Obj = NarzedziaPobierzNumerDok((int)daneDok.GIDTyp, (int)daneDok.GIDNumer);
                            break;
                        }
                    case "NarzedziaPobierzInfoOLicencjach":
                        {
                            ret.Obj = NarzedziaPobierzInfoOLicencjach(Helpers.DeserializeJson<string>(objJson));
                            break;
                        }
                    case "NarzedziaWykonajWydruk":
                        {
                            ZapewnijSesjeXLApi();
                            var w = Helpers.DeserializeJson<XLDaneWydruku>(objJson);
                            ret.Obj = NarzedziaWykonajWydruk((int)w.IdZrodla, (int)w.IdWydruku, (int)w.IdFormatu, w.FiltrSql ?? DomyslnyFiltrSqlDlaWydrukow);
                            break;
                        }
                    case "NarzedziaPrzeliczRabat":
                        {
                            ZapewnijSesjeXLApi();
                            ret.Obj = NarzedziaPrzeliczRabat(Helpers.DeserializeJson<XLDanePrzeliczRabat>(objJson));
                            break;
                        }
                    default:
                        {
                            throw new XLApiFatalException(string.Format("Brak zdefiniowanej metody [{0}].", methodName));
                        }
                }
            }
            catch (XLApiRetryException ex)
            {
                ret.TaskResultCode = TaskResultCode.DoRetry;
                ret.Msg = ex.Message;
            }
            catch (XLApiNonFatalException ex)
            {
                ret.TaskResultCode = TaskResultCode.FinishedWarning;
                ret.Msg = ex.Message;
            }
            catch (Exception ex)
            {
                ret.TaskResultCode = TaskResultCode.FinishedError;
                ret.Msg = ex.Message;
            }

            if (ret.Msg.ToLower().Contains("brak licencji"))
            {
                IdSesji = SesjaXLPrzeloguj(IdSesji, InstanceGuid);
            }

            return ret;
        }
    }
}