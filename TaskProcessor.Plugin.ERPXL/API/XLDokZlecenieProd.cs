﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using cdn_api;
using TaskProcessor.Common;
using TaskProcessor.Plugin.ERPXL.Common;

namespace TaskProcessor.Plugin.ERPXL
{
	public partial class Plugin
	{
		public static int DokZlecenieProdUtworz(int idSesji, XLDokZlecenieProdNag obj)
		{
			var dokNag = new XLZlecenieProdInfo_20141();

			dokNag.Tryb = (int)obj.TrybPracy;
			dokNag.Wersja = XLApiWersja;
			if (obj.KontrahentGlowny.GIDTyp != null) dokNag.KntTyp = (int)obj.KontrahentGlowny.GIDTyp;
			if (obj.KontrahentGlowny.GIDNumer != null) dokNag.KntNumer = (int)obj.KontrahentGlowny.GIDNumer;
			if (obj.KontrahentGlowny.GIDNumer == null)
			{
				if (obj.KontrahentGlowny.KntAkronim != null) dokNag.KntAkronim = obj.KontrahentGlowny.KntAkronim;
			}

			if (obj.KontrahentDocelowy.GIDTyp != null) dokNag.KnDTyp = (int)obj.KontrahentDocelowy.GIDTyp;
			if (obj.KontrahentDocelowy.GIDNumer != null) dokNag.KnDNumer = (int)obj.KontrahentDocelowy.GIDNumer;
			if (obj.KontrahentDocelowy.GIDNumer == null)
			{
				if (obj.KontrahentDocelowy.KntAkronim != null) dokNag.KnDAkronim = obj.KontrahentDocelowy.KntAkronim;
			}

			if (obj.Numer != null) dokNag.Numer = (int)obj.Numer;
			if (obj.Rok != null) dokNag.Rok = (int)obj.Rok;
			if (obj.Miesiac != null) dokNag.Miesiac = (int)obj.Miesiac;
			if (obj.Seria != null) dokNag.Seria = obj.Seria;

			if (obj.Oddzial != null) dokNag.Oddzial = (int)obj.Oddzial;
			if (obj.Projekt != null) dokNag.Projekt = (int)obj.Projekt;

			if (obj.PriorytetZlc != null) dokNag.PriorytetZlc = (int)obj.PriorytetZlc;
			if (obj.PriorytetRez != null) dokNag.PriorytetRez = (int)obj.PriorytetRez;

			if (obj.DokumentObcy != null) dokNag.DokumentObcy = obj.DokumentObcy;
			if (obj.Opis != null) dokNag.Opis = obj.Opis;

			if (obj.DokZlecenieProdPlanowanieOd == XLDokZlecenieProdPlanowanieOd.NaZadanyTermin)
			{
				dokNag.PlanowacOd = 0 - obj.PlanowacOdData.ToXLCzas();
			}
			else
			{
				dokNag.PlanowacOd = obj.DokZlecenieProdPlanowanieOd == XLDokZlecenieProdPlanowanieOd.OdZadanegoTerminu ? obj.PlanowacOdData.ToXLCzas() : 0;
			}

			dokNag.RezerwujZasoby = (obj.RezerwujZasoby == null || obj.RezerwujZasoby == false) ? 0 : 1;

			int errCode = cdn_api.cdn_api.XLNoweZlecenieProd(idSesji, dokNag);
			if (errCode != 0)
			{
				throw new XLApiRetryException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.NoweZlecenieProd, errCode), errCode));
			}

			foreach (var poz in obj.Elementy.OrderBy(m => m.GIDLp))
			{
				var pozInfo = new XLZlecenieProdElemInfo_20141();

				pozInfo.Wersja = XLApiWersja;
				pozInfo.Typ = (int)XLApiTypList.ZlecenieProdukcyjne.ApiTyp;
				if (poz.ZrdTyp != null) pozInfo.ZamTyp = (int)poz.ZrdTyp;
				if (poz.ZrdNumer != null) pozInfo.ZamNumer = (int)poz.ZrdNumer;
				if (poz.ZrdLp != null) pozInfo.ZamLp = (int)poz.ZrdLp;
				if (poz.Technologia != null) pozInfo.Technologia = (int)poz.Technologia;
				if (poz.Oddzial != null) pozInfo.Oddzial = (int)poz.Oddzial;
				if (poz.PriorytetRez != null) pozInfo.PriorytetRez = (int)poz.PriorytetRez;
				if (poz.IdZlecenia != null) pozInfo.IdZlecenia = (int)poz.IdZlecenia;
				if (poz.CChTyp != null) pozInfo.CChTyp = (int)poz.CChTyp;
				if (poz.CChNumer != null) pozInfo.CChNumer = (int)poz.CChNumer;
				if (poz.CChLp != null) pozInfo.CChLp = (int)poz.CChLp;
				if (poz.Cecha != null) pozInfo.Cecha = poz.Cecha;

				if (poz.DokZlecenieProdPlanowanieOd == XLDokZlecenieProdPlanowanieOd.NaZadanyTermin)
				{
					pozInfo.PlanowacOd = 0 - poz.PlanowacOdData.ToXLCzas();
				}
				else
				{
					pozInfo.PlanowacOd = poz.DokZlecenieProdPlanowanieOd == XLDokZlecenieProdPlanowanieOd.OdZadanegoTerminu ? poz.PlanowacOdData.ToXLCzas() : 0;
				}

				pozInfo.TwrTyp = (int)poz.Towar.GIDTyp;
				pozInfo.TwrNumer = (int)poz.Towar.GIDNumer;
				pozInfo.Ilosc = ((decimal)poz.Ilosc).ToString(CultureInfo.InvariantCulture);
				if (poz.Opis != null) pozInfo.Opis = poz.Opis;

				errCode = cdn_api.cdn_api.XLDodajDoZleceniaProd(idSesji, pozInfo);
				if (errCode != 0)
				{
					throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.DodajDoZleceniaProd, errCode), errCode));
				}
			}

			var dokNagZamk = new XLZlecenieProdZamknijInfo_20141 { Wersja = XLApiWersja, Akcja = 0, Id = dokNag.Id };

			errCode = cdn_api.cdn_api.XLZamknijZlecenieProd(idSesji, dokNagZamk);
			if (errCode != 0)
			{
				throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.ZamknijZlecenieProd, errCode), errCode));
			}

			return dokNagZamk.Id;
		}

		public static int DokZlecenieProdModyfikuj(int idSesji, XLDokZlecenieProdNag obj, string connStr = null)
		{
			var dokNagZamk = new XLZlecenieProdZamknijInfo_20141 { Wersja = XLApiWersja, Id = (int)obj.GIDNumer, Tryb = (int)obj.TrybPracy, Akcja = (int)obj.TrybZamkniecia };
			int errCode = cdn_api.cdn_api.XLZamknijZlecenieProd(idSesji, dokNagZamk);
			if (errCode != 0)
			{
				try
				{
					cdn_api.cdn_api.XLZamknijZlecenieProd(idSesji, new XLZlecenieProdZamknijInfo_20141 { Wersja = XLApiWersja, Tryb = (int)obj.TrybPracy, Akcja = (int)XLTrybZamknieciaZlecProd.BezZmian });
				}
				catch
				{
				}
				throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.ZamknijZlecenieProd, errCode), errCode));
			}

			return dokNagZamk.Id;
		}

		public static int DokZlecenieProdUsun(int idSesji, XLDokZlecenieProdNag obj, string connStr = null)
		{
			var dokNagZamk = new XLZlecenieProdZamknijInfo_20141 { Wersja = XLApiWersja, Id = (int)obj.GIDNumer, Tryb = (int)obj.TrybPracy, Akcja = (int)XLTrybZamknieciaZlecProd.Usuniecie };
			int errCode = cdn_api.cdn_api.XLZamknijZlecenieProd(idSesji, dokNagZamk);
			if (errCode != 0)
			{
				try
				{
					cdn_api.cdn_api.XLZamknijZlecenieProd(idSesji, new XLZlecenieProdZamknijInfo_20141 { Wersja = XLApiWersja, Tryb = (int)obj.TrybPracy, Akcja = (int)XLTrybZamknieciaZlecProd.BezZmian });
				}
				catch
				{
				}
				throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.ZamknijZlecenieProd, errCode), errCode));
			}

			return dokNagZamk.Id;
		}

		public static XLDokZlecenieProdNag DokZlecenieProdWczytaj(int gidNumer, string connStr = null)
		{
			var ret = new XLDokZlecenieProdNag { TrybZamkniecia = null };

			using (var conn = new SqlConnection(connStr ?? Helpers.GetSettingsValue("ConnectionString", true)))
			{
				var cmdNag = new SqlCommand(@"
					SELECT 
						PZL_Id,
						PZL_Projekt,
						PZL_Oddzial,
						PZL_DataWystawienia,
						PZL_OpWNumer,
						PZL_FrsId,
						PZL_DataZamkniecia,
						PZL_OpZNumer,
						PZL_CzasModyfikacji,
						PZL_OpMNumer,
						PZL_KntTyp,
						PZL_KntNumer,
						PZL_KnDTyp,
						PZL_KnDNumer,
						PZL_Opis,
						PZL_Rok,
						PZL_Miesiac,
						PZL_Seria,
						PZL_Numer,
						PZL_Lp,
						PZL_Zaksiegowano,
						PZL_SCHTyp,
						PZL_SCHNumer,
						PZL_WsSCHTyp,
						PZL_WsSCHNumer,
						PZL_WsStosujSchemat,
						PZL_WsStosujDziennik,
						PZL_Koszt,
						PZL_KosztSurowca,
						PZL_PriorytetRez,
						PZL_DokumentObcy,
						PZL_PriorytetZlc,
						PZL_PlanowacOd,
						PZL_RezerwujZasoby,
						PZL_PrjId,
						PZL_KosztUstalony,
						PZL_FrmNumer,
						PZL_Stan,
						PZL_Status,
						PZL_GenDokZatwierdzone,
						PZL_DokPrzyjeciaPoDokWydania,
						PZL_ZwolnioneDoProd,
						PZL_RealizacjaWgPlanu,
						ISNULL(K.Knt_Akronim, '') AS 'Knt_Akronim'
					FROM 
						CDN.ProdZlecenia
						LEFT JOIN CDN.KntKarty K
							ON K.Knt_GIDNumer = PZL_KntNumer
					WHERE
						PZL_Id = @GIDNumer
				", conn);

				cmdNag.Parameters.AddWithValue("@GIDNumer", gidNumer);

				conn.Open();

				using (var reader = cmdNag.ExecuteReader())
				{
					reader.Read();

					ret.GIDNumer = Int32.Parse(reader["PZL_Id"].ToString());
					ret.Projekt = Int32.Parse(reader["PZL_Projekt"].ToString());
					ret.Oddzial = Int32.Parse(reader["PZL_Oddzial"].ToString());
					ret.DataWystawienia = Int32.Parse(reader["PZL_DataWystawienia"].ToString()).ToDateTimeFromXLDataInt();
					ret.OpWNumer = Int32.Parse(reader["PZL_OpWNumer"].ToString());
					ret.KontrahentGlowny.GIDTyp = Int32.Parse(reader["PZL_KntTyp"].ToString());
					ret.KontrahentGlowny.GIDNumer = Int32.Parse(reader["PZL_KntNumer"].ToString());
					ret.KontrahentGlowny.KntAkronim = reader["Knt_Akronim"].ToString();
					ret.KontrahentDocelowy.GIDTyp = Int32.Parse(reader["PZL_KnDTyp"].ToString());
					ret.KontrahentDocelowy.GIDNumer = Int32.Parse(reader["PZL_KnDNumer"].ToString());
					ret.Opis = reader["PZL_Opis"].ToString();
					ret.Rok = Int32.Parse(reader["PZL_Rok"].ToString());
					ret.Miesiac = Int32.Parse(reader["PZL_Miesiac"].ToString());
					ret.Seria = reader["PZL_Seria"].ToString();
					ret.Numer = Int32.Parse(reader["PZL_Numer"].ToString());
					ret.PriorytetRez = Int32.Parse(reader["PZL_PriorytetRez"].ToString());
					ret.DokumentObcy = reader["PZL_DokumentObcy"].ToString();
					ret.PriorytetZlc = Int32.Parse(reader["PZL_PriorytetZlc"].ToString());

					ret.PZL_Lp = Int32.Parse(reader["PZL_Lp"].ToString());
					ret.PZL_SCHTyp = Int32.Parse(reader["PZL_SCHTyp"].ToString());
					ret.PZL_SCHNumer = Int32.Parse(reader["PZL_SCHNumer"].ToString());
					ret.PZL_WsSCHTyp = Int32.Parse(reader["PZL_WsSCHTyp"].ToString());
					ret.PZL_WsSCHNumer = Int32.Parse(reader["PZL_WsSCHNumer"].ToString());
					ret.PZL_WsStosujSchemat = Int32.Parse(reader["PZL_WsStosujSchemat"].ToString());
					ret.PZL_WsStosujDziennik = Int32.Parse(reader["PZL_WsStosujDziennik"].ToString());
					ret.PZL_Koszt = Decimal.Parse(reader["PZL_Koszt"].ToString());
					ret.PZL_KosztSurowca = Decimal.Parse(reader["PZL_KosztSurowca"].ToString());
					ret.PZL_Frs_Id = Int32.Parse(reader["PZL_FrsId"].ToString());
					ret.PZL_DataZamkniecia = Int32.Parse(reader["PZL_DataZamkniecia"].ToString()).ToDateTimeFromXLDataInt();
					ret.PZL_OpMNumer = Int32.Parse(reader["PZL_OpMNumer"].ToString());
					ret.PZL_PrjID = Int32.Parse(reader["PZL_PrjId"].ToString());
					ret.PZL_KosztUstalony = Int32.Parse(reader["PZL_KosztUstalony"].ToString());
					ret.PZL_FrmNumer = Int32.Parse(reader["PZL_FrmNumer"].ToString());
					ret.PZL_Stan = Int32.Parse(reader["PZL_Stan"].ToString());
					ret.PZL_Status = reader["PZL_Status"].ToString();
					ret.PZL_GenDokZatwierdzone = Int32.Parse(reader["PZL_GenDokZatwierdzone"].ToString());
					ret.PZL_DokPrzyjeciaPoDokWydania = Int32.Parse(reader["PZL_DokPrzyjeciaPoDokWydania"].ToString());
					ret.PZL_ZwolnioneDoProd = Int32.Parse(reader["PZL_ZwolnionedoProd"].ToString());
					ret.PZL_RealizacjaWgPlanu = Int32.Parse(reader["PZL_RealizacjaWgPlanu"].ToString());
				}

				var cmdElem = new SqlCommand(@"
					SELECT 
						PZE_Id,
						PZE_Zlecenie,
						PZE_Lp,
						PZE_TwrTyp,
						PZE_TwrNumer,
						PZE_Ilosc,
						PZE_Technologia,
						PZE_KlasaCechy,
						PZE_Cecha,
						PZE_PlanowacOd,
						PZE_ZrownoleglacDo,
						PZE_KntTyp,
						PZE_KntNumer,
						PZE_KnDTyp,
						PZE_KnDNumer,
						PZE_Priorytet,
						PZE_IloscPlan,
						ISNULL(Twr_Nazwa, '') AS 'Twr_Nazwa',
						ISNULL(Twr_Kod, '') AS 'Twr_Kod',
						ZZL_ZSGidTyp,
						ZZL_ZSGidNumer,
						ZZL_ZSGidLp
					FROM 
						CDN.ProdZlecElem
						LEFT JOIN CDN.TwrKarty
							ON PZE_TwrNumer = Twr_GIDNumer
						LEFT JOIN CDN.ZamZamLinki 
							ON PZE_Id = ZZL_ZZGidNumer AND ZZL_ZZGidTyp = 14343 AND PZE_Lp = ZZL_ZZGidLp
					WHERE
						PzE_Zlecenie = @GIDNumer
					ORDER BY
						PzE_Lp
				", conn);

				cmdElem.Parameters.AddWithValue("@GIDNumer", gidNumer);

				using (var reader = cmdElem.ExecuteReader())
				{
					while (reader.Read())
					{
						var p = new XLDokZlecenieProdElem();

						p.GIDNumer = gidNumer;
						p.GIDLp = Int32.Parse(reader["PZE_Lp"].ToString());
						p.Towar.GIDTyp = Int32.Parse(reader["PZE_TwrTyp"].ToString());
						p.Towar.GIDNumer = Int32.Parse(reader["PZE_TwrNumer"].ToString());
						p.Towar.TwrNazwa = reader["Twr_Nazwa"].ToString();
						p.Towar.TwrKod = reader["Twr_Kod"].ToString();
						p.Ilosc = Decimal.Parse(reader["PzE_Ilosc"].ToString());
						p.ZrdTyp = DBNull.Value.Equals(reader["ZZL_ZSGidTyp"]) ? (int?)null : Int32.Parse(reader["ZZL_ZSGidTyp"].ToString());
						p.ZrdNumer = DBNull.Value.Equals(reader["ZZL_ZSGidNumer"]) ? (int?)null : Int32.Parse(reader["ZZL_ZSGidNumer"].ToString());
						p.ZrdLp = DBNull.Value.Equals(reader["ZZL_ZSGidLp"]) ? (int?)null : Int32.Parse(reader["ZZL_ZSGidLp"].ToString());
						p.Technologia = Int32.Parse(reader["PZE_Technologia"].ToString());
						p.KontrahentGlowny.GIDTyp = Int32.Parse(reader["PZE_KntTyp"].ToString());
						p.KontrahentGlowny.GIDNumer = Int32.Parse(reader["PZE_KntNumer"].ToString());
						p.KontrahentDocelowy.GIDTyp = Int32.Parse(reader["PZE_KnDTyp"].ToString());
						p.KontrahentDocelowy.GIDNumer = Int32.Parse(reader["PZE_KnDNumer"].ToString());
						p.PriorytetRez = Int32.Parse(reader["PZE_Priorytet"].ToString());

						p.PZE_ZrownoleglacDo = Decimal.Parse(reader["PZE_ZrownoleglacDo"].ToString());
						p.PZE_IloscPlan = DBNull.Value.Equals(reader["PZE_IloscPlan"]) ? (decimal?)null : Decimal.Parse(reader["PZE_IloscPlan"].ToString());

						ret.Elementy.Add(p);
					}
				}
			}

			return ret;
		}
	}
}