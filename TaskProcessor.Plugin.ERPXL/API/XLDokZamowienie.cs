﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using cdn_api;
using TaskProcessor.Common;
using TaskProcessor.Plugin.ERPXL.Common;

namespace TaskProcessor.Plugin.ERPXL
{
	public partial class Plugin
	{
		public static int DokZamowienieUtworz(int idSesji, XLDokZamowienieNag obj, string connStr = null)
		{
			int dokNumer;
			int idDok = 0;
			int errCode = 0;
			var errMsg = new List<string>();

			try
			{
				if (obj.GIDNumer == null)
				{ 
					var dokNag = new XLDokumentZamNagInfo_20141();

					dokNag.Tryb = (int) obj.TrybPracy;
					dokNag.Wersja = XLApiWersja;
					dokNag.Typ = (int)obj.ApiTyp;

					if (obj.KontrahentGlowny.GIDTyp != null) dokNag.KntTyp = (int) obj.KontrahentGlowny.GIDTyp;
					if (obj.KontrahentGlowny.GIDNumer != null) dokNag.KntNumer = (int) obj.KontrahentGlowny.GIDNumer;
					if (obj.KontrahentGlowny.GIDLp != null) dokNag.KntLp = (int) obj.KontrahentGlowny.GIDLp;
					if (obj.KontrahentGlowny.GIDFirma != null) dokNag.KntFirma = (int) obj.KontrahentGlowny.GIDFirma;
					if (obj.KontrahentGlowny.KntAkronim != null) dokNag.Akronim = obj.KontrahentGlowny.KntAkronim;

					if (obj.KontrahentDocelowy.GIDTyp != null) dokNag.KnDTyp = (int) obj.KontrahentDocelowy.GIDTyp;
					if (obj.KontrahentDocelowy.GIDNumer != null) dokNag.KnDNumer = (int) obj.KontrahentDocelowy.GIDNumer;
					if (obj.KontrahentDocelowy.GIDLp != null) dokNag.KnDLp = (int) obj.KontrahentDocelowy.GIDLp;
					if (obj.KontrahentDocelowy.GIDFirma != null) dokNag.KnDFirma = (int) obj.KontrahentDocelowy.GIDFirma;
					if (obj.KontrahentDocelowy.KntAkronim != null) dokNag.AkronimDocelowego = obj.KontrahentDocelowy.KntAkronim;
				
					if (obj.KontrahentAkwizytor.GIDTyp != null) dokNag.AkwTyp = (int) obj.KontrahentAkwizytor.GIDTyp;
					if (obj.KontrahentAkwizytor.GIDNumer != null) dokNag.AkwNumer = (int) obj.KontrahentAkwizytor.GIDNumer;
					if (obj.KontrahentAkwizytor.GIDLp != null) dokNag.AkwLp = (int) obj.KontrahentAkwizytor.GIDLp;
					if (obj.KontrahentAkwizytor.GIDFirma != null) dokNag.AkwFirma = (int) obj.KontrahentAkwizytor.GIDFirma;
					if (obj.KontrahentAkwizytor.KntAkronim != null) dokNag.Akwizytor = obj.KontrahentAkwizytor.KntAkronim;
					if (obj.AkwizytorKntPrc != null) dokNag.AkwizytorKntPrc = (int) obj.AkwizytorKntPrc;

					if (obj.GIDFirma != null) dokNag.GIDFirma = (int)obj.GIDFirma;
					if (obj.WspolnaWaluta != null) dokNag.WspolnaWaluta = (bool)obj.WspolnaWaluta ? 1 : 0;
					if (obj.RodzajZamowienia != null) dokNag.Wewnetrzne = (int) obj.RodzajZamowienia;
					if (obj.DataWystawienia != null) dokNag.DataWystawienia = obj.DataWystawienia.ToXLDataInt();
					if (obj.DataRealizacji != null) dokNag.DataRealizacji = obj.DataRealizacji.ToXLDataInt();
					if (obj.DataWaznosci != null) dokNag.DataWaznosci = obj.DataWaznosci.ToXLDataInt();
					if (obj.DataAktywacjiRez != null) dokNag.DataAktywacjiRezerwacji = obj.DataAktywacjiRez.ToXLDataInt();
					if (obj.MagazynW != null) dokNag.MagazynW = obj.MagazynW;
					if (obj.AdrTyp != null) dokNag.AdrTyp = (int) obj.AdrTyp;
					if (obj.AdrNumer != null) dokNag.AdrNumer = (int) obj.AdrNumer;
					if (obj.AdwTyp != null) dokNag.AdwTyp = (int) obj.AdwTyp;
					if (obj.AdwNumer != null) dokNag.AdwNumer = (int) obj.AdwNumer;
					if (obj.AdPNumer != null) dokNag.AdPNumer = (int) obj.AdPNumer;
					if (obj.RealWCalosci != null) dokNag.RealWCalosci = (int)obj.RealWCalosci;
					if (obj.ExpoNorm != null) dokNag.ExpoNorm = (int) obj.ExpoNorm;
					if (obj.Seria != null) dokNag.ZamSeria = obj.Seria; // seria może być pusta!
					if (obj.Numer != null) dokNag.Numer = (int) obj.Numer;
					if (obj.Miesiac != null) dokNag.Miesiac = (int) obj.Miesiac;
					if (obj.Rok != null) dokNag.Rok = (int) obj.Rok;
					if (obj.DokumentObcy != null) dokNag.DokumentObcy = obj.DokumentObcy;
					if (obj.Waluta != null) dokNag.Waluta = obj.Waluta;
					if (obj.FlagaNB != null) dokNag.FlagaNB = obj.FlagaNB;
					if (obj.FormaPlatnosci != null) dokNag.FormaPl = (int) obj.FormaPlatnosci;
					if (obj.MagTyp != null) dokNag.MagTyp = (int) obj.MagTyp;
					if (obj.MagFirma != null) dokNag.MagFirma = (int) obj.MagFirma;
					if (obj.MagNumer != null) dokNag.MagNumer = (int) obj.MagNumer;
					if (obj.MagLp != null) dokNag.MagLp = (int) obj.MagLp;
					if (obj.RezerwacjeNaNiepotwierdzonym != null) dokNag.RezerwacjeNaNiepotwierdzonym = (int) obj.RezerwacjeNaNiepotwierdzonym;
					dokNag.TerminPlatnosci = obj.TerminPlatnosci ?? -1;
					if (obj.KursL != null) dokNag.KursL = (int) obj.KursL;
					if (obj.KursM != null) dokNag.KursM = (int) obj.KursM;
					if (obj.NrKursu != null) dokNag.NrKursu = (int) obj.NrKursu;
					if (obj.TypKursu != null) dokNag.TypKursu = (int) obj.TypKursu;
					if (obj.ZrdTyp != null) dokNag.ZrdTyp = (int) obj.ZrdTyp;
					if (obj.ZrdFirma != null) dokNag.ZrdFirma = (int) obj.ZrdFirma;
					if (obj.ZrdNumer != null) dokNag.ZrdNumer = (int) obj.ZrdNumer;
					if (obj.Opis != null) dokNag.Opis = obj.Opis;
					if (obj.Url != null) dokNag.URL = obj.Url;

					errCode = cdn_api.cdn_api.XLNowyDokumentZam(idSesji, ref idDok, dokNag);
					if (errCode != 0)
					{
						throw new XLApiRetryException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.NowyDokumentZam, errCode), errCode));
					}
				}
				else
				{
					var dokNag = new XLOtwarcieDokumentuZamInfo_20141();
					dokNag.Tryb = (int)obj.TrybPracy;
					dokNag.Wersja = XLApiWersja;
					dokNag.ZaNNumer = (int)obj.GIDNumer;

					errCode = cdn_api.cdn_api.XLOtworzDokumentZam(idSesji, ref idDok, dokNag);
					if (errCode != 0)
					{
						throw new XLApiRetryException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.OtworzDokumentZam, errCode), errCode));
					}
				}

				foreach (var poz in obj.Elementy)
				{
					var pozInfo = new XLDokumentZamElemInfo_20141();
					pozInfo.Wersja = XLApiWersja;
					if (poz.Towar.GIDTyp != null) pozInfo.TwrTyp = (int) poz.Towar.GIDTyp;
					if (poz.Towar.GIDNumer != null) pozInfo.TwrNumer = (int) poz.Towar.GIDNumer;
					if (poz.Towar.TwrKod != null) pozInfo.Towar = poz.Towar.TwrKod;
					pozInfo.Ilosc = ((decimal)poz.Ilosc).ToString(CultureInfo.InvariantCulture);
					if (poz.Nazwa != null) pozInfo.Nazwa = poz.Nazwa;
					if (poz.Wartosc != null) pozInfo.Wartosc = ((decimal)poz.Wartosc).ToString(CultureInfo.InvariantCulture);
					if (poz.FlagaVAT != null) pozInfo.FlagaVat = (int) poz.FlagaVAT;
					if (poz.Waluta != null) pozInfo.Waluta = poz.Waluta;
					if (poz.Rownanie != null) pozInfo.Rownanie = (int) poz.Rownanie;
					if (poz.DataAktywacjiRezerwacji != null) pozInfo.DataAktywacjiRezerwacji = poz.DataAktywacjiRezerwacji.ToXLDataInt();
					if (poz.DataWaznosciRezerwacji != null) pozInfo.DataWaznosciRezerwacji = poz.DataWaznosciRezerwacji.ToXLDataInt();
					if (poz.ZrdTyp != null) pozInfo.ZrdTyp = (int) poz.ZrdTyp;
					if (poz.ZrdFirma != null) pozInfo.ZrdFirma = (int) poz.ZrdFirma;
					if (poz.ZrdNumer != null) pozInfo.ZrdNumer = (int) poz.ZrdNumer;
					if (poz.ZrdLp != null) pozInfo.ZrdLp = (int) poz.ZrdLp;
					if (poz.Opis != null) pozInfo.Opis = poz.Opis;

					errCode = cdn_api.cdn_api.XLDodajPozycjeZam(idDok, pozInfo);
					if (errCode != 0)
					{
						if (errCode == 35) // deadlock
						{
							throw new XLApiRetryException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.DodajPozycjeZam, errCode), errCode));
						}
						throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.DodajPozycjeZam, errCode), errCode));
					}
				}

				foreach (var plat in obj.Platnosci)
				{
					var platInfo = new XLDokumentZamPlatInfo_20141();
					platInfo.Wersja = XLApiWersja;
					if (plat.TerminData != null) platInfo.TerminData = plat.TerminData.ToXLDataInt();
					if (plat.TerminOffset != null) platInfo.TerminOffset = (int) plat.TerminOffset;
					if (plat.FormaPlatnosci != null) platInfo.FormaPl = (int) plat.FormaPlatnosci;
					if (plat.KwotaProcent != null) platInfo.Procent = (int) plat.KwotaProcent;
					if (plat.KwotaWartosc != null) platInfo.Kwota = ((decimal) plat.KwotaWartosc).ToString(CultureInfo.InvariantCulture);
					if (plat.CzyZaliczka != null) platInfo.Zaliczka = (bool) plat.CzyZaliczka ? 1 : 0;
					if (plat.Waluta != null) platInfo.Waluta = plat.Waluta;
					if (plat.Opis != null) platInfo.Notatki = plat.Opis;

					errCode = cdn_api.cdn_api.XLDodajPlatnoscZam(idDok, platInfo);
					if (errCode != 0)
					{
						throw new XLApiNonFatalException(string.Format("Wystąpił błąd w funkcji XLDodajPlatnoscZam, Kod błędu = {0}.", errCode));
					}
				}

				var dokNagZamk = new XLZamkniecieDokumentuZamInfo_20141 { Wersja = XLApiWersja, TrybZamkniecia = (int) obj.TrybZamkniecia };
				errCode = cdn_api.cdn_api.XLZamknijDokumentZam(idDok, dokNagZamk);
				if (errCode != 0)
				{
					throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.ZamknijDokumentZam, errCode), errCode));
				}

				dokNumer = dokNagZamk.ZamNumer;

				foreach (var atr in obj.Atrybuty)
				{
					try
					{
						var daneZam = DokZamowienieWczytaj(dokNumer, connStr);

						var dodAtrInfo = new XLAtrybutInfo_20141();
						dodAtrInfo.Wersja = XLApiWersja;
						dodAtrInfo.GIDTyp = atr.GIDTyp ?? (int)daneZam.GIDTyp;
						dodAtrInfo.GIDFirma = atr.GIDFirma ?? (int)daneZam.GIDFirma;
						dodAtrInfo.GIDNumer = atr.GIDNumer ?? (int)daneZam.GIDNumer;
						if (atr.GIDLp != null) dodAtrInfo.GIDLp = (int)atr.GIDLp;
						if (atr.GIDSubLp != null) dodAtrInfo.GIDSubLp = (int)atr.GIDSubLp;
						dodAtrInfo.Klasa = atr.Klasa;
						dodAtrInfo.Wartosc = atr.Wartosc;

						errCode = cdn_api.cdn_api.XLDodajAtrybut(idSesji, dodAtrInfo);
						if (errCode != 0)
						{
							throw new Exception(string.Format("Wystąpił błąd w funkcji XLDodajAtrybut (KlasaAtrybutu = {0}), Kod błędu = {1}.", atr.Klasa, errCode));
						}
					}
					catch (Exception ex)
					{
						errMsg.Add(ex.Message);
					}
				}

				try
				{
					if (obj.KontrahentGlowny.NumerOsoby != null && obj.KontrahentGlowny.NumerOsoby > 0)
					{
						var uzupOsob = DokZamowienieUzupelnijOsoby(dokNagZamk.ZamNumer, (int)obj.KontrahentGlowny.NumerOsoby, connStr);
						if (!uzupOsob)
						{
							throw new Exception("Wystąpił błąd przy wpisywaniu osoby na dokument (KontrahentGlowny).");
						}
					}
				}
				catch (Exception ex)
				{
					errMsg.Add(ex.Message);
				}

				try
				{
					if (obj.KontrahentDocelowy.NumerOsoby != null && obj.KontrahentDocelowy.NumerOsoby > 0)
					{
						var uzupOsob = DokZamowienieUzupelnijOsoby(dokNagZamk.ZamNumer, (int)obj.KontrahentDocelowy.NumerOsoby, connStr);
						if (!uzupOsob)
						{
							throw new Exception("Wystąpił błąd przy wpisywaniu osoby na dokument (KontrahentDocelowy).");
						}
					}
				}
				catch (Exception ex)
				{
					errMsg.Add(ex.Message);
				}

				try
				{
					if (obj.Zalaczniki.Any())
					{
						var zalaczniki = obj.Zalaczniki;
						foreach (var zal in zalaczniki)
						{
							zal.ObiTyp = dokNagZamk.ZamTyp;
							zal.ObiNumer = dokNagZamk.ZamNumer;
						}
						var dodZal = ZalacznikiDodajDoObiektu(zalaczniki, connStr);
						if (!dodZal)
						{
							throw new Exception("Wystąpił błąd przy dodawaniu załączników.");
						}
					}
				}
				catch (Exception ex)
				{
					errMsg.Add(ex.Message);
				}

				if (errMsg.Any())
				{
					throw new XLApiNonFatalException(string.Join("; ", errMsg));
				}
			}
			catch (Exception ex)
			{
				if (ex is XLApiNonFatalException && ex is XLApiRetryException)
				{
				}
				else
				{
					if (idDok > 0)
					{
						if (obj.GIDNumer == null)
						{
							cdn_api.cdn_api.XLZamknijDokumentZam(idDok, new XLZamkniecieDokumentuZamInfo_20141 { Wersja = XLApiWersja, TrybZamkniecia = (int)XLTrybZamknieciaZam.Usuniecie });
						}
						else
						{ 
							cdn_api.cdn_api.XLZamknijDokumentZam(idDok, new XLZamkniecieDokumentuZamInfo_20141 { Wersja = XLApiWersja, TrybZamkniecia = (int)XLTrybZamknieciaZam.BezZmian });
						}
					}
				}
				throw;
			}

			return dokNumer;
		}

		public static int DokZamowienieModyfikuj(int idSesji, XLDokZamowienieNag obj, string connStr = null)
		{
			var dokNag = new XLOtwarcieDokumentuZamInfo_20141();
			dokNag.Tryb = (int)obj.TrybPracy;
			dokNag.Wersja = XLApiWersja;
			dokNag.ZaNNumer = (int)obj.GIDNumer;

			int idDok = 0;

			int errCode = cdn_api.cdn_api.XLOtworzDokumentZam(idSesji, ref idDok, dokNag);
			if (errCode != 0)
			{
				throw new XLApiRetryException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.OtworzDokumentZam, errCode), errCode));
			}

			foreach (var poz in obj.Elementy)
			{
				var pozInfo = new XLDokumentZamElemModInfo_20141();
				pozInfo.Wersja = XLApiWersja;
				pozInfo.EleLp = (int)poz.GIDLp;
				if (poz.ModRabat != null) pozInfo.Rabat = (int)poz.ModRabat;
				if (poz.ModCena != null) pozInfo.Cena = ((decimal)poz.ModCena).ToString(CultureInfo.InvariantCulture);
				if (poz.ModCenaP != null) pozInfo.CenaP = ((decimal)poz.ModCenaP).ToString(CultureInfo.InvariantCulture);
				if (poz.Ilosc != null) pozInfo.Ilosc = ((decimal)poz.Ilosc).ToString(CultureInfo.InvariantCulture);
				if (poz.Wartosc != null) pozInfo.Wartosc = ((decimal)poz.Wartosc).ToString(CultureInfo.InvariantCulture);
				if (poz.ModVAT != null) pozInfo.Vat = poz.ModVAT;
				if (poz.ModStawkaVAT != null) pozInfo.StawkaVAT = ((decimal)poz.ModStawkaVAT).ToString(CultureInfo.InvariantCulture);
				if (poz.Waluta != null) pozInfo.Waluta = poz.Waluta;
				
				errCode = cdn_api.cdn_api.XLModyfikujPozycjeZam(idDok, pozInfo);
				if (errCode != 0)
				{
					throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", InfoOBledzieModyfikujPozycjeZam(errCode), errCode));
				}
			}

			var dokNagZamk = new XLZamkniecieDokumentuZamInfo_20141 { Wersja = XLApiWersja, TrybZamkniecia = (int)obj.TrybZamkniecia };
			errCode = cdn_api.cdn_api.XLZamknijDokumentZam(idDok, dokNagZamk);
			if (errCode != 0)
			{
				try
				{
					cdn_api.cdn_api.XLZamknijDokumentZam(idDok, new XLZamkniecieDokumentuZamInfo_20141 { Wersja = XLApiWersja, TrybZamkniecia = (int)XLTrybZamknieciaZam.BezZmian });
				}
				catch
				{
				}
				throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.ZamknijDokumentZam, errCode), errCode));
			}
			
			return dokNagZamk.ZamNumer;
		}

		public static int DokZamowienieUsun(int idSesji, XLDokZamowienieNag obj, string connStr = null)
		{
			var dokNag = new XLOtwarcieDokumentuZamInfo_20141();
			dokNag.Tryb = (int)obj.TrybPracy;
			dokNag.Wersja = XLApiWersja;

			if (obj.GIDNumer == null)
			{
				if (obj.Elementy.Any())
				{
					dokNag.ZaNNumer = (int) obj.Elementy.First().GIDNumer;
				}
			}
			else
			{
				dokNag.ZaNNumer = (int) obj.GIDNumer;
			}

			int idDok = 0;

			int errCode = cdn_api.cdn_api.XLOtworzDokumentZam(idSesji, ref idDok, dokNag);
			if (errCode != 0)
			{
				throw new XLApiRetryException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.OtworzDokumentZam, errCode), errCode));
			}

			foreach (var poz in obj.Elementy)
			{
				var pozInfo = new XLUsunZamElemInfo_20141();
				pozInfo.Wersja = XLApiWersja;
				pozInfo.Pozycja = (int)poz.GIDLp;

				errCode = cdn_api.cdn_api.XLUsunPozycjeZam(idDok, pozInfo);
				if (errCode != 0)
				{
					throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", InfoOBledzieUsunPozycjeZam(errCode), errCode));
				}
			}

			var trybZamk = (obj.GIDNumer == null || obj.TrybZamkniecia == null) ? XLTrybZamknieciaZam.BezZmian : obj.TrybZamkniecia;

			var dokNagZamk = new XLZamkniecieDokumentuZamInfo_20141 { Wersja = XLApiWersja, TrybZamkniecia = (int)trybZamk };
			errCode = cdn_api.cdn_api.XLZamknijDokumentZam(idDok, dokNagZamk);
			if (errCode != 0)
			{
				try
				{
					cdn_api.cdn_api.XLZamknijDokumentZam(idDok, new XLZamkniecieDokumentuZamInfo_20141 { Wersja = XLApiWersja, TrybZamkniecia = (int)XLTrybZamknieciaZam.BezZmian });
				}
				catch
				{
				}
				throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.ZamknijDokumentZam, errCode), errCode));
			}

			return dokNagZamk.ZamNumer;
		}

		public static XLDokZamowienieNag DokZamowienieWczytaj(int gidNumer, string connStr = null)
		{
			var ret = new XLDokZamowienieNag { WspolnaWaluta = null, TrybZamkniecia = null, ExpoNorm = null };

			using (var conn = new SqlConnection(connStr ?? Helpers.GetSettingsValue("ConnectionString", true)))
			{
				var cmdNag = new SqlCommand(@"
					SELECT
						ZaN_GIDTyp,
						ZaN_GIDFirma,
						ZaN_KntTyp,
						ZaN_KntNumer,
						ISNULL(Knt_Akronim, '') AS 'Knt_Akronim',
						ZaN_KnSLp,
						ZaN_FlagaNB,
						ZaN_FormaNr,
						ZaN_MagNumer,
						ZaN_DataRealizacji,
						ZaN_DataWaznosci,
						ZaN_DataWystawienia,
						ZaN_Waluta,
						ZaN_KursL,
						ZaN_KursM,
						ZaN_NrKursu,
						ZaN_TypKursu,
						ZaN_ZamNumer,
						ZaN_ZamMiesiac,
						ZaN_ZamRok,
						ZaN_ZamTyp,
						ZaN_ZrdFirma,
						ZaN_RokMiesiac,
						ZaN_Stan
						ZaN_Url,
						ZaN_ZrdNumer,
						ZaN_ZrdTyp,
						ZaN_Stan,
						ZaN_ExpoNorm,
						ZaN_ZamSeria,
						ZaN_PotwOferty,
						ZaN_DokumentObcy,
						ISNULL((SELECT TOP 1 ZnO_Opis FROM CDN.ZaNOpisy WHERE ZnO_ZamTyp = ZaN_GIDTyp AND ZnO_ZamNumer = ZaN_GIDNumer), '') AS 'Opis'
					FROM
						CDN.ZamNag
						LEFT JOIN CDN.KntKarty KG
							ON KG.Knt_GIDNumer = ZaN_KntNumer
					WHERE
						ZaN_GIDNumer = @GIDNumer
				", conn);

				cmdNag.Parameters.AddWithValue("@GIDNumer", gidNumer);

				conn.Open();

				using (var reader = cmdNag.ExecuteReader())
				{
					reader.Read();

					ret.GIDTyp = Int32.Parse(reader["ZaN_GIDTyp"].ToString());
					ret.ZamTyp = Int32.Parse(reader["ZaN_ZamTyp"].ToString());
					ret.ApiTyp = UzupelnijXLApiTyp((int)ret.GIDTyp, ret.ZamTyp);
					ret.GIDNumer = gidNumer;
					ret.GIDFirma = Int32.Parse(reader["ZaN_GIDFirma"].ToString());
					ret.Seria = reader["ZaN_ZamSeria"].ToString();
					ret.KontrahentGlowny.GIDTyp = Int32.Parse(reader["ZaN_KntTyp"].ToString());
					ret.KontrahentGlowny.GIDNumer = Int32.Parse(reader["ZaN_KntNumer"].ToString());
					ret.KontrahentGlowny.NumerOsoby = Int32.Parse(reader["ZaN_KnSLp"].ToString());
					ret.KontrahentGlowny.KntAkronim = reader["Knt_Akronim"].ToString();
					ret.FlagaNB = reader["ZaN_FlagaNB"].ToString();
					ret.FormaPlatnosci = Int32.Parse(reader["ZaN_FormaNr"].ToString());
					ret.MagNumer = Int32.Parse(reader["ZaN_MagNumer"].ToString());
					ret.DataRealizacji = Int32.Parse(reader["ZaN_DataRealizacji"].ToString()).ToDateTimeFromXLDataInt();
					ret.DataWaznosci = Int32.Parse(reader["ZaN_DataWaznosci"].ToString()).ToDateTimeFromXLDataInt();
					ret.DataWystawienia = Int32.Parse(reader["ZaN_DataWystawienia"].ToString()).ToDateTimeFromXLDataInt();
					ret.Waluta = reader["ZaN_Waluta"].ToString();
					ret.KursL = Decimal.Parse(reader["ZaN_KursL"].ToString());
					ret.KursM = Int32.Parse(reader["ZaN_KursM"].ToString());
					ret.NrKursu = Int32.Parse(reader["ZaN_NrKursu"].ToString());
					ret.TypKursu = Int32.Parse(reader["ZaN_TypKursu"].ToString());
					ret.Rok = Int32.Parse(reader["ZaN_ZamRok"].ToString());
					ret.Miesiac = Int32.Parse(reader["ZaN_ZamMiesiac"].ToString());
					ret.Numer = Int32.Parse(reader["ZaN_ZamNumer"].ToString());
					ret.RokMiesiac = Int32.Parse(reader["ZaN_RokMiesiac"].ToString());
					ret.ZrdNumer = Int32.Parse(reader["ZaN_ZamNumer"].ToString());
					ret.ZrdTyp = Int32.Parse(reader["ZaN_ZrdTyp"].ToString());
					ret.ZrdFirma = Int32.Parse(reader["ZaN_ZrdFirma"].ToString());
					ret.Stan = Int32.Parse(reader["ZaN_Stan"].ToString());
					ret.ExpoNorm = Int32.Parse(reader["ZaN_ExpoNorm"].ToString());
					ret.DokumentObcy = reader["ZaN_DokumentObcy"].ToString();
					ret.PotwierdzenieOferty = Enum.IsDefined(typeof(XLStanOferty), Int32.Parse(reader["ZaN_PotwOferty"].ToString())) ? (XLStanOferty?)Enum.Parse(typeof(XLStanOferty), reader["ZaN_PotwOferty"].ToString()) : null;
					ret.Opis = reader["Opis"].ToString();
					ret.Url = reader["ZaN_Url"].ToString();
				}

				var cmdElem = new SqlCommand(@"
					SELECT
						ZaE_GIDLp,
						ZaE_TwrTyp,
						ZaE_TwrNumer,
						ZaE_Ilosc, 
						ZaE_TwrNazwa,
						ISNULL(Twr_Kod, '') AS 'Twr_Kod',
						ZaE_WartoscPoRabacie,
						ISNULL((SELECT TOP 1 ZeO_Opis FROM CDN.ZaEOpisy WHERE ZeO_ZamTyp = ZaE_GIDTyp AND ZeO_ZamNumer = ZaE_GIDNumer AND ZeO_ZamLp = ZaE_GIDLp), '') AS 'Opis',
						ZZL_ZSGidTyp,
						ZZL_ZSGidFirma,
						ZZL_ZSGidNumer,
						ZZL_ZSGidLp
					FROM
						CDN.ZamElem
						LEFT JOIN CDN.TwrKarty
							ON ZaE_TwrNumer = Twr_GIDNumer
						LEFT JOIN CDN.ZamZamLinki
							ON ZaE_GIDTyp = ZZL_ZZGidTyp AND ZaE_GIDNumer = ZZL_ZZGidNumer AND ZaE_GIDLp = ZZL_ZZGidLp
					WHERE
						ZaE_GIDNumer = @GIDNumer
					ORDER BY
						ZaE_Pozycja
				", conn);

				cmdElem.Parameters.AddWithValue("@GIDNumer", gidNumer);

				using (var reader = cmdElem.ExecuteReader())
				{
					while (reader.Read())
					{
						var p = new XLDokZamowienieElem();

						p.GIDTyp = ret.GIDTyp;
						p.GIDNumer = gidNumer;
						p.GIDLp = Int32.Parse(reader["ZaE_GIDLp"].ToString());
						p.Towar.GIDTyp = Int32.Parse(reader["ZaE_TwrTyp"].ToString());
						p.Towar.GIDNumer = Int32.Parse(reader["ZaE_TwrNumer"].ToString());
						p.Towar.TwrNazwa = reader["ZaE_TwrNazwa"].ToString();
						p.Towar.TwrKod = reader["Twr_Kod"].ToString();
						p.Ilosc = Decimal.Parse(reader["ZaE_Ilosc"].ToString());
						p.Wartosc = Decimal.Parse(reader["ZaE_WartoscPoRabacie"].ToString());
						p.Opis = reader["Opis"].ToString();
						p.ZrdFirma = DBNull.Value.Equals(reader["ZZL_ZSGidFirma"]) ? (int?)null : Int32.Parse(reader["ZZL_ZSGidFirma"].ToString());
						p.ZrdTyp = DBNull.Value.Equals(reader["ZZL_ZSGidTyp"]) ? (int?)null : Int32.Parse(reader["ZZL_ZSGidTyp"].ToString());
						p.ZrdNumer = DBNull.Value.Equals(reader["ZZL_ZSGidNumer"]) ? (int?)null : Int32.Parse(reader["ZZL_ZSGidNumer"].ToString());
						p.ZrdLp = DBNull.Value.Equals(reader["ZZL_ZSGidLp"]) ? (int?)null : Int32.Parse(reader["ZZL_ZSGidLp"].ToString());

						ret.Elementy.Add(p);
					}
				}

				var cmdPlat = new SqlCommand(@"
					SELECT
						ZaP_TerminDni,
						ZaP_FormaNr,
						ZaP_Procent,
						ZaP_Kwota,
						ZaP_Pozostaje,
						ZaP_Waluta,
						ZaP_Notatki
					FROM
						CDN.ZamPlat
					WHERE
						ZaP_GIDNumer = @GIDNumer AND ZaP_GIDLp > 0
				", conn);

				cmdPlat.Parameters.AddWithValue("@GIDNumer", gidNumer);

				using (var reader = cmdPlat.ExecuteReader())
				{
					while (reader.Read())
					{
						var p = new XLDokZamowieniePlatnosc();

						p.TerminOffset = Int32.Parse(reader["ZaP_TerminDni"].ToString());
						p.FormaPlatnosci = Int32.Parse(reader["ZaP_FormaNr"].ToString());
						var proc = Decimal.Parse(reader["ZaP_Procent"].ToString());
						if (proc > 0)
						{
							p.KwotaProcent = proc;
						}
						else
						{
							p.KwotaWartosc = Decimal.Parse(reader["ZaP_Kwota"].ToString());
						}
						p.CzyZaliczka = Decimal.Parse(reader["ZaP_Pozostaje"].ToString()) == -1;
						p.Waluta = reader["ZaP_Waluta"].ToString();
						p.Opis = reader["ZaP_Notatki"].ToString();

						ret.Platnosci.Add(p);
					}
				}
			}

			ret.Zalaczniki = ZalacznikiWczytajListe(new XLObiektOgolny { GIDTyp = (int)ret.GIDTyp, GIDNumer = gidNumer }, connStr: connStr);

			return ret;
		}

		public static bool DokZamowienieUzupelnijOsoby(int gidNumer, int idOsoby, string connStr = null)
		{
			bool ret = false;

			using (var conn = new SqlConnection(connStr ?? Helpers.GetSettingsValue("ConnectionString", true)))
			{
				conn.Open();

				var cmdText = @"SELECT ZaN_GIDTyp, ZaN_GIDFirma, ZaN_KntTyp, ZaN_KntNumer FROM CDN.ZamNag WHERE ZaN_GIDNumer = @GIDNumer";

				var cmd = new SqlCommand(cmdText, conn);
				cmd.Parameters.Add("@GIDNumer", SqlDbType.VarChar).Value = gidNumer;

				int idFirmy;
				int gidTyp;
				int kntTyp;
				int kntNumer;

				using (var reader = cmd.ExecuteReader())
				{
					reader.Read();

					idFirmy = Int32.Parse(reader["ZaN_GIDFirma"].ToString());
					gidTyp = Int32.Parse(reader["ZaN_GIDTyp"].ToString());
					kntTyp = Int32.Parse(reader["ZaN_KntTyp"].ToString());
					kntNumer = Int32.Parse(reader["ZaN_KntNumer"].ToString());
				}

				cmdText = @"
					UPDATE CDN.ZamNag SET
						ZaN_KnSTyp = @KntTyp, ZaN_KnSFirma = @Firma, ZaN_KnSNumer = @KntNumer, ZaN_KnSLp = @KntLp,
						ZaN_KnSTypOdb = @KntTyp, ZaN_KnSFirmaOdb = @Firma, ZaN_KnSNumerOdb = @KntNumer, ZaN_KnSLpOdb = @KntLp,
						ZaN_KnSTypPlt = @KntTyp, ZaN_KnSFirmaPlt = @Firma, ZaN_KnSNumerPlt = @KntNumer, ZaN_KnSLpPlt = @KntLp
					WHERE ZaN_GIDTyp = @ZaNGIDTyp AND ZaN_GIDNumer = @ZaNGIDNumer";

				cmd = new SqlCommand(cmdText, conn);
				cmd.Parameters.Add("@KntTyp", SqlDbType.Int).Value = kntTyp;
				cmd.Parameters.Add("@Firma", SqlDbType.Int).Value = idFirmy;
				cmd.Parameters.Add("@KntNumer", SqlDbType.Int).Value = kntNumer;
				cmd.Parameters.Add("@KntLp", SqlDbType.Int).Value = idOsoby;
				cmd.Parameters.Add("@ZaNGIDTyp", SqlDbType.Int).Value = gidTyp;
				cmd.Parameters.Add("@ZaNGIDNumer", SqlDbType.Int).Value = gidNumer;

				if (cmd.ExecuteNonQuery() > 0)
				{
					ret = true;
				}
			}

			return ret;
		}

		public static int DokZamowieniePrzepnijPlatnosci(int zamSrcNumer, int zamDstNumer, string connStr = null)
		{
			using (var conn = new SqlConnection(connStr ?? Helpers.GetSettingsValue("ConnectionString", true)))
			{
				var cmdProc = new SqlCommand(@"
					BEGIN TRANSACTION;

					BEGIN TRY
	
						DECLARE @SrcZaNGIDNumer INT = @ZamSrcNumer
						DECLARE @DstZaNGIDNumer INT = @ZamDstNumer

						IF EXISTS(SELECT ZaN_Stan FROM CDN.ZamNag WHERE ZaN_Stan <> 5 AND ZaN_GIDNumer = @DstZaNGIDNumer)
							RAISERROR('Dokument docelowy musi mieć stan potwierdzony.', 16, 1)

						IF EXISTS(SELECT ZaN_Aktywny FROM CDN.ZamNag WHERE ZaN_Aktywny <> 0 AND ZaN_GIDNumer = @SrcZaNGIDNumer)
							RAISERROR('Dokument źródłowy nie może być edytowany w ERP XL.', 16, 1)

						IF EXISTS(SELECT ZaN_Aktywny FROM CDN.ZamNag WHERE ZaN_Aktywny <> 0 AND ZaN_GIDNumer = @DstZaNGIDNumer)
							RAISERROR('Dokument docelowy nie może być edytowany w ERP XL.', 16, 1)

						IF EXISTS(SELECT TrS_ZlcNumer FROM CDN.TraSElem WHERE TrS_ZlcNumer = @SrcZaNGIDNumer AND TrS_ZlcTyp = 960)
							RAISERROR('Do dokumentu źródłowego zostały wygenerowane dokumenty handlowe.', 16, 1)

						DECLARE @PrzepZal TABLE (
							GIDLp INT,
							TrNGIDTyp INT,
							TrNGIDNumer INT,
							PRIMARY KEY (GIDLp)
						)
						INSERT INTO @PrzepZal
							SELECT ZaP_GIDLp, ZaP_TrNTyp, ZaP_TrNNumer FROM CDN.ZamPlat WHERE ZaP_GIDNumer = @SrcZaNGIDNumer AND ZaP_TrNTyp > 0 AND ZaP_GIDLp > 0

						IF ((SELECT COUNT(ZaP_GIDLp) FROM CDN.ZamPlat INNER JOIN @PrzepZal P ON ZaP_GIDLp = GIDLp AND ZaP_GIDNumer = @DstZaNGIDNumer) <> (SELECT COUNT(GIDLp) FROM @PrzepZal))
						BEGIN
							RAISERROR('Dokumenty źródłowy i docelowy muszą mieć taką samą ilość płatności.', 16, 1)
						END

						DECLARE @TrNGIDTyp INT
						DECLARE @TrNGIDNumer INT

						DECLARE @i INT = ISNULL((SELECT MAX(GIDLp) FROM @PrzepZal), 0)

						WHILE (@i > 0 AND EXISTS(SELECT GIDLp FROM @PrzepZal WHERE GIDLp = @i))
						BEGIN

							SET @TrNGIDTyp = (SELECT TrNGIDTyp FROM @PrzepZal WHERE GIDLp = @i)
							SET @TrNGIDNumer = (SELECT TrNGIDNumer FROM @PrzepZal WHERE GIDLp = @i)

							UPDATE CDN.TraNag SET TrN_ZaNNumer = @DstZaNGIDNumer WHERE TrN_ZaNNumer = @SrcZaNGIDNumer
							UPDATE CDN.ZamPlat SET ZaP_TrNTyp = 0, ZaP_TrNNumer = 0 WHERE ZaP_GIDNumer = @SrcZaNGIDNumer AND ZaP_GIDLp = @i
							UPDATE CDN.ZamPlat SET ZaP_TrNTyp = @TrNGIDTyp, ZaP_TrNNumer = @TrNGIDNumer WHERE ZaP_GIDNumer = @DstZaNGIDNumer AND ZaP_GIDLp = @i
							UPDATE CDN.ZamNag SET ZaN_Stan = 5 WHERE ZaN_GIDNumer = @DstZaNGIDNumer AND ZaN_Stan = 3 -- potwierdzone --> w realizacji

							SET @i = @i - 1

						END

						SELECT COUNT(GIDLp) AS 'Kod', '' AS 'Info' FROM @PrzepZal

					END TRY
					BEGIN CATCH

						SELECT -1 AS 'Kod', RTRIM('Wystąpił błąd przy przepinaniu zaliczek. ' + ERROR_MESSAGE()) AS 'Info'
	
						IF @@TRANCOUNT > 0
							ROLLBACK TRANSACTION;

					END CATCH;

					IF @@TRANCOUNT > 0
						COMMIT TRANSACTION;
				", conn);

				cmdProc.Parameters.AddWithValue("@ZamSrcNumer", zamSrcNumer);
				cmdProc.Parameters.AddWithValue("@ZamDstNumer", zamDstNumer);

				conn.Open();

				using (var reader = cmdProc.ExecuteReader())
				{
					reader.Read();
					var kodWyjscia = Int32.Parse(reader["Kod"].ToString());
					var info = reader["Info"].ToString();
					if (!string.IsNullOrEmpty(info))
					{
						throw new XLApiFatalException(info);
					}
					return kodWyjscia;
				}
			}

		}

	}
}