﻿using System.Globalization;
using cdn_api;
using TaskProcessor.Plugin.ERPXL.Common;

namespace TaskProcessor.Plugin.ERPXL
{
    public partial class Plugin
    {
        public static int ProdTechnologiaUtworz(int idSesji, XLProdukcjaTechnologia obj)
        {
            var prodTech = new XLProdTechnologiaInfo_20141();
            prodTech.Wersja = XLApiWersja;
            
            if (obj.IdWersji != null) prodTech.IDWersji = (int) obj.IdWersji;
            if (obj.Projekt != null) prodTech.Projekt = (int) obj.Projekt;
            if (obj.Rok != null) prodTech.Rok = (int) obj.Rok;
            if (obj.Numer != null) prodTech.Numer = (int) obj.Numer;
            if (obj.Miesiac != null) prodTech.Miesiac = (int) obj.Miesiac;
            if (obj.Typ != null) prodTech.Typ = (int) obj.Typ;
            if (obj.Kod != null) prodTech.Kod = obj.Kod;
            if (obj.Nazwa != null) prodTech.Nazwa = obj.Nazwa;
            if (obj.OpisWersji != null) prodTech.OpisWersji = obj.OpisWersji;
            if (obj.Jednostka != null) prodTech.Jednostka = obj.Jednostka;
            if (obj.Opis != null) prodTech.Opis = obj.Opis;
            if (obj.Seria != null) prodTech.Seria = obj.Seria;
            if (obj.Ilosc != null) prodTech.Ilosc = ((decimal)obj.Ilosc).ToString(CultureInfo.InvariantCulture);
            if (obj.IloscMin != null) prodTech.IloscMin = ((decimal)obj.IloscMin).ToString(CultureInfo.InvariantCulture);
            if (obj.IloscProd != null) prodTech.IloscProd = ((decimal)obj.IloscProd).ToString(CultureInfo.InvariantCulture);
            if (obj.IloscPlan != null) prodTech.IloscPlan = ((decimal)obj.IloscPlan).ToString(CultureInfo.InvariantCulture);
            if (obj.DataZatwierdzenia != null) prodTech.DataZatwierdzenia = obj.DataZatwierdzenia.ToXLDataInt();
            if (obj.DataWystawienia != null) prodTech.DataWystawienia = obj.DataWystawienia.ToXLDataInt();

            if (obj.KontrahentGlowny.GIDTyp != null) prodTech.KntTyp = (int)obj.KontrahentGlowny.GIDTyp;
            if (obj.KontrahentGlowny.GIDNumer != null) prodTech.KntNumer = (int)obj.KontrahentGlowny.GIDNumer;
            if (obj.KontrahentGlowny.GIDNumer == null)
            {
                if (obj.KontrahentGlowny.KntAkronim != null) prodTech.KntAkronim = obj.KontrahentGlowny.KntAkronim;
            }

            if (obj.KontrahentDocelowy.GIDTyp != null) prodTech.KnDTyp = (int)obj.KontrahentDocelowy.GIDTyp;
            if (obj.KontrahentDocelowy.GIDNumer != null) prodTech.KnDNumer = (int)obj.KontrahentDocelowy.GIDNumer;
            if (obj.KontrahentDocelowy.GIDNumer == null)
            {
                if (obj.KontrahentDocelowy.KntAkronim != null) prodTech.KnDAkronim = obj.KontrahentDocelowy.KntAkronim;
            }

            if (obj.Towar.GIDTyp != null) prodTech.TwrTyp = (int) obj.Towar.GIDTyp;
            if (obj.Towar.GIDNumer != null) prodTech.TwrNumer = (int)obj.Towar.GIDNumer;

            int errCode = cdn_api.cdn_api.XLNowaTechnologia(idSesji, prodTech);
            if (errCode != 0)
            {
                throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.NowaTechnologia, errCode), errCode));
            }

            foreach (var czynnosc in obj.Czynnosci)
            {
                czynnosc.IdTechnologii = prodTech.Id;
                var idCzynnosci = ProdCzynnoscDoTechUtworz(idSesji, czynnosc);
                foreach (var zasob in czynnosc.Zasoby)
                {
                    zasob.IdTechnologiaCzynnosc = idCzynnosci;
                    ProdNowyZasobDoCzynnosci(idSesji, zasob);
                }
            }

            return prodTech.Id;
        }

        public static int ProdCzynnoscDoTechUtworz(int idSesji, XLProdukcjaCzynnoscTech obj)
        {
            var prodCzyn = new XLProdTechnologiaCzynnoscInfo_20141();
            prodCzyn.Wersja = XLApiWersja;

            if (obj.IdTechnologii != null) prodCzyn.IdTechnologii = (int) obj.IdTechnologii;
            if (obj.IdOjca != null) prodCzyn.IdOjca = (int) obj.IdOjca;
            if (obj.Ilosc != null) prodCzyn.Ilosc = ((decimal)obj.Ilosc).ToString(CultureInfo.InvariantCulture);
            if (obj.Kod != null) prodCzyn.Kod = obj.Kod;
            if (obj.Nazwa != null) prodCzyn.Nazwa = obj.Nazwa;
            if (obj.Opis != null) prodCzyn.Opis = obj.Opis;
            if (obj.Jednostka != null) prodCzyn.Jednostka = obj.Jednostka;
            if (obj.Towar.GIDTyp != null) prodCzyn.TwrTyp = (int)obj.Towar.GIDTyp;
            if (obj.Towar.GIDNumer != null) prodCzyn.TwrNumer = (int)obj.Towar.GIDNumer;

            int errCode = cdn_api.cdn_api.XLNowaCzynnoscTechnologia(idSesji, prodCzyn);
            if (errCode != 0)
            {
                throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.NowaCzynnoscTechnologia, errCode), errCode));
            }

            return prodCzyn.Id;
        }

        public static int ProdNowyZasobDoCzynnosci(int idSesji, XLProdukcjaZasobCzynnoscTech obj)
        {
            var prodZasob = new XLProdTechnologiaZasobInfo_20141();
            prodZasob.Wersja = XLApiWersja;

            if (obj.IdTechnologiaCzynnosc != null) prodZasob.IdTechnologiaCzynnosc = (int) obj.IdTechnologiaCzynnosc;
            if (obj.TechnologiaZasob != null) prodZasob.TechnologiaZasob = (int) obj.TechnologiaZasob;
            if (obj.ZrodloZasobu != null) prodZasob.ZrodloZasobu = (int) obj.ZrodloZasobu;
            if (obj.TypZasobu != null) prodZasob.TypZasobu = (int) obj.TypZasobu;
            if (obj.Koszt != null) prodZasob.Koszt = (int) obj.Koszt;
            if (obj.WagaIlosc != null) prodZasob.WagaIlosc = (int) obj.WagaIlosc;
            if (obj.Cena != null) prodZasob.Cena = ((decimal)obj.Cena).ToString(CultureInfo.InvariantCulture);
            if (obj.IloscFormat != null) prodZasob.IloscFormat = (int)obj.IloscFormat;
            if (obj.Ilosc != null) prodZasob.Ilosc = ((decimal)obj.Ilosc).ToString(CultureInfo.InvariantCulture);
            if (obj.Cecha != null) prodZasob.Cecha = obj.Cecha;
            if (obj.Jednostka != null) prodZasob.Jednostka = obj.Jednostka;
            if (obj.Towar.GIDTyp != null) prodZasob.TwrTyp = (int)obj.Towar.GIDTyp;
            if (obj.Towar.GIDNumer != null) prodZasob.TwrNumer = (int)obj.Towar.GIDNumer;
            if (obj.Kod != null) prodZasob.Kod = obj.Kod;
            if (obj.Nazwa != null) prodZasob.Nazwa = obj.Nazwa;
            if (obj.Magazyn != null) prodZasob.Magazyn = obj.Magazyn;

            int errCode = cdn_api.cdn_api.XLNowyZasobCzynnoscTechnologia(idSesji, prodZasob);
            if (errCode != 0)
            {
                throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.NowyZasobCzynnoscTechnologia, errCode), errCode));
            }

            return prodZasob.Id;
        }

    }
}
