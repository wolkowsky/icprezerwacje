﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using TaskProcessor.Plugin.ERPXL.Common;
using cdn_api;
using TaskProcessor.Common;

namespace TaskProcessor.Plugin.ERPXL
{
    public partial class Plugin
    {
        public static int RezerwacjaUtworz(int idSesji, XLRezerwacja obj)
        {
            var rez = new XLRezNagInfo_20141();
            rez.Wersja = XLApiWersja;
            int idRez = 0;

            try
            {
                if (obj.Towar.GIDNumer != null) rez.TwrNumer = (int)obj.Towar.GIDNumer;
                if (obj.Towar.GIDTyp != null) rez.TwrTyp = (int)obj.Towar.GIDTyp;
                if (obj.Towar.GIDFirma != null) rez.TwrFirma = (int)obj.Towar.GIDFirma;
                if (obj.Towar.GIDLp != null) rez.TwrLp = (int)obj.Towar.GIDLp;
                if (obj.Kontrahent.GIDTyp != null) rez.KntTyp = (int)obj.Kontrahent.GIDTyp;
                if (obj.Kontrahent.GIDFirma != null) rez.KntFirma = (int)obj.Kontrahent.GIDFirma;
                if (obj.Kontrahent.GIDNumer != null) rez.KntNumer = (int)obj.Kontrahent.GIDNumer;
                if (obj.Kontrahent.GIDLp != null) rez.GIDLp = (int)obj.Kontrahent.GIDLp;
                if (obj.MagTyp != null) rez.MagTyp = (int)obj.MagTyp;
                if (obj.MagFirma != null) rez.MagFirma = (int)obj.MagFirma;
                if (obj.MagNumer != null) rez.MagNumer = (int)obj.MagNumer;
                if (obj.MagLp != null) rez.MagLp = (int)obj.MagLp;
                if (obj.FRSId != null) rez.FrsID = (int)obj.FRSId;
                if (obj.DataRealizacji != null)
                {
                    if (obj.DataRealizacji.ToXLDataIntFromNow() >= 0)
                    {
                        rez.DataRealizacji = obj.DataRealizacji.ToXLDataIntFromNow();
                    }
                    else
                    {
                        throw new XLApiFatalException(string.Format("Data nie może być w przeszłości."));
                    }
                }
                if (obj.DataWaznosci != null)
                {
                    if (obj.DataWaznosci.ToXLDataIntFromNow() >= 0)
                    {
                        rez.DataWaznosci = obj.DataWaznosci.ToXLDataIntFromNow();
                    }
                    else
                    {
                        throw new XLApiFatalException(string.Format("Data nie może być w przeszłości."));
                    }
                }
                if (obj.Priorytet != null) rez.Priorytet = (int)obj.Priorytet;
                if (obj.Towar.TwrKod != null) rez.TwrKod = obj.Towar.TwrKod;
                if (obj.Kontrahent.KntAkronim != null) rez.KntAkronim = obj.Kontrahent.KntAkronim;
                if (obj.Magazyn != null) rez.Magazyn = obj.Magazyn;
                if (obj.Ilosc != null) rez.Ilosc = ((decimal)obj.Ilosc).ToString(CultureInfo.InvariantCulture);

                int errCode = cdn_api.cdn_api.XlDodajRezerwacje(idSesji, ref idRez, rez);
                if (errCode != 0)
                {
                    throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.",
                        InfoOBledzieDodajRezerwacje(errCode), errCode));
                }
                else
                {
                    obj.GIDNumer = rez.GIDNumer;
                    obj.GIDTyp = rez.GIDTyp;
                    obj.GIDFirma = rez.GIDFirma;
                    obj.GIDLp = rez.GIDLp;
                }

                var zas = new XLRezDstInfo_20141();

                zas.Wersja = XLApiWersja;
                if (obj.Zasob.GIDTyp != null) zas.GIDTyp = (int)obj.Zasob.GIDTyp;
                if (obj.Zasob.GIDFirma != null) zas.GIDFirma = (int)obj.Zasob.GIDFirma;
                if (obj.Zasob.GIDNumer != null) zas.GIDNumer = (int)obj.Zasob.GIDNumer;
                if (obj.Zasob.GIDLp != null) zas.GIDLp = (int)obj.Zasob.GIDLp;
                if (obj.Zasob.DstTyp != null) zas.DstTyp = (int)obj.Zasob.DstTyp;
                if (obj.Zasob.DstFirma != null) zas.DstFirma = (int)obj.Zasob.DstFirma;
                if (obj.Zasob.DstNumer != null) zas.DstNumer = (int)obj.Zasob.DstNumer;
                if (obj.Zasob.DstLp != null) zas.DstLp = (int)obj.Zasob.DstLp;
                if (obj.Zasob.MagNumer != null) zas.MagNumer = (int)obj.Zasob.MagNumer;

                if (zas.GIDNumer != 0 || idRez != 0)
                {
                    errCode = cdn_api.cdn_api.XlDodajZasobDoRezerwacji(idSesji, ref idRez, zas);
                    if (errCode != 0)
                    {
                        throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.",
                            InfoOBledzieDodajZasobDoRezerwacji(errCode), errCode));
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is XLApiNonFatalException && ex is XLApiRetryException)
                {
                }
                else
                {
                    if (idRez > 0 || obj.GIDNumer > 0)
                    {
                        var usRez = new XLRezUsunInfo_20141();

                        usRez.Wersja = XLApiWersja;

                        usRez.GIDTyp = rez.GIDTyp;
                        usRez.GIDFirma = rez.GIDFirma;
                        usRez.GIDNumer = rez.GIDFirma;
                        usRez.GIDLp = rez.GIDLp;

                        int errCode = cdn_api.cdn_api.XlUsunRezerwacje(idSesji, ref idRez, usRez);
                        if (errCode != 0)
                        {
                            throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", InfoOBledzieUsunRezerwacje(errCode), errCode));
                        }
                    }
                }
                throw;
            }
            return rez.GIDNumer;
        }

        public static XLRezerwacja RezerwacjaWczytaj(int gidNumer, string connStr = null)
        {
            var ret = new XLRezerwacja();

            using (var conn = new SqlConnection(connStr ?? Helpers.GetSettingsValue("ConnectionString", true)))
            {
                var cmdNag = new SqlCommand(@"
                    SELECT 
                        Rez_GIDTyp,
                        Rez_GIDFirma,
                        Rez_GIDNumer,
                        Rez_GIDLp,
                        Rez_TwrTyp,
                        Rez_TwrFirma,
                        Rez_TwrNumer,
                        Rez_TwrLp,
                        Rez_KntTyp,
                        Rez_KntFirma,
                        Rez_KntNumer,
                        Rez_KntLp,
                        Rez_ZrdTyp,
                        Rez_ZrdFirma,
                        Rez_ZrdNumer,
                        Rez_ZrdLp,
                        Rez_OpeTyp,
                        Rez_OpeFirma,
                        Rez_OpeNumer,
                        Rez_OpeLp,
                        Rez_MagTyp,
                        Rez_MagFirma,
                        Rez_MagNumer,
                        Rez_MagLp,
                        Rez_DstTyp,
                        Rez_DstFirma,
                        Rez_DstNumer,
                        Rez_DstLp,
                        Rez_Ilosc,
                        Rez_Zrealizowano,
                        Rez_IloscMag,
                        Rez_IloscImp,
                        Rez_IloscSSC,
                        Rez_IloscSAD,
                        Rez_TStamp,
                        Rez_DataRealizacji,
                        Rez_DataWaznosci,
                        Rez_DataAktywacji,
                        Rez_Aktywna,
                        Rez_Zrodlo,
                        Rez_DataPotwDst,
                        Rez_FrsID,
                        Rez_Typ,
                        Rez_Priorytet,
                        Rez_DataRezerwacji,
                        Rez_BsSTwrNumer,
                        Rez_BsNID,
                        Rez_BsSRodzaj,
                        Rez_PTZID,
                        Rez_CCHNumer,
                        Rez_Cecha,
                        Rez_Opis
                    FROM
                        CDN.Rezerwacje
                    WHERE
                        Rez_GIDNumer = @GIDNumer", conn);

                cmdNag.Parameters.AddWithValue("@GIDNumer", gidNumer);

                conn.Open();

                using (var reader = cmdNag.ExecuteReader())
                {
                    reader.Read();
                    ret.GIDTyp = Int32.Parse(reader["Rez_GIDTyp"].ToString());
                    ret.GIDFirma = Int32.Parse(reader["Rez_GIDFirma"].ToString());
                    ret.GIDNumer = Int32.Parse(reader["Rez_GIDNumer"].ToString());
                    ret.GIDLp = Int32.Parse(reader["Rez_GIDLp"].ToString());
                    ret.Towar.GIDTyp = Int32.Parse(reader["Rez_TwrTyp"].ToString());
                    ret.Towar.GIDFirma = Int32.Parse(reader["Rez_TwrFirma"].ToString());
                    ret.Towar.GIDNumer = Int32.Parse(reader["Rez_TwrNumer"].ToString());
                    ret.Towar.GIDLp = Int32.Parse(reader["Rez_TwrLp"].ToString());
                    ret.Kontrahent.GIDTyp = Int32.Parse(reader["Rez_KntTyp"].ToString());
                    ret.Kontrahent.GIDFirma = Int32.Parse(reader["Rez_KntFirma"].ToString());
                    ret.Kontrahent.GIDNumer = Int32.Parse(reader["Rez_KntNumer"].ToString());
                    ret.Kontrahent.GIDLp = Int32.Parse(reader["Rez_KntLp"].ToString());
                    ret.MagTyp = Int32.Parse(reader["Rez_MagTyp"].ToString());
                    ret.MagFirma = Int32.Parse(reader["Rez_MagFirma"].ToString());
                    ret.MagNumer = Int32.Parse(reader["Rez_MagNumer"].ToString());
                    ret.MagLp = Int32.Parse(reader["Rez_MagLp"].ToString());
                    ret.Zasob.DstTyp = Int32.Parse(reader["Rez_DstTyp"].ToString());
                    ret.Zasob.DstFirma = Int32.Parse(reader["Rez_DstFirma"].ToString());
                    ret.Zasob.DstTyp = Int32.Parse(reader["Rez_DstNumer"].ToString());
                    ret.Zasob.DstTyp = Int32.Parse(reader["Rez_DstLp"].ToString());
                    ret.Ilosc = Decimal.Parse(reader["Rez_Ilosc"].ToString());
                    ret.Rez_Zrealizowano = Decimal.Parse(reader["Rez_Zrealizowano"].ToString());
                    ret.Rez_IloscMag = Decimal.Parse(reader["Rez_IloscMag"].ToString());
                    ret.Rez_IloscImp = Decimal.Parse(reader["Rez_IloscImp"].ToString());
                    ret.Rez_IloscSSC = Decimal.Parse(reader["Rez_IloscSSC"].ToString());
                    ret.Rez_IloscSAD = Decimal.Parse(reader["Rez_IloscSAD"].ToString());
                    ret.Rez_TStamp = Int32.Parse(reader["Rez_TStamp"].ToString()).ToDateTimeFromXLCzas();
                    ret.DataRealizacji = Int32.Parse(reader["Rez_DataRealizacji"].ToString()).ToDateTimeFromXLDataInt();
                    ret.DataWaznosci = Int32.Parse(reader["Rez_DataWaznosci"].ToString()).ToDateTimeFromXLDataInt();
                    ret.Rez_Aktywna = reader["Rez_Aktywna"].ToString() == "1";
                    ret.Rez_ZrodloRezerwacji = Enum.IsDefined(typeof(XLPochodzenieRezerwacji), reader["Rez_Zrodlo"].ToString()) ? (XLPochodzenieRezerwacji?)Enum.Parse(typeof(XLPochodzenieRezerwacji), reader["Rez_Zrodlo"].ToString()) : null;
                    ret.Rez_ZrdTyp = Int32.Parse(reader["Rez_ZrdTyp"].ToString());
                    ret.Rez_ZrdFirma = Int32.Parse(reader["Rez_ZrdFirma"].ToString());
                    ret.Rez_ZrdNumer = Int32.Parse(reader["Rez_ZrdNumer"].ToString());
                    ret.Rez_ZrdLp = Int32.Parse(reader["Rez_ZrdLp"].ToString());
                    ret.Rez_OpeTyp = Int32.Parse(reader["Rez_OpeTyp"].ToString());
                    ret.Rez_OpeFirma = Int32.Parse(reader["Rez_OpeFirma"].ToString());
                    ret.Rez_OpeNumer = Int32.Parse(reader["Rez_OpeNumer"].ToString());
                    ret.Rez_OpeLp = Int32.Parse(reader["Rez_OpeLp"].ToString());
                    ret.Priorytet = Int32.Parse(reader["Rez_Priorytet"].ToString());
                    ret.FRSId = Int32.Parse(reader["Rez_FRSId"].ToString());
                }
            }

            return ret;
        }

        public static int RezerwacjaUsun(int idSesji, XLRezerwacja obj)
        {
            var usRez = new XLRezUsunInfo_20141();

            int IDRez = 0;

            usRez.Wersja = XLApiWersja;

            if (obj.GIDTyp != null) usRez.GIDTyp = (int)obj.GIDTyp;
            if (obj.GIDFirma != null) usRez.GIDFirma = (int)obj.GIDFirma;
            if (obj.GIDNumer != null) usRez.GIDNumer = (int)obj.GIDNumer;
            if (obj.GIDLp != null) usRez.GIDLp = (int)obj.GIDLp;

            int errCode = cdn_api.cdn_api.XlUsunRezerwacje(idSesji, ref IDRez, usRez);
            if (errCode != 0)
            {
                throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", InfoOBledzieUsunRezerwacje(errCode), errCode));
            }

            return (int)obj.GIDNumer;
        }
    }
}
