﻿using cdn_api;
using TaskProcessor.Plugin.ERPXL.Common;

namespace TaskProcessor.Plugin.ERPXL
{
    public partial class Plugin
    {
        public static int KontrahentAdresUtworz(int idSesji, XLKontrahentAdres obj, string connStr = null)
        {
            int idAdr = 0;

            var adr = new XLAdresInfo_20141();
            adr.Wersja = XLApiWersja;
            if (obj.GIDTyp != null) adr.GIDTyp = (int) obj.GIDTyp;
            if (obj.GIDFirma != null) adr.GIDFirma = (int) obj.GIDFirma;
            if (obj.GIDNumer != null) adr.GIDNumer = (int) obj.GIDNumer;
            if (obj.GIDLp != null) adr.GIDLp = (int) obj.GIDLp;
            if (obj.Typ != null) adr.Typ = (int) obj.Typ;
            if (obj.Wysylkowy != null) adr.Wysylkowy = (int) obj.Wysylkowy;
            if (obj.Tryb != null) adr.Tryb = (int) obj.Tryb;
            if (obj.Flagi != null) adr.Flagi = (int) obj.Flagi;
            if (obj.KntTyp != null) adr.KntTyp = (int) obj.KntTyp;
            if (obj.KntFirma != null) adr.KntFirma = (int) obj.KntFirma;
            if (obj.KntNumer != null) adr.KntNumer = (int) obj.KntNumer;
            if (obj.KntLp != null) adr.KntLp = (int) obj.KntLp;
            if (obj.NRB != null) adr.NRB = (int) obj.NRB;
            if (obj.RegionCRM != null) adr.RegionCRM = (int) obj.RegionCRM;
            if (obj.NiePublikuj != null) adr.NiePublikuj = (int) obj.NiePublikuj;
            if (obj.Akronim != null) adr.Akronim = obj.Akronim;
            if (obj.Nazwa1 != null) adr.Nazwa1 = obj.Nazwa1;
            if (obj.Nazwa2 != null) adr.Nazwa2 = obj.Nazwa2;
            if (obj.Nazwa3 != null) adr.Nazwa3 = obj.Nazwa3;
            if (obj.KodP != null) adr.KodP = obj.KodP;
            if (obj.Miasto != null) adr.Miasto = obj.Miasto;
            if (obj.Ulica != null) adr.Ulica = obj.Ulica;
            if (obj.Adres != null) adr.Adres = obj.Adres;
            if (obj.NipPrefiks != null) adr.NipPrefiks = obj.NipPrefiks;
            if (obj.NipE != null) adr.NipE = obj.NipE;
            if (obj.Regon != null) adr.Regon = obj.Regon;
            if (obj.Pesel != null) adr.Pesel = obj.Pesel;
            if (obj.Bank != null) adr.Bank = obj.Bank;
            if (obj.NrRachunku != null) adr.NrRachunku = obj.NrRachunku;
            if (obj.Odleglosc != null) adr.Odleglosc = obj.Odleglosc;
            if (obj.Kraj != null) adr.Kraj = obj.Kraj;
            if (obj.Wojewodztwo != null) adr.Wojewodztwo = obj.Wojewodztwo;
            if (obj.Powiat != null) adr.Powiat = obj.Powiat;
            if (obj.Gmina != null) adr.Gmina = obj.Gmina;
            if (obj.Telefon1 != null) adr.Telefon1 = obj.Telefon1;
            if (obj.Telefon2 != null) adr.Telefon2 = obj.Telefon2;
            if (obj.Fax != null) adr.Fax = obj.Fax;
            if (obj.Modem != null) adr.Modem = obj.Modem;
            if (obj.Telex != null) adr.Telex = obj.Telex;
            if (obj.EMail != null) adr.EMail = obj.EMail;
            if (obj.KontoDostawcy != null) adr.KontoDostawcy = obj.KontoDostawcy;
            if (obj.KontoOdbiorcy != null) adr.KontoOdbiorcy = obj.KontoOdbiorcy;

            int errCode = cdn_api.cdn_api.XLNowyAdres(idSesji, ref idAdr, adr);
            if (errCode != 0)
            {
                throw new XLApiRetryException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.NowyAdres, errCode), errCode));
            }
            
            var adrZamk = new XLZamkniecieAdresuInfo_20141 { Wersja = XLApiWersja, Tryb = (int)XLTrybZamknieciaAdresu.Zapisanie };
            errCode = cdn_api.cdn_api.XLZamknijAdres(idAdr, adrZamk);
            if (errCode != 0)
            {
                try
                {
                    cdn_api.cdn_api.XLZamknijAdres(idAdr, new XLZamkniecieAdresuInfo_20141 { Wersja = XLApiWersja, Tryb = (int)XLTrybZamknieciaAdresu.Usuniecie });
                }
                catch
                {
                }
                throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.ZamknijAdres, errCode), errCode));
            }

            return adrZamk.GidLp;
        }

        public static int KontrahentAdresModyfikuj(XLKontrahentAdres obj, string connStr = null)
        {
            var adr = new XLAdresInfo_20141();
            adr.Wersja = XLApiWersja;
            if (obj.GIDTyp != null) adr.GIDTyp = (int)obj.GIDTyp;
            if (obj.GIDFirma != null) adr.GIDFirma = (int)obj.GIDFirma;
            if (obj.GIDNumer != null) adr.GIDNumer = (int)obj.GIDNumer;
            if (obj.GIDLp != null) adr.GIDLp = (int)obj.GIDLp;
            if (obj.Typ != null) adr.Typ = (int)obj.Typ;
            if (obj.Wysylkowy != null) adr.Wysylkowy = (int)obj.Wysylkowy;
            if (obj.Tryb != null) adr.Tryb = (int)obj.Tryb;
            if (obj.Flagi != null) adr.Flagi = (int)obj.Flagi;
            if (obj.KntTyp != null) adr.KntTyp = (int)obj.KntTyp;
            if (obj.KntFirma != null) adr.KntFirma = (int)obj.KntFirma;
            if (obj.KntNumer != null) adr.KntNumer = (int)obj.KntNumer;
            if (obj.KntLp != null) adr.KntLp = (int)obj.KntLp;
            if (obj.NRB != null) adr.NRB = (int)obj.NRB;
            if (obj.RegionCRM != null) adr.RegionCRM = (int)obj.RegionCRM;
            if (obj.NiePublikuj != null) adr.NiePublikuj = (int)obj.NiePublikuj;
            if (obj.Akronim != null) adr.Akronim = obj.Akronim;
            if (obj.Nazwa1 != null) adr.Nazwa1 = obj.Nazwa1;
            if (obj.Nazwa2 != null) adr.Nazwa2 = obj.Nazwa2;
            if (obj.Nazwa3 != null) adr.Nazwa3 = obj.Nazwa3;
            if (obj.KodP != null) adr.KodP = obj.KodP;
            if (obj.Miasto != null) adr.Miasto = obj.Miasto;
            if (obj.Ulica != null) adr.Ulica = obj.Ulica;
            if (obj.Adres != null) adr.Adres = obj.Adres;
            if (obj.NipPrefiks != null) adr.NipPrefiks = obj.NipPrefiks;
            if (obj.NipE != null) adr.NipE = obj.NipE;
            if (obj.Regon != null) adr.Regon = obj.Regon;
            if (obj.Pesel != null) adr.Pesel = obj.Pesel;
            if (obj.Bank != null) adr.Bank = obj.Bank;
            if (obj.NrRachunku != null) adr.NrRachunku = obj.NrRachunku;
            if (obj.Odleglosc != null) adr.Odleglosc = obj.Odleglosc;
            if (obj.Kraj != null) adr.Kraj = obj.Kraj;
            if (obj.Wojewodztwo != null) adr.Wojewodztwo = obj.Wojewodztwo;
            if (obj.Powiat != null) adr.Powiat = obj.Powiat;
            if (obj.Gmina != null) adr.Gmina = obj.Gmina;
            if (obj.Telefon1 != null) adr.Telefon1 = obj.Telefon1;
            if (obj.Telefon2 != null) adr.Telefon2 = obj.Telefon2;
            if (obj.Fax != null) adr.Fax = obj.Fax;
            if (obj.Modem != null) adr.Modem = obj.Modem;
            if (obj.Telex != null) adr.Telex = obj.Telex;
            if (obj.EMail != null) adr.EMail = obj.EMail;
            if (obj.KontoDostawcy != null) adr.KontoDostawcy = obj.KontoDostawcy;
            if (obj.KontoOdbiorcy != null) adr.KontoOdbiorcy = obj.KontoOdbiorcy;

            int errCode = cdn_api.cdn_api.XLZmienAdres(adr);
            if (errCode != 0)
            {
                throw new XLApiRetryException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.ZmienAdres, errCode), errCode));
            }

            return adr.GIDLp;
        }
        
    }
}