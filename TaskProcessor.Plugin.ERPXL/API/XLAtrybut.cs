﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using cdn_api;
using TaskProcessor.Common;
using TaskProcessor.Plugin.ERPXL.Common;

namespace TaskProcessor.Plugin.ERPXL
{
    public partial class Plugin
    {
        public static int AtrybutDodaj(int idSesji, XLAtrybut obj, string connStr = null)
        {
            return AtrybutDodajListe(idSesji, new List<XLAtrybut> { obj }, connStr);
        }

        public static int AtrybutDodajListe(int idSesji, List<XLAtrybut> listObj, string connStr = null)
        {
            foreach (var obj in listObj)
            {
                var defObj = new XLAtrybutInfo_20141();
                defObj.Wersja = XLApiWersja;
                defObj.Klasa = obj.Klasa;
                defObj.Wartosc = obj.Wartosc;
                defObj.GIDTyp = (int) obj.GIDTyp;
                defObj.GIDNumer = (int) obj.GIDNumer;
                if (obj.GIDLp != null) defObj.GIDLp = (int) obj.GIDLp;
                if (obj.GIDSubLp != null) defObj.GIDSubLp = (int) obj.GIDSubLp;
                if (obj.GIDFirma != null) defObj.GIDFirma = (int) obj.GIDFirma;
                if (obj.ZamTyp != null) defObj.ZamTyp = (int) obj.ZamTyp;
                if (obj.ZamRodzaj != null) defObj.ZamRodzaj = (int) obj.ZamRodzaj;

                int errCode = cdn_api.cdn_api.XLDodajAtrybut(idSesji, defObj);
                if (errCode != 0)
                {
                    if (listObj.Count == 1)
                    {
                        throw new XLApiRetryException(string.Format("{0}, GIDTyp = {1}, GIDNumer = {2}, Klasa = {3}, Kod błędu = {4}.", InfoOBledzieDodajAtrybut(errCode), obj.GIDTyp, obj.GIDNumer, obj.Klasa, errCode));
                    }
                    else
                    {
                        throw new XLApiFatalException(string.Format("{0}, GIDTyp = {1}, GIDNumer = {2}, Klasa = {3}, Kod błędu = {4}.", InfoOBledzieDodajAtrybut(errCode), obj.GIDTyp, obj.GIDNumer, obj.Klasa, errCode));
                    }
                }
            }

            return 1;
        }

        public static int AtrybutModyfikuj(XLAtrybut obj, string connStr = null)
        {
            return AtrybutModyfikujListe(new List<XLAtrybut>() { obj }, connStr);
        }

        public static int AtrybutModyfikujListe(List<XLAtrybut> objList, string connStr = null)
        {
            using (var conn = new SqlConnection(connStr ?? Helpers.GetSettingsValue("ConnectionString", true)))
            {
                conn.Open();

                foreach (XLAtrybut atrybut in objList)
                {
                    var sql = "UPDATE CDN.Atrybuty SET Atr_Wartosc = @WartoscAtrybutu WHERE Atr_AtkId = ISNULL((SELECT TOP 1 AtK_ID FROM CDN.AtrybutyKlasy WHERE AtK_Nazwa = @KlasaAtrybutu), -1) AND Atr_ObiNumer = @GIDNumer AND Atr_ObiTyp = @GIDTyp";
                    if (atrybut.GIDLp != null) sql += " AND Atr_ObiLp = @GIDLp";
                    if (atrybut.GIDSubLp != null) sql += " AND Atr_ObiSubLp = @GIDSubLp";

                    var cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@KlasaAtrybutu", atrybut.Klasa);
                    cmd.Parameters.AddWithValue("@WartoscAtrybutu", atrybut.Wartosc);
                    cmd.Parameters.AddWithValue("@GIDNumer", atrybut.GIDNumer);
                    cmd.Parameters.AddWithValue("@GIDTyp", atrybut.GIDTyp);
                    if (atrybut.GIDLp != null) cmd.Parameters.AddWithValue("@GIDLp", atrybut.GIDLp);
                    if (atrybut.GIDSubLp != null) cmd.Parameters.AddWithValue("@GIDSubLp", atrybut.GIDSubLp);
                    
                    var rowsAff = cmd.ExecuteNonQuery();
                    if (rowsAff < 1)
                    {
                        throw new XLApiFatalException(string.Format("Wystąpił błąd przy próbie modyfikacji atrybutu [ObiTyp = {0}, ObiNumer = {1}, KlasaAtrybutu = {2}, WartoscAtrybutu = {3}].", atrybut.GIDTyp, atrybut.GIDNumer, atrybut.Klasa, atrybut.Wartosc));
                    }
                }

                return 1;
            }
        }

        public static string AtrybutPobierzWartosc(XLAtrybut obj, string connStr = null)
        {
            var atrs = AtrybutWczytajListe(obj, connStr);
            if (atrs.Any())
            {
                return atrs.First().Wartosc;
            }
            else
            {
                throw new XLApiFatalException(string.Format("Wystąpił błąd przy próbie pobrania wartości atrybutu [ObiTyp = {0}, ObiNumer = {1}, KlasaAtrybutu = {2}].", obj.GIDTyp, obj.GIDNumer, obj.Klasa));
            }
        }

        public static List<XLAtrybut> AtrybutWczytajListe(XLAtrybut obj, string connStr = null)
        {
            var ret = new List<XLAtrybut>();

            using (var conn = new SqlConnection(connStr ?? Helpers.GetSettingsValue("ConnectionString", true)))
            {
                var cmdSql = "SELECT AtK_Nazwa, Atr_Wartosc FROM CDN.Atrybuty INNER JOIN CDN.AtrybutyKlasy ON AtK_ID = Atr_AtkId WHERE Atr_ObiNumer = @GIDNumer AND Atr_ObiTyp = @GIDTyp";
                if (obj.GIDLp != null) cmdSql += " AND Atr_ObiLp = @GIDLp";
                if (obj.GIDSubLp != null) cmdSql += " AND Atr_ObiSubLp = @GIDSubLp";
                if (obj.Klasa != null) cmdSql += " AND AtK_Nazwa = @KlasaAtrybutu";

                var cmd = new SqlCommand(cmdSql, conn);
                cmd.Parameters.AddWithValue("@GIDNumer", obj.GIDNumer);
                cmd.Parameters.AddWithValue("@GIDTyp", obj.GIDTyp);
                if (obj.GIDLp != null) cmd.Parameters.AddWithValue("@GIDLp", obj.GIDLp);
                if (obj.GIDSubLp != null) cmd.Parameters.AddWithValue("@GIDSubLp", obj.GIDSubLp);
                if (obj.Klasa != null) cmd.Parameters.AddWithValue("@KlasaAtrybutu", obj.Klasa);

                conn.Open();
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        ret.Add(new XLAtrybut
                        {
                            GIDNumer = obj.GIDNumer,
                            GIDTyp = obj.GIDTyp,
                            GIDLp = obj.GIDLp,
                            GIDSubLp = obj.GIDSubLp,
                            GIDFirma = obj.GIDFirma,
                            Klasa = reader["AtK_Nazwa"].ToString(),
                            Wartosc = reader["Atr_Wartosc"].ToString(),
                        });
                    }
                }
            }

            return ret;
        }

    }
}
