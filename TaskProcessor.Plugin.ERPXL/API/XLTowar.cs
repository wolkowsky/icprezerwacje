﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using cdn_api;
using TaskProcessor.Common;
using TaskProcessor.Plugin.ERPXL.Common;

namespace TaskProcessor.Plugin.ERPXL
{
    public partial class Plugin
    {
        public static int TowarUtworz(int idSesji, XLTowar obj)
        {
            var errMsg = new List<string>();
            int twrID = 0;
            int twrNumer = 0;

            var twr = new XLTowarInfo_20141();
            twr.Wersja = XLApiWersja;
            twr.Typ = (int)obj.ApiTyp;

            if (obj.CenaSpr != null) twr.CenaSpr = (int)obj.CenaSpr;

            if (obj.Cecha.GIDTyp != null) twr.CCkTyp = (int)obj.Cecha.GIDTyp;
            if (obj.Cecha.GIDNumer != null) twr.CCkNumer = (int)obj.Cecha.GIDNumer;
            if (obj.Cecha.GIDFirma != null) twr.CCkFirma = (int)obj.Cecha.GIDFirma;
            if (obj.Cecha.GIDLp != null) twr.CCkLp = (int)obj.Cecha.GIDLp;
            if (obj.Cecha.KlasaCechy != null) twr.KlasaCechy = obj.Cecha.KlasaCechy;
            if (obj.Cecha.CechaSprzedaz != null) twr.CechaSprzedaz = (bool)obj.Cecha.CechaSprzedaz ? 1 : 0;
            if (obj.Cecha.CechaZakup != null) twr.CechaZakup = (bool)obj.Cecha.CechaZakup ? 1 : 0;

            if (obj.KosztUTyp != null) twr.KosztUTyp = (int)obj.KosztUTyp;

            if (obj.TwGTyp != null) twr.TwGTyp = (int)obj.TwGTyp;
            if (obj.TwGNumer != null) twr.TwGNumer = (int)obj.TwGNumer;
            if (obj.TwGLp != null) twr.TwGLp = (int)obj.TwGLp;
            if (obj.TwrGrupa != null) twr.TwrGrupa = obj.TwrGrupa;

            if (obj.RozliczMag != null) twr.RozliczMag = (int)obj.RozliczMag;
            if (obj.KgoId != null) twr.KgoId = (int)obj.KgoId;
            if (obj.FlagaVATZak != null) twr.FlagaVat = (int)obj.FlagaVATZak;
            if (obj.FlagaVATSpr != null) twr.FlagaVatSpr = (int)obj.FlagaVATSpr;
            if (obj.Koncesja != null) twr.Koncesja = (int)obj.Koncesja;
            if (obj.Zlom != null) twr.Zlom = (int)obj.Zlom;
            if (obj.Kaucja != null) twr.Kaucja = (int)obj.Kaucja;
            if (obj.IdSchematuKaucji != null) twr.SKNId = (int)obj.IdSchematuKaucji;
            if (obj.JmCalkowita != null) twr.JMCalkowita = (int)obj.JmCalkowita;
            if (obj.TwrKod != null) twr.Kod = obj.TwrKod;
            if (obj.TwrNazwa != null) twr.Nazwa = obj.TwrNazwa;
            if (obj.TwrSWW != null) twr.SWW = obj.TwrSWW;
            if (obj.TwrEAN != null) twr.EAN = obj.TwrEAN;
            if (obj.Jm != null) twr.Jm = obj.Jm;
            if (obj.GrupaPodZak != null) twr.GrupaPod = obj.GrupaPodZak;
            if (obj.GrupaPodSpr != null) twr.GrupaPodSpr = obj.GrupaPodSpr;
            if (obj.Symbol != null) twr.Symbol = obj.Symbol;
            if (obj.Magazyn != null) twr.Magazyn = obj.Magazyn;
            if (obj.MarzaMin != null) twr.MarzaMin = ((decimal)obj.MarzaMin).ToString(CultureInfo.InvariantCulture);
            if (obj.KosztUslugi != null) twr.KosztUslugi = ((decimal)obj.KosztUslugi).ToString(CultureInfo.InvariantCulture);
            if (obj.Akcyza != null) twr.Akcyza = ((decimal)obj.Akcyza).ToString(CultureInfo.InvariantCulture);
            if (obj.Clo != null) twr.Clo = ((decimal)obj.Clo).ToString(CultureInfo.InvariantCulture);
            if (obj.Opis != null) twr.Opis = obj.Opis;
            if (obj.KgoKod != null) twr.KgoKod = obj.KgoKod;
            if (obj.StawkaPodZak != null) twr.StawkaPod = ((decimal)obj.StawkaPodZak).ToString(CultureInfo.InvariantCulture);
            if (obj.StawkaPodSpr != null) twr.StawkaPodSpr = ((decimal)obj.StawkaPodSpr).ToString(CultureInfo.InvariantCulture);
            if (obj.ZrodlowaZak != null) twr.Zrodlowa = obj.ZrodlowaZak;
            if (obj.ZrodlowaSpr != null) twr.ZrodlowaSpr = obj.ZrodlowaSpr;
            if (obj.FppKod != null) twr.FPPKod = obj.FppKod;
            if (obj.SchematKaucji != null) twr.SchematKaucji = obj.SchematKaucji;

            int errCode = cdn_api.cdn_api.XLNowyTowar(idSesji, ref twrID, twr);
            if (errCode == 0)
            {
                twrNumer = twr.GIDNumer;
                try
                {
                    var zamTwr = new XLZamkniecieTowaruInfo_20141();

                    zamTwr.Wersja = XLApiWersja;
                    zamTwr.Tryb = (int)XLTrybZamknieciaTowaru.Zamknij;

                    errCode = cdn_api.cdn_api.XLZamknijTowar(twrID, zamTwr);
                    if (errCode != 0)
                    {
                        throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.ZamknijTowar, errCode), errCode));
                    }
                }
                catch (Exception ex)
                {
                    errMsg.Add(ex.Message);
                }
            }
            else
            {
                throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.NowyTowar, errCode), errCode));
            }

            foreach (var cena in obj.Ceny)
            {
                try
                {
                    var zmCene = new XLCenaInfo_20141();
                    zmCene.Wersja = XLApiWersja;

                    zmCene.GIDTyp = 16;
                    zmCene.GIDNumer = twrNumer;
                    if (cena.GIDFirma != null) zmCene.GIDFirma = (int)cena.GIDFirma;
                    if (cena.GIDLp != null) zmCene.GIDLp = (int)cena.GIDLp;
                    if (cena.Numer != null) zmCene.Numer = (int)cena.Numer;
                    if (cena.NrKursu != null) zmCene.NrKursu = (int)cena.NrKursu;
                    if (cena.Aktualizacja != null) zmCene.Aktualizacja = (bool)cena.Aktualizacja ? 1 : 0;
                    if (cena.Kod != null) zmCene.Kod = cena.Kod;
                    if (cena.Waluta != null) zmCene.Waluta = cena.Waluta;
                    if (cena.Wartosc != null) zmCene.Wartosc = ((decimal)cena.Wartosc).ToString(CultureInfo.InvariantCulture);
                    if (cena.Marza != null) zmCene.Marza = ((decimal)cena.Marza).ToString(CultureInfo.InvariantCulture);
                    if (cena.Zaok != null) zmCene.Zaok = cena.GIDTyp.ToString();
                    if (cena.Offset != null) zmCene.Offset = cena.Offset;
                    
                    errCode = cdn_api.cdn_api.XLZmienCene(zmCene);
                    if (errCode != 0)
                    {
                        throw new XLApiNonFatalException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.ZmienCene, errCode), errCode));
                    }
                }
                catch (Exception ex)
                {
                    errMsg.Add(ex.Message);
                }
            }
            if (errMsg.Any())
            {
                throw new XLApiNonFatalException(string.Join("; ", errMsg));
            }

            return twr.GIDNumer;
        }


        public static XLTowar TowarWczytaj(int gidNumer, string connStr = null)
        {
            var ret = new XLTowar() { TrybZamkniecia = null };

            using (var conn = new SqlConnection(connStr ?? Helpers.GetSettingsValue("ConnectionString", true)))
            {
                var cmdNag = new SqlCommand(@"                    
                    SELECT
                        Twr_GIDTyp,
                        Twr_GIDFirma,
                        Twr_GIDLp,
                        Twr_Typ,
                        Twr_Kod,
                        Twr_Nazwa,
                        Twr_Nazwa1,
                        Twr_Sww,
                        Twr_Ean,
                        Twr_Jm,
                        Twr_CenaSpr,
                        Twr_RozliczMag,
                        Twr_GrupaPod,
                        Twr_Akcyza,
                        Twr_Koncesja,
                        Twr_KosztUslugi,
                        Twr_KosztUTyp,
                        Twr_Clo,
                        Twr_PodatekImp,
                        Twr_Aktywna,
                        Twr_CCKTyp,
                        Twr_CCKFirma,
                        Twr_CCKNumer,
                        Twr_CCKLp,
                        Twr_OpeTypM,
                        Twr_OpeFirmaM,
                        Twr_OpeNumerM,
                        Twr_OpeLpM,
                        Twr_StawkaPod,
                        Twr_FlagaVat,
                        Twr_Zrodlowa,
                        Twr_GrupaPodSpr,
                        Twr_StawkaPodSpr,
                        Twr_FlagaVatSpr,
                        Twr_ZrodlowaSpr,
                        Twr_Zlom
                    FROM
                        CDN.TwrKarty
                    WHERE
                        Twr_GIDNumer = @GIDNumer", conn);

                cmdNag.Parameters.AddWithValue("@GIDNumer", gidNumer);

                conn.Open();

                using (var reader = cmdNag.ExecuteReader())
                {
                    reader.Read();

                    ret.GIDNumer = gidNumer;
                    ret.GIDTyp = Int32.Parse(reader["Twr_GIDTyp"].ToString());
                    ret.GIDFirma = Int32.Parse(reader["Twr_GIDFirma"].ToString());
                    ret.GIDLp = Int32.Parse(reader["Twr_GIDLp"].ToString());
                    ret.ApiTyp = Int32.Parse(reader["Twr_Typ"].ToString());
                    ret.TwrKod = reader["Twr_Kod"].ToString();
                    ret.TwrNazwa = reader["Twr_Nazwa"] + " " + reader["Twr_Nazwa1"];
                    ret.TwrSWW = reader["Twr_Sww"].ToString();
                    ret.TwrEAN = reader["Twr_Ean"].ToString();
                    ret.Jm = reader["Twr_Jm"].ToString();
                    ret.RozliczMag = Enum.IsDefined(typeof(XLTwrRozliczMagazyn), reader["Twr_RozliczMag"].ToString()) ? (XLTwrRozliczMagazyn?)Enum.Parse(typeof(XLTwrRozliczMagazyn), reader["Twr_RozliczMag"].ToString()) : null;
                    ret.GrupaPodZak = reader["Twr_GrupaPod"].ToString();
                    ret.Akcyza = Decimal.Parse(reader["Twr_Akcyza"].ToString());
                    ret.Koncesja = Int32.Parse(reader["Twr_Koncesja"].ToString());
                    ret.KosztUslugi = Decimal.Parse(reader["Twr_KosztUslugi"].ToString());
                    ret.KosztUTyp = Enum.IsDefined(typeof(XLTwrKosztUslugiTyp), reader["Twr_KosztUTyp"].ToString()) ? (XLTwrKosztUslugiTyp?)Enum.Parse(typeof(XLTwrKosztUslugiTyp), reader["Twr_KosztUTyp"].ToString()) : null;
                    ret.Clo = Decimal.Parse(reader["Twr_Clo"].ToString());
                    ret.Cecha.GIDTyp = Int32.Parse(reader["Twr_CCKTyp"].ToString());
                    ret.Cecha.GIDFirma = Int32.Parse(reader["Twr_CCKFirma"].ToString());
                    ret.Cecha.GIDNumer = Int32.Parse(reader["Twr_CCKNumer"].ToString());
                    ret.Cecha.GIDLp = Int32.Parse(reader["Twr_CCKLp"].ToString());
                    ret.StawkaPodZak = Decimal.Parse(reader["Twr_StawkaPod"].ToString());
                    ret.FlagaVATZak = Enum.IsDefined(typeof(XLFlagaVAT), reader["Twr_FlagaVat"].ToString()) ? (XLFlagaVAT?)Enum.Parse(typeof(XLFlagaVAT), reader["Twr_FlagaVat"].ToString()) : null;
                    ret.ZrodlowaZak = reader["Twr_Zrodlowa"].ToString();
                    ret.GrupaPodSpr = reader["Twr_GrupaPodSpr"].ToString();
                    ret.StawkaPodSpr = Decimal.Parse(reader["Twr_StawkaPodSpr"].ToString());
                    ret.FlagaVATSpr = Enum.IsDefined(typeof(XLFlagaVAT), reader["Twr_FlagaVatSpr"].ToString()) ? (XLFlagaVAT?)Enum.Parse(typeof(XLFlagaVAT), reader["Twr_FlagaVatSpr"].ToString()) : null;
                    ret.ZrodlowaSpr = reader["Twr_ZrodlowaSpr"].ToString();
                    ret.Zlom = Enum.IsDefined(typeof(XLTwrZlom), reader["Twr_Zlom"].ToString()) ? (XLTwrZlom?)Enum.Parse(typeof(XLTwrZlom), reader["Twr_Zlom"].ToString()) : null;
                }

                var cmdCeny = new SqlCommand(@"
                    SELECT 
                        TwC_TwrTyp,
                        TwC_TwrFirma,
                        TwC_TwrNumer,
                        TwC_TwrLp,
                        TwC_Waluta,
                        TwC_NrKursu,
                        TwC_Marza,
                        TwC_Wartosc,
                        TwC_Zaok,
                        TwC_Offset,
                        TwC_Aktualizacja,
                        TwC_Priorytet,
                        TwC_CzasModyfikacji,
                        TwC_KosztyDodatkowe,
                        TwC_DokTyp,
                        TwC_DokFirma,
                        TwC_DokNumer,
                        TwC_DokLp
                    FROM
                        CDN.TwrCeny
                    WHERE
                        TwC_TwrNumer = @GIDNumer", conn);

                cmdCeny.Parameters.AddWithValue("@GIDNumer", gidNumer);

                using (var reader = cmdCeny.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var p = new XLCena();
                        p.GIDTyp = Int32.Parse(reader["TwC_TwrTyp"].ToString());
                        p.GIDFirma = Int32.Parse(reader["TwC_TwrFirma"].ToString());
                        p.GIDNumer = Int32.Parse(reader["TwC_TwrNumer"].ToString());
                        p.GIDLp = Int32.Parse(reader["TwC_TwrLp"].ToString());
                        p.Waluta = reader["Twc_Waluta"].ToString();
                        p.NrKursu = Int32.Parse(reader["TwC_NrKursu"].ToString());
                        p.Marza = Decimal.Parse(reader["TwC_Marza"].ToString());
                        p.Wartosc = Decimal.Parse(reader["TwC_Wartosc"].ToString());
                        p.Zaok = reader["TwC_Zaok"].ToString();
                        p.Offset = reader["TwC_Offset"].ToString();
                        p.Aktualizacja = Int32.Parse(reader["TwC_Aktualizacja"].ToString()) == 1;
                        ret.Ceny.Add(p);
                    }
                }
            }
            
            return ret;
        }
        
        public static void TowarModyfikuj(int idSesji, XLTowar obj)
        {
            foreach (var cena in obj.Ceny)
            {
                var zmCene = new XLCenaInfo_20141();
                zmCene.Wersja = XLApiWersja;

                if (cena.GIDTyp != null) zmCene.GIDTyp = (int)cena.GIDTyp;
                if (cena.GIDFirma != null) zmCene.GIDFirma = (int)cena.GIDFirma;
                if (cena.GIDNumer != null) zmCene.GIDNumer = (int)cena.GIDNumer;
                if (cena.GIDLp != null) zmCene.GIDLp = (int)cena.GIDLp;
                if (cena.Numer != null) zmCene.Numer = (int)cena.Numer;
                if (cena.NrKursu != null) zmCene.NrKursu = (int)cena.NrKursu;
                if (cena.Aktualizacja != null) zmCene.Aktualizacja = (bool)cena.Aktualizacja ? 1 : 0;
                if (cena.Kod != null) zmCene.Kod = cena.Kod;
                if (cena.Waluta != null) zmCene.Waluta = cena.Waluta;
                if (cena.Wartosc != null) zmCene.Wartosc = ((decimal)cena.Wartosc).ToString(CultureInfo.InvariantCulture);
                if (cena.Marza != null) zmCene.Marza = ((decimal)cena.Marza).ToString(CultureInfo.InvariantCulture);
                if (cena.Zaok != null) zmCene.Zaok = cena.GIDTyp.ToString();
                if (cena.Offset != null) zmCene.Offset = cena.Offset;
                
                int errCode = cdn_api.cdn_api.XLZmienCene(zmCene);
                if (errCode != 0)
                {
                    throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.ZmienCene, errCode), errCode));
                }
            }
        }
    }
}