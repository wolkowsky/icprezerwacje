﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using cdn_api;
using TaskProcessor.Common;
using TaskProcessor.Plugin.ERPXL.Common;

namespace TaskProcessor.Plugin.ERPXL
{
	public partial class Plugin
	{
		public static int DokHandlowyUtworz(int idSesji, XLDokHandlowyNag obj, string connStr = null)
		{
			int dokNumer;
			int idDok = 0;
			var errMsg = new List<string>();

			try
			{
				int errCode;

				try
				{
					errCode = cdn_api.cdn_api.XLDokumentyZwiazane(new XLZwiazanyDokInfo_20141 { Wersja = XLApiWersja, Akcja = -1 }); // czyszczenie listy
					if (errCode != 0)
					{
						throw new Exception(string.Format("Wystąpił błąd w funkcji XLDokumentyZwiazane (czyszczenie listy), Kod błędu = {0}.", errCode));
					}
					foreach (var el in obj.DokumentyZwiazane)
					{
						errCode = cdn_api.cdn_api.XLDokumentyZwiazane(new XLZwiazanyDokInfo_20141 { Wersja = XLApiWersja, GIDTyp = (int)el.GIDTyp, GIDNumer = (int)el.GIDNumer });
						if (errCode != 0)
						{
							throw new Exception(string.Format("Wystąpił błąd w funkcji XLDokumentyZwiazane (dodawanie do listy dokumentu GIDTyp = {0}, GIDNumer = {1}), Kod błędu = {2}.", el.GIDTyp, el.GIDNumer, errCode));
						}
					}
				}
				catch (Exception ex)
				{
					errMsg.Add(ex.Message);
				}
				
				var dokNag = new XLDokumentNagInfo_20141();
				dokNag.Tryb = (int) obj.TrybPracy;
				dokNag.Wersja = XLApiWersja;
				dokNag.Typ = (int) obj.ApiTyp;

				if (obj.KontrahentGlowny.GIDTyp != null) dokNag.KntTyp = (int) obj.KontrahentGlowny.GIDTyp;
				if (obj.KontrahentGlowny.GIDNumer != null) dokNag.KntNumer = (int) obj.KontrahentGlowny.GIDNumer;
				if (obj.KontrahentGlowny.GIDLp != null) dokNag.KntLp = (int) obj.KontrahentGlowny.GIDLp;
				if (obj.KontrahentGlowny.GIDFirma != null) dokNag.KntFirma = (int) obj.KontrahentGlowny.GIDFirma;
				if (obj.KontrahentGlowny.KntAkronim != null) dokNag.Akronim = obj.KontrahentGlowny.KntAkronim;

				if (obj.KontrahentDocelowy.GIDTyp != null) dokNag.KnDTyp = (int) obj.KontrahentDocelowy.GIDTyp;
				if (obj.KontrahentDocelowy.GIDNumer != null) dokNag.KnDNumer = (int) obj.KontrahentDocelowy.GIDNumer;
				if (obj.KontrahentDocelowy.GIDLp != null) dokNag.KnDLp = (int) obj.KontrahentDocelowy.GIDLp;
				if (obj.KontrahentDocelowy.GIDFirma != null) dokNag.KnDFirma = (int) obj.KontrahentDocelowy.GIDFirma;
				if (obj.KontrahentDocelowy.KntAkronim != null) dokNag.Docelowy = obj.KontrahentDocelowy.KntAkronim;

				if (obj.KontrahentPlatnik.GIDTyp != null) dokNag.KnPTyp = (int) obj.KontrahentPlatnik.GIDTyp;
				if (obj.KontrahentPlatnik.GIDNumer != null) dokNag.KnPNumer = (int) obj.KontrahentPlatnik.GIDNumer;
				if (obj.KontrahentPlatnik.KntAkronim != null) dokNag.Platnik = obj.KontrahentPlatnik.KntAkronim;

				if (obj.KontrahentAkwizytor.GIDTyp != null) dokNag.AkwTyp = (int) obj.KontrahentAkwizytor.GIDTyp;
				if (obj.KontrahentAkwizytor.GIDNumer != null) dokNag.AkwNumer = (int) obj.KontrahentAkwizytor.GIDNumer;
				if (obj.KontrahentAkwizytor.GIDLp != null) dokNag.AkwLp = (int) obj.KontrahentAkwizytor.GIDLp;
				if (obj.KontrahentAkwizytor.GIDFirma != null) dokNag.AkwFirma = (int) obj.KontrahentAkwizytor.GIDFirma;
				if (obj.KontrahentAkwizytor.KntAkronim != null) dokNag.Akwizytor = obj.KontrahentAkwizytor.KntAkronim;
				if (obj.AkwizytorKntPrc != null) dokNag.AkwizytorKntPrc = (int) obj.AkwizytorKntPrc;

				if (obj.GIDFirma != null) dokNag.GIDFirma = (int) obj.GIDFirma;
				if (obj.DataDokumentu != null) dokNag.Data = obj.DataDokumentu.ToXLDataInt();
				if (obj.DataSprzedazyZakupu != null) dokNag.DataSpr = obj.DataSprzedazyZakupu.ToXLDataInt();
				if (obj.Seria != null) dokNag.Seria = obj.Seria; // seria może być pusta!
				if (obj.Numer != null) dokNag.Numer = (int) obj.Numer;
				if (obj.Miesiac != null) dokNag.Miesiac = (int) obj.Miesiac;
				if (obj.Rok != null) dokNag.Rok = (int) obj.Rok;
				if (obj.DokumentObcy != null) dokNag.DokumentObcy = obj.DokumentObcy;
				if (obj.AdrTyp != null) dokNag.AdrTyp = (int) obj.AdrTyp;
				if (obj.AdrNumer != null) dokNag.AdrNumer = (int) obj.AdrNumer;
				if (obj.AdwTyp != null) dokNag.AdwTyp = (int) obj.AdwTyp;
				if (obj.AdwNumer != null) dokNag.AdwNumer = (int) obj.AdwNumer;
				if (obj.AdPNumer != null) dokNag.AdPNumer = (int) obj.AdPNumer;
				if (obj.ExpoNorm != null) dokNag.ExpoNorm = (int) obj.ExpoNorm;
				if (obj.Waluta != null) dokNag.Waluta = obj.Waluta;
				if (obj.FlagaNB != null) dokNag.NB = obj.FlagaNB;
				if (obj.FormaPlatnosci != null) dokNag.Forma = (int) obj.FormaPlatnosci;
				if (obj.ZamTyp != null) dokNag.ZamTyp = (int) obj.ZamTyp;
				if (obj.ZamFirma != null) dokNag.ZamFirma = (int) obj.ZamFirma;
				if (obj.ZamNumer != null) dokNag.ZamNumer = (int) obj.ZamNumer;
				if (obj.ZamLp != null) dokNag.ZamLp = (int) obj.ZamLp;
				if (obj.IgnorujMagazynowe != null) dokNag.IgnorujMagazynowe = (int)obj.IgnorujMagazynowe;
				if (obj.IgnorujZaliczkowe != null) dokNag.IgnorujZaliczkowe = (int)obj.IgnorujZaliczkowe;
				if (obj.CzyFiskalny != null) dokNag.Fiskalny = (bool) obj.CzyFiskalny ? 1 : 0;
				if (obj.CzyUwzgledniacZwiazane != null) dokNag.Zwiazane = (bool) obj.CzyUwzgledniacZwiazane ? 1 : 0;
				dokNag.Termin = obj.TerminPlatnosci ?? -1;
				if (obj.MagazynZ != null) dokNag.MagazynZ = obj.MagazynZ;
				if (obj.MagazynD != null) dokNag.MagazynD = obj.MagazynD;
				if (obj.Opis != null) dokNag.Opis = obj.Opis;
				if (obj.Url != null) dokNag.URL = obj.Url;

				errCode = cdn_api.cdn_api.XLNowyDokument(idSesji, ref idDok, dokNag);
				if (errCode != 0)
				{
					throw new XLApiRetryException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.NowyDokument, errCode), errCode));
				}

				foreach (var poz in obj.Elementy)
				{
					var pozInfo = new XLDokumentElemInfo_20141();
					pozInfo.Wersja = XLApiWersja;
					if (poz.CzyPobieracTowarPrzezGID != null) pozInfo.TwrReq = (bool)poz.CzyPobieracTowarPrzezGID ? 1 : 0;
					if (poz.Towar.GIDTyp != null) pozInfo.TwrTyp = (int) poz.Towar.GIDTyp;
					if (poz.Towar.GIDNumer != null) pozInfo.TwrNumer = (int) poz.Towar.GIDNumer;
					if (poz.Towar.TwrKod != null) pozInfo.TowarKod = poz.Towar.TwrKod;
					if (poz.Ilosc != null) pozInfo.Ilosc = ((decimal)poz.Ilosc).ToString(CultureInfo.InvariantCulture);
					if (poz.ZamTyp != null) pozInfo.ZamTyp = (int) poz.ZamTyp;
					if (poz.ZamFirma != null) pozInfo.ZamFirma = (int) poz.ZamFirma;
					if (poz.ZamNumer != null) pozInfo.ZamNumer = (int) poz.ZamNumer;
					if (poz.ZamLp != null) pozInfo.ZamLp = (int) poz.ZamLp;
					if (poz.Nazwa != null) pozInfo.TowarNazwa = poz.Nazwa;
					if (poz.JmZ != null) pozInfo.JmZ = poz.JmZ;
					if (poz.FlagaVAT != null) pozInfo.FlagaVAT = (int) poz.FlagaVAT;
					if (poz.Waluta != null) pozInfo.Waluta = poz.Waluta;
					if (poz.Wartosc != null) pozInfo.Wartosc = ((decimal)poz.Wartosc).ToString(CultureInfo.InvariantCulture);
					if (poz.CzySprzedawacMimoRezerwacji != null) pozInfo.RezIgnor = (bool) poz.CzySprzedawacMimoRezerwacji ? 1 : 0;
					if (poz.CzyWartoscOstateczna != null) pozInfo.WartoscOst = (bool) poz.CzyWartoscOstateczna ? 1 : 0;
					if (poz.CzyPodanaIloscJestWymagana != null) pozInfo.IloscReq = (bool)poz.CzyPodanaIloscJestWymagana ? 1 : 0;
					if (poz.Rownanie != null) pozInfo.Rownanie = (int) poz.Rownanie;
					if (poz.Opis != null) pozInfo.Opis = poz.Opis;

					errCode = cdn_api.cdn_api.XLDodajPozycje(idDok, pozInfo);
					if (errCode != 0)
					{
						if(errCode == 106) // deadlock
						{
							throw new XLApiRetryException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.DodajPozycje, errCode), errCode));
						}
						throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.DodajPozycje, errCode), errCode));
					}
				}

				var dokNagZamk = new XLZamkniecieDokumentuInfo_20141 { Wersja = XLApiWersja, Tryb = (int) obj.TrybZamkniecia, Magazynowe = (int) obj.Magazynowe };
				errCode = cdn_api.cdn_api.XLZamknijDokument(idDok, dokNagZamk);
				if (errCode != 0)
				{
					throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.ZamknijDokument, errCode), errCode));
				}

				dokNumer = dokNagZamk.GidNumer;

				foreach (var atr in obj.Atrybuty)
				{
					try
					{
						var dodAtrInfo = new XLAtrybutInfo_20141();
						dodAtrInfo.Wersja = XLApiWersja;
						dodAtrInfo.GIDTyp = atr.GIDTyp ?? dokNag.GIDTyp;
						dodAtrInfo.GIDFirma = atr.GIDFirma ?? dokNag.GIDFirma;
						dodAtrInfo.GIDNumer = atr.GIDNumer ?? dokNag.GIDNumer;
						if (atr.GIDLp != null) dodAtrInfo.GIDLp = (int)atr.GIDLp;
						if (atr.GIDSubLp != null) dodAtrInfo.GIDSubLp = (int)atr.GIDSubLp;
						dodAtrInfo.Klasa = atr.Klasa;
						dodAtrInfo.Wartosc = atr.Wartosc;

						errCode = cdn_api.cdn_api.XLDodajAtrybut(idSesji, dodAtrInfo);
						if (errCode != 0)
						{
							throw new Exception(string.Format("Wystąpił błąd w funkcji XLDodajAtrybut (KlasaAtrybutu = {0}), Kod błędu = {1}.", atr.Klasa, errCode));
						}
					}
					catch (Exception ex)
					{
						errMsg.Add(ex.Message);
					}
				}

				try
				{
					if (obj.Zalaczniki.Any())
					{
						var zalaczniki = obj.Zalaczniki;
						foreach (var zal in zalaczniki)
						{
							zal.ObiTyp = dokNag.GIDTyp;
							zal.ObiNumer = dokNagZamk.GidNumer;
						}
						var dodZal = ZalacznikiDodajDoObiektu(zalaczniki, connStr);
						if (!dodZal)
						{
							throw new Exception("Wystąpił błąd przy dodawaniu załączników.");
						}
					}
				}
				catch (Exception ex)
				{
					errMsg.Add(ex.Message);
				}

				if (errMsg.Any())
				{
					throw new XLApiNonFatalException(string.Join("; ", errMsg));
				}
			}
			catch (Exception ex)
			{
				if (ex is XLApiNonFatalException && ex is XLApiRetryException)
				{
				}
				else
				{
					if (idDok > 0)
					{
						cdn_api.cdn_api.XLZamknijDokument(idDok, new XLZamkniecieDokumentuInfo_20141 { Wersja = XLApiWersja, Tryb = (int)XLTrybZamknieciaDokHan.Skasowanie });
					}
				}
				throw;
			}

			return dokNumer;
		}

		public static int DokHandlowyModyfikuj(int idSesji, XLDokHandlowyNag obj, string connStr = null)
		{
			var dokNag = new XLOtwarcieNagInfo_20141();
			dokNag.Tryb = (int)obj.TrybPracy;
			dokNag.Wersja = XLApiWersja;
			dokNag.GIDNumer = (int)obj.GIDNumer;
			dokNag.GIDTyp = (int)DokHandlowyWczytaj((int)obj.GIDNumer, connStr).GIDTyp;

			int idDok = 0;

			int errCode = cdn_api.cdn_api.XLOtworzDokument(idSesji, ref idDok, dokNag);
			if (errCode != 0)
			{
				throw new XLApiRetryException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.OtworzDokumentHan, errCode), errCode));
			}

			var dokNagZamk = new XLZamkniecieDokumentuInfo_20141 { Wersja = XLApiWersja, Tryb = (int)obj.TrybZamkniecia, Magazynowe = (int)obj.Magazynowe };
			errCode = cdn_api.cdn_api.XLZamknijDokument(idDok, dokNagZamk);
			if (errCode != 0)
			{
				try
				{
					cdn_api.cdn_api.XLZamknijDokument(idDok, new XLZamkniecieDokumentuInfo_20141 { Wersja = XLApiWersja, Tryb = (int)XLTrybZamknieciaDokHan.ZamkniecieOtwartego });
				}
				catch
				{
				}
				throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.ZamknijDokument, errCode), errCode));
			}

			return dokNagZamk.GidNumer;
		}

		public static int DokHandlowyUsun(int idSesji, XLDokHandlowyNag obj, string connStr = null)
		{
			var dokNag = new XLOtwarcieNagInfo_20141();
			dokNag.Tryb = (int)obj.TrybPracy;
			dokNag.Wersja = XLApiWersja;
			dokNag.GIDNumer = (int)obj.GIDNumer;
			dokNag.GIDTyp = (int)DokHandlowyWczytaj((int)obj.GIDNumer, connStr).GIDTyp;

			if (obj.GIDNumer == null)
			{
				if (obj.Elementy.Any())
				{
					dokNag.GIDNumer = (int)obj.Elementy.First().GIDNumer;
				}
			}
			else
			{
				dokNag.GIDNumer = (int)obj.GIDNumer;
			}

			int idDok = 0;

			int errCode = cdn_api.cdn_api.XLOtworzDokument(idSesji, ref idDok, dokNag);
			if (errCode != 0)
			{
				throw new XLApiRetryException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.OtworzDokumentHan, errCode), errCode));
			}

			foreach (var poz in obj.Elementy)
			{
				var pozInfo = new XLUsunElemInfo_20141();
				pozInfo.Wersja = XLApiWersja;
				pozInfo.Pozycja = (int)poz.GIDLp;

				errCode = cdn_api.cdn_api.XLUsunPozycje(idDok, pozInfo);
				if (errCode != 0)
				{
					throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.UsunPozycje, errCode), errCode));
				}
			}

			var trybZamk = (obj.GIDNumer == null || obj.TrybZamkniecia == null) ? XLTrybZamknieciaDokHan.ZamkniecieOtwartego : obj.TrybZamkniecia;

			var dokNagZamk = new XLZamkniecieDokumentuInfo_20141 { Wersja = XLApiWersja, Tryb = (int)trybZamk, Magazynowe = (int)obj.Magazynowe };
			errCode = cdn_api.cdn_api.XLZamknijDokument(idDok, dokNagZamk);
			if (errCode != 0)
			{
				try
				{
					cdn_api.cdn_api.XLZamknijDokument(idDok, new XLZamkniecieDokumentuInfo_20141 { Wersja = XLApiWersja, Tryb = (int)XLTrybZamknieciaDokHan.ZamkniecieOtwartego });
				}
				catch
				{
				}
				throw new XLApiFatalException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.ZamknijDokument, errCode), errCode));
			}

			return dokNagZamk.GidNumer;
		}

		public static XLDokHandlowyNag DokHandlowyWczytaj(int gidNumer, string connStr = null)
		{
			var ret = new XLDokHandlowyNag { TrybZamkniecia = null, Magazynowe = null, ExpoNorm = null };

			using (var conn = new SqlConnection(connStr ?? Helpers.GetSettingsValue("ConnectionString", true)))
			{
				var cmdNag = new SqlCommand(@"
					SELECT
						TrN_GIDTyp,
						TrN_GIDFirma,
						TrN_KntTyp,
						TrN_KntNumer,
						ISNULL(Knt_Akronim, '') AS 'Knt_Akronim',
						TrN_FlagaNB,
						TrN_FormaNr,
						TrN_Data3 AS 'DataSprzedazy',
						TrN_Data2 AS 'DataWystawienia',
						TrN_Waluta,
						TrN_KursL,
						TrN_KursM,
						TrN_NrKursu,
						TrN_TrNNumer,
						TrN_TrNMiesiac,
						TrN_TrNRok,
						TrN_TrNTyp,
						TrN_RokMiesiac,
						TrN_Stan
						TrN_Url,
						TrN_Stan,
						TrN_ExpoNorm,
						TrN_TrNSeria,
						TrN_DokumentObcy,
						ISNULL((SELECT TOP 1 TnO_Opis FROM CDN.TrNOpisy WHERE TnO_TrNTyp = TrN_GIDTyp AND TnO_TrNNumer = TrN_GIDNumer), '') AS 'Opis'
					FROM
						CDN.TraNag
						LEFT JOIN CDN.KntKarty KG
							ON KG.Knt_GIDNumer = TrN_KntNumer
					WHERE
						TrN_GIDNumer = @GIDNumer
				", conn);

				cmdNag.Parameters.AddWithValue("@GIDNumer", gidNumer);

				conn.Open();

				using (var reader = cmdNag.ExecuteReader())
				{
					reader.Read();

					ret.GIDTyp = Int32.Parse(reader["TrN_GIDTyp"].ToString());
					ret.ApiTyp = UzupelnijXLApiTyp((int)ret.GIDTyp);
					ret.GIDNumer = gidNumer;
					ret.GIDFirma = Int32.Parse(reader["TrN_GIDFirma"].ToString());
					ret.Seria = reader["TrN_TrNSeria"].ToString();
					ret.KontrahentGlowny.GIDTyp = Int32.Parse(reader["TrN_KntTyp"].ToString());
					ret.KontrahentGlowny.GIDNumer = Int32.Parse(reader["TrN_KntNumer"].ToString());
					ret.KontrahentGlowny.KntAkronim = reader["Knt_Akronim"].ToString();
					ret.FlagaNB = reader["TrN_FlagaNB"].ToString();
					ret.FormaPlatnosci = Int32.Parse(reader["TrN_FormaNr"].ToString());
					ret.DataDokumentu = Int32.Parse(reader["DataWystawienia"].ToString()).ToDateTimeFromXLDataInt();
					ret.DataSprzedazyZakupu = Int32.Parse(reader["DataSprzedazy"].ToString()).ToDateTimeFromXLDataInt();
					ret.Waluta = reader["TrN_Waluta"].ToString();
					ret.Rok = Int32.Parse(reader["TrN_TrNRok"].ToString());
					ret.Miesiac = Int32.Parse(reader["TrN_TrNMiesiac"].ToString());
					ret.Numer = Int32.Parse(reader["TrN_TrNNumer"].ToString());
					ret.RokMiesiac = Int32.Parse(reader["TrN_RokMiesiac"].ToString());
					ret.Stan = Int32.Parse(reader["TrN_Stan"].ToString());
					ret.ExpoNorm = Int32.Parse(reader["TrN_ExpoNorm"].ToString());
					ret.DokumentObcy = reader["TrN_DokumentObcy"].ToString();
					ret.Opis = reader["Opis"].ToString();
					ret.Url = reader["TrN_Url"].ToString();
				}

				var cmdElem = new SqlCommand(@"
					SELECT
						TrE_GIDLp,
						TrE_TwrTyp,
						TrE_TwrNumer,
						TrE_Ilosc, 
						TrE_TwrNazwa,
						ISNULL(Twr_Kod, '') AS 'Twr_Kod',
						TrE_WartoscPoRabacie,
						ISNULL((SELECT TOP 1 ZeO_Opis FROM CDN.ZaEOpisy WHERE ZeO_ZamTyp = TrE_GIDTyp AND ZeO_ZamNumer = TrE_GIDNumer AND ZeO_ZamLp = TrE_GIDLp), '') AS 'Opis'
					FROM
						CDN.TraElem
						LEFT JOIN CDN.TwrKarty
							ON TrE_TwrNumer = Twr_GIDNumer
					WHERE
						TrE_GIDNumer = @GIDNumer
					ORDER BY
						TrE_Pozycja
				", conn);

				cmdElem.Parameters.AddWithValue("@GIDNumer", gidNumer);

				using (var reader = cmdElem.ExecuteReader())
				{
					while (reader.Read())
					{
						var p = new XLDokHandlowyElem { CzySprzedawacMimoRezerwacji = null, CzyWartoscOstateczna = null };

						p.GIDTyp = ret.GIDTyp;
						p.GIDNumer = gidNumer;
						p.GIDLp = Int32.Parse(reader["TrE_GIDLp"].ToString());
						p.Towar.GIDTyp = Int32.Parse(reader["TrE_TwrTyp"].ToString());
						p.Towar.GIDNumer = Int32.Parse(reader["TrE_TwrNumer"].ToString());
						p.Towar.TwrNazwa = reader["TrE_TwrNazwa"].ToString();
						p.Towar.TwrKod = reader["Twr_Kod"].ToString();
						p.Ilosc = Decimal.Parse(reader["TrE_Ilosc"].ToString());
						p.Wartosc = Decimal.Parse(reader["TrE_WartoscPoRabacie"].ToString());
						p.Opis = reader["Opis"].ToString();

						ret.Elementy.Add(p);
					}
				}

				var cmdPlat = new SqlCommand(@"
					SELECT
						(CASE WHEN TrP_Termin - ISNULL(TrN_Data2, 0) < 0 THEN 0 ELSE TrP_Termin - ISNULL(TrN_Data2, 0) END) AS 'TerminOffset',
						TrP_FormaNr,
						TrP_Kwota,
						TrP_Pozostaje,
						TrP_Waluta,
						TrP_Notatki
					FROM
						CDN.TraPlat
						LEFT JOIN CDN.TraNag
							ON TrP_GIDNumer = TrN_GIDNumer
					WHERE
					   TrP_GIDNumer = @GIDNumer AND TrP_GIDLp > 0
				", conn);

				cmdPlat.Parameters.AddWithValue("@GIDNumer", gidNumer);

				using (var reader = cmdPlat.ExecuteReader())
				{
					while (reader.Read())
					{
						var p = new XLDokHandlowyPlatnosc();

						p.TerminOffset = Int32.Parse(reader["TerminOffset"].ToString());
						p.FormaPlatnosci = Int32.Parse(reader["TrP_FormaNr"].ToString());
						p.KwotaWartosc = Decimal.Parse(reader["TrP_Kwota"].ToString());
						p.Waluta = reader["TrP_Waluta"].ToString();
						p.Opis = reader["TrP_Notatki"].ToString();

						ret.Platnosci.Add(p);
					}
				}
			}

			ret.Zalaczniki = ZalacznikiWczytajListe(new XLObiektOgolny { GIDTyp = (int)ret.GIDTyp, GIDNumer = gidNumer }, connStr: connStr);

			return ret;
		}

	}
}
