﻿using System;
using System.Data.SqlClient;
using TaskProcessor.Common;
using TaskProcessor.Plugin.ERPXL.Common;

namespace TaskProcessor.Plugin.ERPXL
{
    public partial class Plugin
    {
        public static XLKontrahent KontrahentWczytaj(int gidNumer, string connStr = null)
        {
            var ret = new XLKontrahent { TrybZamkniecia = null };

            using (var conn = new SqlConnection(connStr ?? Helpers.GetSettingsValue("ConnectionString", true)))
            {
                var cmdNag = new SqlCommand(@"
                    SELECT 
                        Knt_GIDTyp,
                        Knt_GIDFirma,
                        Knt_GIDNumer,
                        Knt_GIDLp,
                        Knt_KnATyp,
                        Knt_KnAFirma,
                        Knt_KnANumer,
                        Knt_KnALp,
                        Knt_Typ,
                        Knt_Akwizytor,
                        Knt_Akronim,
                        Knt_FppKod,
                        Knt_Nazwa1,
                        Knt_Nazwa2,
                        Knt_Nazwa3,
                        Knt_KodP,
                        Knt_Miasto,
                        Knt_Ulica,
                        Knt_Adres,
                        Knt_NipE,
                        Knt_NipPrefiks,
                        Knt_Regon,
                        Knt_Pesel,
                        Knt_KontoDostawcy,
                        Knt_KontoOdbiorcy,
                        Knt_Kraj,
                        Knt_Powiat,
                        Knt_Gmina,
                        Knt_Wojewodztwo,
                        Knt_RegionCRM,
                        Knt_GLN,
                        Knt_Telefon1,
                        Knt_Telefon2,
                        Knt_Fax,
                        Knt_Modem,
                        Knt_Telex,
                        Knt_EMail,
                        Knt_URL,
                        Knt_BnkTyp,
                        Knt_BnkFirma,
                        Knt_BnkNumer,
                        Knt_BnkLp,
                        Knt_NrRachunku,
                        Knt_Soundex,
                        Knt_Rabat,
                        Knt_LimitWart,
                        Knt_MaxLimitWart,
                        Knt_LimitPoTerminie,
                        Knt_LimitOkres,
                        Knt_Dewizowe,
                        Knt_Symbol,
                        Knt_NrKursu,
                        Knt_Cena,
                        Knt_FormaPl,
                        Knt_Marza,
                        Knt_TypKarty,
                        Knt_NumerKarty,
                        Knt_DataKarty,
                        Knt_Ean,
                        Knt_ObcaKarta,
                        Knt_Osoba,
                        Knt_ExpoKraj,
                        Knt_SeriaFa,
                        Knt_PlatnikVat,
                        Knt_TypDok,
                        Knt_Status,
                        Knt_FAVATData,
                        Knt_SposobDostawy,
                        Knt_HasloChk,
                        Knt_HasloKontrahent,
                        Knt_Dzialalnosc,
                        Knt_ZTrTyp,
                        Knt_ZTrFirma,
                        Knt_ZTrNumer,
                        Knt_ZTrLp,
                        Knt_OpeTyp,
                        Knt_OpeFirma,
                        Knt_OpeNumer,
                        Knt_OpeLp,
                        Knt_AkwTyp,
                        Knt_AkwFirma,
                        Knt_AkwNumer,
                        Knt_AkwLp,
                        Knt_PrcTyp,
                        Knt_PrcFirma,
                        Knt_PrcNumer,
                        Knt_PrcLp,
                        Knt_Atrybut1,
                        Knt_Format1,
                        Knt_Wartosc1,
                        Knt_Atrybut2,
                        Knt_Format2,
                        Knt_Wartosc2,
                        Knt_Atrybut3,
                        Knt_Format3,
                        Knt_Wartosc3,
                        Knt_AkwProwizja,
                        Knt_Umowa,
                        Knt_DataW,
                        Knt_LastModL,
                        Knt_LastModO,
                        Knt_LastModC,
                        Knt_FaVATOsw,
                        Knt_CechaOpis,
                        Knt_Aktywna,
                        Knt_Wsk,
                        Knt_OutlookUrl,
                        Knt_Nieaktywny,
                        Knt_Zrodlo,
                        Knt_Branza,
                        Knt_Rodzaj,
                        Knt_RolaPartnera,
                        Knt_Odleglosc,
                        Knt_KarTyp,
                        Knt_KarFirma,
                        Knt_KarNumer,
                        Knt_KarLp,
                        Knt_NRB,
                        Knt_Archiwalny,
                        Knt_AdresNieAktualny,
                        Knt_LastTransLockDate,
                        Knt_OpeTypM,
                        Knt_OpeFirmaM,
                        Knt_OpeNumerM,
                        Knt_OpeLpM,
                        Knt_BlokadaTransakcji,
                        Knt_Oddzialowy,
                        Knt_Spedytor,
                        Knt_TerminPlKa,
                        Knt_FormaPlKa,
                        Knt_TerminPlZak,
                        Knt_FormaPlZak,
                        Knt_SpTerminPlSpr,
                        Knt_SpTerminPlRK,
                        Knt_SpTerminPlZak,
                        Knt_LimitTerminowy,
                        Knt_DataWygasniecia,
                        Knt_PIN,
                        Knt_Priorytet,
                        Knt_FrsID,
                        Knt_Controlling,
                        Knt_RolnikRyczaltowy,
                        Knt_PriorytetRez,
                        Knt_Powiazany,
                        Knt_PlatnoscKaucji,
                        Knt_TerminRozliczeniaKaucji,
                        Knt_KnPTyp,
                        Knt_KnPNumer,
                        Knt_KnPParam,
                        Knt_DataUtworzenia,
                        Knt_DokumentTozsamosci,
                        Knt_DataWydania,
                        Knt_OrganWydajacy,
                        Knt_MaxDniPoTerminie,
                        Knt_KalendarzDst,
                        Knt_KalendarzWys,
                        Knt_KnGTyp,
                        Knt_KnGNumer,
                        Knt_Punkty,
                        Knt_KnCTyp,
                        Knt_KnCNumer,
                        Knt_EFaVatAktywne,
                        Knt_EFaVatOsw,
                        Knt_EFaVatDataDo,
                        Knt_EFaVatEMail,
                        Knt_PodatnikiemNabywca,
                        Knt_MSTwrGrupaNumer,
                        Knt_StanPostep,
                        Knt_ESklep,
                    FROM
                        CDN.KntKarty
                    WHERE
                        Knt_GIDNumer = @GIDNumer", conn);

                cmdNag.Parameters.AddWithValue("@GIDNumer", gidNumer);

                conn.Open();

                using (var reader = cmdNag.ExecuteReader())
                {
                    reader.Read();
                    ret.GIDTyp = Int32.Parse(reader["Knt_GIDTyp"].ToString());
                    ret.GIDFirma = Int32.Parse(reader["Knt_GIDFirma"].ToString());
                    ret.GIDNumer = Int32.Parse(reader["Knt_GIDNumer"].ToString());
                    ret.GIDLp = Int32.Parse(reader["Knt_GIDLp"].ToString());
                    ret.Knt_KnATyp = Int32.Parse(reader["Knt_KnATyp"].ToString());
                    ret.Knt_KnAFirma = Int32.Parse(reader["Knt_KnAFirma"].ToString());
                    ret.Knt_KnANumer = Int32.Parse(reader["Knt_KnANumer"].ToString());
                    ret.Knt_KnALp = Int32.Parse(reader["Knt_KnALp"].ToString());
                    ret.ApiTyp = Int32.Parse(reader["Knt_Typ"].ToString());
                    ret.Akwizytor = Boolean.Parse(reader["Knt_Akwizytor"].ToString());
                    ret.KntAkronim = reader["Knt_Akronim"].ToString();
                    ret.FPPKod = reader["Knt_FppKod"].ToString();
                    ret.Nazwa1 = reader["Knt_Nazwa1"].ToString();
                    ret.Nazwa2 = reader["Knt_Nazwa2"].ToString();
                    ret.Nazwa3 = reader["Knt_Nazwa3"].ToString();
                    ret.KodP = reader["Knt_KodP"].ToString();
                    ret.Miasto = reader["Knt_Miasto"].ToString();
                    ret.Ulica = reader["Knt_Ulica"].ToString();
                    ret.Adres = reader["Knt_Adres"].ToString();
                    ret.NipE = reader["Knt_NipE"].ToString();
                    ret.NipPrefiks = reader["Knt_NipPrefiks"].ToString();
                    ret.Regon = reader["Knt_Regon"].ToString();
                    ret.Pesel = reader["Knt_Pesel"].ToString();
                    ret.KontoDostawcy = reader["Knt_KontoDostawcy"].ToString();
                    ret.KontoOdbiorcy = reader["Knt_KontoOdbiorcy"].ToString();
                    ret.Kraj = reader["Knt_Kraj"].ToString();
                    ret.Powiat = reader["Knt_Powiat"].ToString();
                    ret.Gmina = reader["Knt_Gmina"].ToString();
                    ret.Wojewodztwo = reader["Knt_Wojewodztwo"].ToString();
                    ret.RegionCRM = Int32.Parse(reader["Knt_RegionCRM"].ToString());
                    ret.GLN = reader["Knt_GLN"].ToString();
                    ret.Telefon1 = reader["Knt_Telefon1"].ToString();
                    ret.Telefon2 = reader["Knt_Telefon2"].ToString();
                    ret.Fax = reader["Knt_Fax"].ToString();
                    ret.Modem = reader["Knt_Modem"].ToString();
                    ret.Telex = reader["Knt_Telex"].ToString();
                    ret.Email = reader["Knt_EMail"].ToString();
                    ret.URL = reader["Knt_URL"].ToString();
                    ret.Knt_BnkTyp = Int32.Parse(reader["Knt_BnkTyp"].ToString());
                    ret.Knt_BnkFirma = Int32.Parse(reader["Knt_BnkFirma"].ToString());
                    ret.Knt_BnkNumer = Int32.Parse(reader["Knt_BnkNumer"].ToString());
                    ret.Knt_BnkLp = Int32.Parse(reader["Knt_BnkLp"].ToString());
                    ret.NrRachunku = reader["Knt_NrRachunku"].ToString();
                    ret.Knt_Soundex = reader["Knt_Soundex"].ToString();
                    ret.Rabat = Decimal.Parse(reader["Knt_Rabat"].ToString());
                    ret.MaxLimitWartosc = Decimal.Parse(reader["Knt_MaxLimitWart"].ToString());
                    ret.LimitPoTerminie = Decimal.Parse(reader["Knt_LimitPoTerminie"].ToString());
                    ret.Knt_LimitOkres = Int32.Parse(reader["Knt_LimitOkres"].ToString());
                    ret.Dewizowe = Enum.IsDefined(typeof(XLKntRozliczeniaDewizowe), reader["Knt_Dewizowe"].ToString()) ? (XLKntRozliczeniaDewizowe?)Enum.Parse(typeof(XLKntRozliczeniaDewizowe), reader["Knt_Dewizowe"].ToString()) : null;
                    ret.SymbolWaluty = reader["Knt_Waluta"].ToString();
                    ret.Knt_NrKursu = Int32.Parse(reader["Knt_NrKursu"].ToString());
                }
            }

            return ret;
        }
    }
}
