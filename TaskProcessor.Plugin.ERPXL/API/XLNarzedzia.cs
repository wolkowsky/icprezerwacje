﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using cdn_api;
using TaskProcessor.Common;
using TaskProcessor.Plugin.ERPXL.Common;

namespace TaskProcessor.Plugin.ERPXL
{
    public partial class Plugin
    {
        private const string DomyslnyFiltrSqlDlaWydrukow = "(1=1)";

        public static XLInfoOWersji NarzedziaPobierzWersjeXL(string connStr = null)
        {
            var ret = new XLInfoOWersji();
            
            string wersja;
            
            using (var conn = new SqlConnection(connStr ?? Helpers.GetSettingsValue("ConnectionString", true)))
            {
                var cmd = new SqlCommand(@"SELECT ISNULL((SELECT SYS_Wartosc FROM CDN.SystemCDN WHERE SYS_Id = 3), '') AS 'WersjaXL'", conn);

                conn.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    wersja = reader.GetString(0);
                }
            }

            ret.Wersja = wersja;

            try
            {
                var wList = wersja.Split('.').Select(int.Parse).ToList();
                if (wList.Count >= 1)
                {
                    ret.Rok = wList[0];
                }
                if (wList.Count >= 2)
                {
                    ret.Wydanie = wList[1];
                }
                if (wList.Count >= 3)
                {
                    ret.Kompilacja = wList[2];
                }
            }
            catch { }

            return ret;
        }

        public static string NarzedziaPobierzNumerDok(int gidTyp, int gidNumer, string connStr = null)
        {
            using (var conn = new SqlConnection(connStr ?? Helpers.GetSettingsValue("ConnectionString", true)))
            {
                var cmd = new SqlCommand(@"SELECT CDN.NazwaObiektu(@GIDTyp, @GIDNumer, 0, 2)", conn);

                cmd.Parameters.AddWithValue("@GIDTyp", gidTyp);
                cmd.Parameters.AddWithValue("@GIDNumer", gidNumer);

                conn.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    return reader.GetString(0);
                }
            }
        }

        public static byte[] NarzedziaWykonajWydruk(int idZrodla, int idWydruku, int idFormatu, string filtrSql = DomyslnyFiltrSqlDlaWydrukow)
        {
            byte[] ret = null;

            string tempDir = Helpers.GetTempDir();

            if (!Directory.Exists(tempDir))
            {
                try
                {
                    Directory.CreateDirectory(tempDir);
                }
                catch
                {
                    throw new XLApiFatalException("Wystąpił błąd przy wykonywaniu wydruku - nie można utworzyć katalogu plików tymczasowych.");
                }
            }

            string tempFile = Path.Combine(tempDir, Guid.NewGuid() + ".tmp");

            var wydrukInfo = new XLWydrukInfo_20141();
            wydrukInfo.Wersja = XLApiWersja;
            wydrukInfo.Zrodlo = idZrodla;
            wydrukInfo.Wydruk = idWydruku;
            wydrukInfo.Format = idFormatu;
            wydrukInfo.FiltrSQL = filtrSql;
            wydrukInfo.Urzadzenie = 0;
            wydrukInfo.DrukujDoPliku = 1;
            wydrukInfo.PlikDocelowy = tempFile;

            int errCode = cdn_api.cdn_api.XLWykonajPodanyWydruk(wydrukInfo);
            if (errCode != 0)
            {
                throw new XLApiRetryException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.WykonajPodanyWydruk, errCode), errCode));
            }

            ret = File.ReadAllBytes(tempFile);

            if (File.Exists(tempFile))
            {
                try
                {
                    File.Delete(tempFile);
                }
                catch { }
            }

            return ret;
        }

        public static XLDanePrzeliczRabat NarzedziaPrzeliczRabat(XLDanePrzeliczRabat dane)
        {
            var xlDane = new XLRabatyComInfo_20141();
            xlDane.Wersja = XLApiWersja;
            if (dane.Typ != null) xlDane.Typ = (int)dane.Typ;
            if (dane.Data != null) xlDane.Data = dane.Data.ToXLCzas();
            if (dane.ExpoNorm != null) xlDane.ExpoNorm = (int)dane.ExpoNorm;
            if (dane.FormaPl != null) xlDane.FormaPl = (int)dane.FormaPl;
            if (dane.RabatFormyPlatnosci != null) xlDane.RabatFormyPlatnosci = (int)dane.RabatFormyPlatnosci;
            if (dane.RabatGlobalny != null) xlDane.RabatGlobalny = (int)dane.RabatGlobalny;
            if (dane.LiczOdWartosciKsiegowej != null) xlDane.LiczOdWartosciKsiegowej = (int)dane.LiczOdWartosciKsiegowej;
            if (dane.TypDopasowania != null) xlDane.TypDopasowania = (int)dane.TypDopasowania;
            if (dane.IloscCenaWartosc != null) xlDane.IloscCenaWartosc = (int)dane.IloscCenaWartosc;
            if (dane.Dokladnosc != null) xlDane.Dokladnosc = (int)dane.Dokladnosc;
            if (dane.WymusZmianeWartosci != null) xlDane.WymusZmianeWartosci = (int)dane.WymusZmianeWartosci;
            if (dane.NumerKursuWaluty != null) xlDane.NumerKursuWaluty = (int)dane.NumerKursuWaluty;
            if (dane.MianownikKursuWaluty != null) xlDane.MianownikKursuWaluty = (int)dane.MianownikKursuWaluty;
            if (dane.LicznikKursuWaluty != null) xlDane.LicznikKursuWaluty = (int)dane.LicznikKursuWaluty;
            if (dane.TypRabatuKontrahentaNaTowar != null) xlDane.TypRabatuKontrahentaNaTowar = (int)dane.TypRabatuKontrahentaNaTowar;
            if (dane.RabatKontrahentaNaTowar != null) xlDane.RabatKontrahentaNaTowar = (int)dane.RabatKontrahentaNaTowar;
            if (dane.TypProgu != null) xlDane.TypProgu = (int)dane.TypProgu;
            if (dane.WartoscProgowa != null) xlDane.WartoscProgowa = (int)dane.WartoscProgowa;
            if (dane.TypRabatuProgowego != null) xlDane.TypRabatuProgowego = (int)dane.TypRabatuProgowego;
            if (dane.RabatProgowy != null) xlDane.RabatProgowy = (int)dane.RabatProgowy;
            if (dane.Flagi != null) xlDane.Flagi = (int)dane.Flagi;
            if (dane.NumerCenyPoczatkowej != null) xlDane.NumerCenyPoczatkowej = (int)dane.NumerCenyPoczatkowej;
            if (dane.RabatEfektywny != null) xlDane.RabatEfektywny = (int)dane.RabatEfektywny;
            if (dane.GIDTyp != null) xlDane.GIDTyp = (int)dane.GIDTyp;
            if (dane.GIDFirma != null) xlDane.GIDFirma = (int)dane.GIDFirma;
            if (dane.GIDNumer != null) xlDane.GIDNumer = (int)dane.GIDNumer;
            if (dane.GIDLp != null) xlDane.GIDLp = (int)dane.GIDLp;
            if (dane.ZstTyp != null) xlDane.ZstTyp = (int)dane.ZstTyp;
            if (dane.ZstFirma != null) xlDane.ZstFirma = (int)dane.ZstFirma;
            if (dane.ZstNumer != null) xlDane.ZstNumer = (int)dane.ZstNumer;
            if (dane.ZstLp != null) xlDane.ZstLp = (int)dane.ZstLp;
            if (dane.FlagaNB != null) xlDane.FlagaNB = dane.FlagaNB;
            if (dane.Ilosc != null) xlDane.Ilosc = ((decimal)dane.Ilosc).ToString(CultureInfo.InvariantCulture);
            if (dane.CenaTowaru != null) xlDane.CenaTowaru = ((decimal)dane.CenaTowaru).ToString(CultureInfo.InvariantCulture);
            if (dane.CenaPoRabacie != null) xlDane.CenaPoRabacie = ((decimal)dane.CenaPoRabacie).ToString(CultureInfo.InvariantCulture);
            if (dane.WartoscPoRabacie != null) xlDane.WartoscPoRabacie = ((decimal)dane.WartoscPoRabacie).ToString(CultureInfo.InvariantCulture);
            if (dane.CenaKsiegowa != null) xlDane.CenaKsiegowa = ((decimal)dane.CenaKsiegowa).ToString(CultureInfo.InvariantCulture);
            if (dane.WartoscKsiegowa != null) xlDane.WartoscKsiegowa = ((decimal)dane.WartoscKsiegowa).ToString(CultureInfo.InvariantCulture);
            if (dane.Akronim != null) xlDane.Akronim = dane.Akronim;
            if (dane.TowarKod != null) xlDane.TowarKod = dane.TowarKod;
            if (dane.WalutaTowaru != null) xlDane.WalutaTowaru = dane.WalutaTowaru;
            
            int errCode = cdn_api.cdn_api.XLPrzeliczRabat(xlDane);
            if (errCode != 0)
            {
                throw new XLApiRetryException(string.Format("{0}, Kod błędu = {1}.", PobierzInfoOBledzieXLApi(XLNumerFunkcji.PrzeliczRabat, errCode), errCode));
            }

            dane.Typ = xlDane.Typ;
            dane.Data = xlDane.Data.ToDateTimeFromXLCzas();
            dane.ExpoNorm = xlDane.ExpoNorm;
            dane.FormaPl = xlDane.FormaPl;
            dane.RabatFormyPlatnosci = xlDane.RabatFormyPlatnosci;
            dane.RabatGlobalny = xlDane.RabatGlobalny;
            dane.LiczOdWartosciKsiegowej = xlDane.LiczOdWartosciKsiegowej;
            dane.TypDopasowania = xlDane.TypDopasowania;
            dane.IloscCenaWartosc = xlDane.IloscCenaWartosc;
            dane.Dokladnosc = xlDane.Dokladnosc;
            dane.WymusZmianeWartosci = xlDane.WymusZmianeWartosci;
            dane.NumerKursuWaluty = xlDane.NumerKursuWaluty;
            dane.MianownikKursuWaluty = xlDane.MianownikKursuWaluty;
            dane.LicznikKursuWaluty = xlDane.LicznikKursuWaluty;
            dane.TypRabatuKontrahentaNaTowar = xlDane.TypRabatuKontrahentaNaTowar;
            dane.RabatKontrahentaNaTowar = xlDane.RabatKontrahentaNaTowar;
            dane.TypProgu = xlDane.TypProgu;
            dane.WartoscProgowa = xlDane.WartoscProgowa;
            dane.TypRabatuProgowego = xlDane.TypRabatuProgowego;
            dane.RabatProgowy = xlDane.RabatProgowy;
            dane.Flagi = xlDane.Flagi;
            dane.NumerCenyPoczatkowej = xlDane.NumerCenyPoczatkowej;
            dane.RabatEfektywny = xlDane.RabatEfektywny;
            dane.GIDTyp = xlDane.GIDTyp;
            dane.GIDFirma = xlDane.GIDFirma;
            dane.GIDNumer = xlDane.GIDNumer;
            dane.GIDLp = xlDane.GIDLp;
            dane.ZstTyp = xlDane.ZstTyp;
            dane.ZstFirma = xlDane.ZstFirma;
            dane.ZstNumer = xlDane.ZstNumer;
            dane.ZstLp = xlDane.ZstLp;
            dane.FlagaNB = xlDane.FlagaNB;
            dane.Ilosc = Convert.ToDecimal(String.IsNullOrEmpty(xlDane.Ilosc) ? "0" : xlDane.Ilosc, CultureInfo.InvariantCulture);
            dane.CenaTowaru = Convert.ToDecimal(String.IsNullOrEmpty(xlDane.CenaTowaru) ? "0" : xlDane.CenaTowaru, CultureInfo.InvariantCulture);
            dane.CenaPoRabacie = Convert.ToDecimal(String.IsNullOrEmpty(xlDane.CenaPoRabacie) ? "0" : xlDane.CenaPoRabacie, CultureInfo.InvariantCulture);
            dane.WartoscPoRabacie = Convert.ToDecimal(String.IsNullOrEmpty(xlDane.WartoscPoRabacie) ? "0" : xlDane.WartoscPoRabacie, CultureInfo.InvariantCulture);
            dane.CenaKsiegowa = Convert.ToDecimal(String.IsNullOrEmpty(xlDane.CenaKsiegowa) ? "0" : xlDane.CenaKsiegowa, CultureInfo.InvariantCulture);
            dane.WartoscKsiegowa = Convert.ToDecimal(String.IsNullOrEmpty(xlDane.WartoscKsiegowa) ? "0" : xlDane.WartoscKsiegowa, CultureInfo.InvariantCulture);
            dane.Akronim = xlDane.Akronim;
            dane.TowarKod = xlDane.TowarKod;
            dane.WalutaTowaru = xlDane.WalutaTowaru;

            return dane;
        }

        private static int? UzupelnijXLApiTyp(int gidTyp, int? podTyp = null)
        {
            var t = XLApiTypList._Types.Where(m => m.GIDTyp == gidTyp && m.PodTyp == podTyp).ToList();
            var ret = t.Count == 1 ? t.Single().ApiTyp : null;
            return ret;
        }

    }
}