﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using Ionic.Zlib;
using TaskProcessor.Common;
using TaskProcessor.Plugin.ERPXL.Common;

namespace TaskProcessor.Plugin.ERPXL
{
    public partial class Plugin
    {
        public static bool ZalacznikiDodajDoObiektu(List<XLZalacznik> zalaczniki, string connStr = null)
        {
            bool ret = false;

            using (var conn = new SqlConnection(connStr ?? Helpers.GetSettingsValue("ConnectionString", true)))
            {
                var wersjaXL = NarzedziaPobierzWersjeXL(connStr);

                conn.Open();

                int? obiTyp = null;
                int? obiNumer = null;

                if (zalaczniki.Any())
                {
                    obiTyp = zalaczniki.First().ObiTyp;
                    obiNumer = zalaczniki.First().ObiNumer;
                }
                else
                {
                    throw new Exception("Błąd dodawania załączników - uzupełnij listę załączników.");
                }

                foreach (var el in zalaczniki)
                {
                    using (var ts = new TransactionScope())
                    {
                        string retId;
                        string cmdText;

                        if (wersjaXL.Rok >= 2015)
                        {
                            cmdText = @"
                                INSERT INTO
                                    CDN.DaneBinarne
                                    (DAB_Jezyk, DAB_Nazwa, DAB_TypId, DAB_Rozszerzenie, DAB_Rozmiar, DAB_Dane,
                                    DAB_Kod, DAB_CzasModyfikacji, DAB_OpeNumer, DAB_CzasDodania, DAB_OpeNumerD, DAB_Usuwac, DAB_Archiwalny,
                                    DAB_Aktywny, DAB_CzasArchiwizacji, DAB_OpisArchiwizacji, DAB_PPPrawa, DAB_PKPrawa, DAB_eSklep, DAB_iMall,
                                    DAB_MobSpr, DAB_BI, DAB_DBGId, DAB_Systemowa, DAB_ProcID, DAB_ZewnetrznySys, DAB_ZewnetrznyId,
                                    DAB_GPSSz, DAB_GPSDl, DAB_GPSDataPobrania, DAB_GPSGodzinaPobrania, DAB_eSklepTypDanych, DAB_ZewnetrznyTyp, DAB_Retail, DAB_Opis)
                                VALUES
                                    (0, @DAB_Nazwa, @DAB_TypId, @DAB_Rozszerzenie, 0, @DAB_Dane, @DAB_Kod, @DAB_CzasModyfikacji, 1, @DAB_CzasDodania, 1, 0, 0, 0, 2000000000, '', 0, @DAB_PKPrawa, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.000000, 0.000000, 0, 0, 1, 0, 0, '');
                                SELECT SCOPE_IDENTITY();";
                        }
                        else
                        {
                            cmdText = @"
                                INSERT INTO
                                    CDN.DaneBinarne
                                    (DAB_Jezyk, DAB_Nazwa, DAB_TypId, DAB_Rozszerzenie, DAB_Rozmiar, DAB_Dane,
                                    DAB_Kod, DAB_CzasModyfikacji, DAB_OpeNumer, DAB_CzasDodania, DAB_OpeNumerD, DAB_Usuwac, DAB_Archiwalny,
                                    DAB_Aktywny, DAB_CzasArchiwizacji, DAB_OpisArchiwizacji, DAB_PPPrawa, DAB_PKPrawa, DAB_eSklep, DAB_iMall,
                                    DAB_MobSpr, DAB_BI, DAB_DBGId, DAB_Systemowa, DAB_ProcID, DAB_ZewnetrznySys, DAB_ZewnetrznyId,
                                    DAB_GPSSz, DAB_GPSDl, DAB_GPSDataPobrania, DAB_GPSGodzinaPobrania, DAB_eSklepTypDanych)
                                VALUES
                                    (0, @DAB_Nazwa, @DAB_TypId, @DAB_Rozszerzenie, 0, @DAB_Dane, @DAB_Kod, @DAB_CzasModyfikacji, 1, @DAB_CzasDodania, 1, 0, 0, 0, 2000000000, '', 0, @DAB_PKPrawa, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.000000, 0.000000, 0, 0, 1);
                                SELECT SCOPE_IDENTITY();";
                        }

                        using (var cmd = new SqlCommand(cmdText, conn))
                        {
                            cmd.Parameters.Add("@DAB_Nazwa", SqlDbType.NVarChar).Value = el.NazwaPliku;
                            cmd.Parameters.Add("@DAB_Kod", SqlDbType.NVarChar).Value = el.Kod ?? (el.NazwaPliku.Length > 39 ? el.NazwaPliku.Substring(0, 39) : el.NazwaPliku); // + '_' + Guid.NewGuid().ToString().Substring(0, 8);
                            cmd.Parameters.Add("@DAB_Dane", SqlDbType.VarBinary).Value = el.Dane;
                            cmd.Parameters.Add("@DAB_PKPrawa", SqlDbType.Int).Value = (el.DostepnoscWPulpicieKnt != null && (bool)el.DostepnoscWPulpicieKnt) ? 1 : 0;
                            cmd.Parameters.Add("@DAB_CzasModyfikacji", SqlDbType.Int).Value = el.CzasModyfikacji != null ? el.CzasModyfikacji.ToXLCzas() : (int)DateTime.Now.Subtract(new DateTime(1990, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
                            cmd.Parameters.Add("@DAB_CzasDodania", SqlDbType.Int).Value = el.CzasDodania != null ? el.CzasDodania.ToXLCzas() : (int)DateTime.Now.Subtract(new DateTime(1990, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
                            cmd.Parameters.Add("@DAB_Rozszerzenie", SqlDbType.VarChar).Value = el.RozszerzeniePliku.Replace(".", "");
                            cmd.Parameters.Add("@DAB_TypId", SqlDbType.Int).Value = el.TypDanych ?? DaneBinarneUstalTypDanych(el.RozszerzeniePliku.Replace(".", ""));

                            retId = cmd.ExecuteScalar().ToString();
                        }

                        int dabId;
                        if (int.TryParse(retId, out dabId))
                        {
                            if (wersjaXL.Rok >= 2015)
                            {
                                cmdText = @"
                                    INSERT INTO
                                        CDN.DaneObiekty (DAO_DABId, DAO_ObiTyp, DAO_ObiNumer, DAO_ObiLp, DAO_ObiSubLp, DAO_Pozycja, DAO_Domyslna, DAO_Blokada, DAO_PPPrawa, DAO_PKPrawa, DAO_eSklep, DAO_iMall, DAO_MobSpr, DAO_BI, DAO_Systemowa)
                                    VALUES
                                        (@DAO_DABId, @DAO_ObiTyp, @DAO_ObiNumer, 0, 0, (ISNULL((SELECT MAX(DAO_Pozycja) FROM CDN.DaneObiekty WHERE DAO_ObiTyp = @DAO_ObiTyp AND DAO_ObiNumer = @DAO_ObiNumer), 0) + 1), 0, 0, 0, 0, 0, 0, 0, 0, 0);";
                            }
                            else
                            {
                                cmdText = @"
                                    INSERT INTO
                                        CDN.DaneObiekty (DAO_DABId, DAO_ObiTyp, DAO_ObiNumer, DAO_ObiLp, DAO_ObiSubLp, DAO_Domyslna, DAO_Blokada, DAO_PPPrawa, DAO_PKPrawa, DAO_eSklep, DAO_iMall, DAO_MobSpr, DAO_BI, DAO_Systemowa)
                                    VALUES
                                        (@DAO_DABId, @DAO_ObiTyp, @DAO_ObiNumer, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);";
                            }

                            using (var cmd = new SqlCommand(cmdText, conn))
                            {
                                cmd.Parameters.Add("@DAO_DABId", SqlDbType.Int).Value = dabId;
                                cmd.Parameters.Add("@DAO_ObiTyp", SqlDbType.Int).Value = obiTyp;
                                cmd.Parameters.Add("@DAO_ObiNumer", SqlDbType.Int).Value = obiNumer;
                                if (cmd.ExecuteNonQuery() > 0)
                                {
                                    ret = true;
                                }
                            }
                        }

                        ts.Complete();
                    }
                }
            }

            return ret;
        }

        public static bool ZalacznikModyfikuj(XLZalacznik zalacznik, string connStr = null)
        {
            bool ret = true;

            using (var conn = new SqlConnection(connStr ?? Helpers.GetSettingsValue("ConnectionString", true)))
            {
                conn.Open();

                using (var ts = new TransactionScope())
                {
                    var sql = "UPDATE CDN.DaneBinarne SET DAB_Jezyk = DAB_Jezyk";

                    if (zalacznik.Dane != null) sql += ", DAB_Dane = @DAB_Dane";
                    if (zalacznik.Kod != null) sql += ", DAB_Kod = @DAB_Kod";
                    if (zalacznik.NazwaPliku != null) sql += ", DAB_Nazwa = @DAB_Nazwa";
                    if (zalacznik.RozszerzeniePliku != null) sql += ", DAB_Rozszerzenie = @DAB_Rozszerzenie";
                    if (zalacznik.TypDanych != null) sql += ", DAB_TypId = @DAB_TypId";
                    if (zalacznik.CzasDodania != null) sql += ", DAB_CzasDodania = @DAB_CzasDodania";
                    if (zalacznik.CzasModyfikacji != null) sql += ", DAB_CzasModyfikacji = @DAB_CzasModyfikacji";

                    sql += " WHERE DAB_ID = @Id";

                    var cmd = new SqlCommand(sql, conn);

                    cmd.Parameters.AddWithValue("@Id", zalacznik.Id);
                    if (zalacznik.Dane != null) cmd.Parameters.AddWithValue("@DAB_Dane", zalacznik.Dane);
                    if (zalacznik.Kod != null) cmd.Parameters.AddWithValue("@DAB_Kod", zalacznik.Kod);
                    if (zalacznik.NazwaPliku != null) cmd.Parameters.AddWithValue("@DAB_Nazwa", zalacznik.NazwaPliku);
                    if (zalacznik.RozszerzeniePliku != null) cmd.Parameters.AddWithValue("@DAB_Rozszerzenie", zalacznik.RozszerzeniePliku);
                    if (zalacznik.TypDanych != null) cmd.Parameters.AddWithValue("@DAB_TypId", zalacznik.TypDanych);
                    if (zalacznik.CzasDodania != null) cmd.Parameters.AddWithValue("@DAB_CzasDodania", zalacznik.CzasDodania.ToXLCzas());
                    if (zalacznik.CzasModyfikacji != null) cmd.Parameters.AddWithValue("@DAB_CzasModyfikacji", zalacznik.CzasModyfikacji.ToXLCzas());

                    if (cmd.ExecuteNonQuery() == 0)
                    {
                        ret = false;
                    }

                    ts.Complete();
                }
            }

            return ret;
        }

        public static List<XLZalacznik> ZalacznikiWczytajListe(XLObiektOgolny obiekt, bool? czyWczytacDaneBinarnePlikow = true, string connStr = null)
        {
            var ret = new List<XLZalacznik>();

            using (var conn = new SqlConnection(connStr ?? Helpers.GetSettingsValue("ConnectionString", true)))
            {
                var wersjaXL = NarzedziaPobierzWersjeXL(connStr);

                string cmdText;

                if (wersjaXL.Rok >= 2015)
                {
                    cmdText = "SELECT DAO_DABId, DAO_ObiTyp, DAO_ObiNumer, DAO_ObiLp, DAB_CzasModyfikacji, DAB_CzasDodania, DAB_Nazwa, DAB_Rozszerzenie, DAB_Dane, DAB_Rozmiar, DAB_TypId, DAB_Kod, DAB_PKPrawa FROM CDN.DaneObiekty INNER JOIN CDN.DaneBinarne ON DAO_DABId = DAB_ID WHERE DAO_ObiTyp = @DAO_ObiTyp AND DAO_ObiNumer = @DAO_ObiNumer ORDER BY DAO_Pozycja";
                }
                else
                {
                    cmdText = "SELECT DAO_DABId, DAO_ObiTyp, DAO_ObiNumer, DAO_ObiLp, DAB_CzasModyfikacji, DAB_CzasDodania, DAB_Nazwa, DAB_Rozszerzenie, DAB_Dane, DAB_Rozmiar, DAB_TypId, DAB_Kod, DAB_PKPrawa FROM CDN.DaneObiekty INNER JOIN CDN.DaneBinarne ON DAO_DABId = DAB_ID WHERE DAO_ObiTyp = @DAO_ObiTyp AND DAO_ObiNumer = @DAO_ObiNumer ORDER BY 1";
                }
            
                var cmd = new SqlCommand(cmdText, conn);

                cmd.Parameters.Add("@DAO_ObiTyp", SqlDbType.Int).Value = obiekt.GIDTyp;
                cmd.Parameters.Add("@DAO_ObiNumer", SqlDbType.Int).Value = obiekt.GIDNumer;

                conn.Open();
                
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        byte[] fileData = null;
                        if ((bool)czyWczytacDaneBinarnePlikow)
                        {
                            fileData = (byte[]) reader["DAB_Dane"];
                            if (Int32.Parse(reader["DAB_Rozmiar"].ToString()) > 0)
                            {
                                fileData = ZlibStream.UncompressBuffer(fileData);
                            }
                        }
                        ret.Add(new XLZalacznik
                        {
                            Id = Int32.Parse(reader["DAO_DABId"].ToString()),
                            ObiTyp = Int32.Parse(reader["DAO_ObiTyp"].ToString()),
                            ObiNumer = Int32.Parse(reader["DAO_ObiNumer"].ToString()),
                            ObiLp = Int32.Parse(reader["DAO_ObiLp"].ToString()),
                            Dane = fileData,
                            Kod = (string)reader["DAB_Kod"],
                            DostepnoscWPulpicieKnt = (Int32.Parse(reader["DAB_PKPrawa"].ToString()) == 1),
                            TypDanych = Int32.Parse(reader["DAB_TypId"].ToString()),
                            CzasModyfikacji = Int32.Parse(reader["DAB_CzasModyfikacji"].ToString()).ToDateTimeFromXLCzas(),
                            CzasDodania = Int32.Parse(reader["DAB_CzasDodania"].ToString()).ToDateTimeFromXLCzas(),
                            NazwaPliku = reader["DAB_Nazwa"].ToString(),
                            RozszerzeniePliku = reader["DAB_Rozszerzenie"].ToString(),
                        });
                    }
                }

            }

            return ret;
        }

        private static int DaneBinarneUstalTypDanych(string ext)
        {
            int ret = 329; // archiwum

            switch (ext)
            {
                case "jpg":
                case "jpeg":
                case "bmp":
                case "gif":
                case "tiff":
                case "wmf":
                case "png":
                    {
                        ret = 326; // obraz
                        break;
                    }
                case "doc":
                case "docx":
                    {
                        ret = 327; // MS Word
                        break;
                    }
                case "xls":
                case "xlsx":
                    {
                        ret = 328; // MS Excel
                        break;
                    }
                case "pdf":
                    {
                        ret = 884; // PDF
                        break;
                    }
            }

            return ret;
        }

    }
}
