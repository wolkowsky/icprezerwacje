﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using cdn_api;
using TaskProcessor.Common;
using TaskProcessor.Plugin.ERPXL.Common;

namespace TaskProcessor.Plugin.ERPXL
{
	public partial class Plugin
	{
		public static int SesjaXLUtworz(Guid instanceGuid, int czyTrybWsadowy = 1, int czyUtworzWlasnaSesje = 1)
		{
			int ret = XLApiBlednaSesja;

			var xlLogin = new XLLoginInfo_20141();
			xlLogin.Wersja = XLApiWersja;
			xlLogin.ProgramID = string.Format("{0} {1}", XLApiAplikacja, instanceGuid);
			xlLogin.Winieta = -1;
			xlLogin.TrybWsadowy = czyTrybWsadowy;
			xlLogin.TrybNaprawy = 1;
			xlLogin.UtworzWlasnaSesje = czyUtworzWlasnaSesje;

			xlLogin.SerwerKlucza = Helpers.GetSettingsValue("SerwerKlucza") ?? "";
			xlLogin.OpeIdent = Helpers.GetSettingsValue("OpeIdent");
			xlLogin.OpeHaslo = Helpers.GetSettingsValue("OpeHaslo");
			xlLogin.Baza = Helpers.GetSettingsValue("Baza");

			int errCode = cdn_api.cdn_api.XLLogin(xlLogin, ref ret);
			if (errCode != 0)
			{
				throw new XLApiFatalException("Wystąpił błąd przy próbie zalogowania do ERP XL. Kod błędu: " + errCode + ", Info: " + InfoOBledzieXLApi(errCode));
			}

			Logger.LogMessage(string.Format("Zalogowano do ERP XL [Baza = {0}, OpeIdent = {1}, IdSesji = {2}]", Helpers.GetSettingsValue("Baza"), Helpers.GetSettingsValue("OpeIdent"), ret), instanceGuid, PluginDataInfo.PluginName);

			return ret;
		}

		public static void SesjaXLZakoncz(int idSesji, Guid? instanceGuid = null)
		{
			if (idSesji > 0)
			{
				int errCode = cdn_api.cdn_api.XLLogout(idSesji);
				if (errCode != 0)
				{
					Logger.LogMessage("Wystąpił błąd przy próbie wylogowania z ERP XL. Kod błędu: " + errCode + ", Info: " + InfoOBledzieWylogowywania(errCode), instanceGuid ?? PluginDataInfo.PluginGuid, PluginDataInfo.PluginName);
				}
				Logger.LogMessage(string.Format("Wylogowano z ERP XL [IdSesji = {0}]", idSesji), instanceGuid ?? PluginDataInfo.PluginGuid, PluginDataInfo.PluginName);
			}
			else
			{
				Logger.LogMessage(string.Format("Brak konieczności wylogowania z ERP XL - błędny numer sesji [IdSesji = {0}]", idSesji), instanceGuid ?? PluginDataInfo.PluginGuid, PluginDataInfo.PluginName);
			}
		}
		
		public static bool SesjaXLSprawdzPolaczenie(int idSesji)
		{
			bool ret = true;
			int polErrCode = cdn_api.cdn_api.XLPolaczenie(new XLPolaczenieInfo_20141 { Wersja = XLApiWersja });
			int licErrCode = cdn_api.cdn_api.XLSprawdzLicencje(new XLLicencjaInfo_20141 { Wersja = XLApiWersja, Licencja = 1, Odswiez = 1 });
			bool czySesjaAktywna = SprawdzCzySesjaJestAktywna(idSesji);
			if (polErrCode != 0 || licErrCode != 0 || !czySesjaAktywna)
			{
				ret = false;
			}
			return ret;
		}

		public static int SesjaXLPrzeloguj(int idSesji, Guid? instanceGuid = null)
		{
			SesjaXLZakoncz(idSesji, instanceGuid);
			return SesjaXLUtworz(instanceGuid ?? Guid.NewGuid());
		}

		private void ZapewnijSesjeXLApi()
		{
			if (!SesjaXLSprawdzPolaczenie(IdSesji))
			{
				Logger.LogMessage(string.Format("Wykryto błędną sesję połączenia do ERP XL [IdSesji = {0}]", IdSesji), InstanceGuid, PluginName);
				IdSesji = SesjaXLPrzeloguj(IdSesji, InstanceGuid);
			}
		}

		private static bool SprawdzCzySesjaJestAktywna(int idSesji, string connStr = null)
		{
			using (var conn = new SqlConnection(connStr ?? Helpers.GetSettingsValue("ConnectionString", true)))
			{
				var cmd = new SqlCommand(@"SELECT ISNULL((SELECT CASE WHEN SES_Aktywna = 0 THEN 1 ELSE 0 END FROM CDN.Sesje WHERE SES_SesjaID = @SesjaId), 0)", conn);

				cmd.Parameters.AddWithValue("@SesjaId", idSesji);

				conn.Open();

				using (var reader = cmd.ExecuteReader())
				{
					reader.Read();
					return (reader.GetInt32(0) == 1);
				}
			}
		}

		public static int SesjaXLOdswiezWszystkieLicencje(Guid? instanceGuid = null)
		{
			var lic = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 17, 19, 101, 301, 302, 303, 304, 305, 306, 406, 501, 502 };
			int sumLic = 0;
			foreach (var i in lic)
			{
				sumLic += cdn_api.cdn_api.XLSprawdzLicencje(new XLLicencjaInfo_20141 { Wersja = XLApiWersja, Licencja = i, Odswiez = 1 });
			}
			Logger.LogMessage("Odświeżono wszystkie licencje" + (sumLic > 0 ? " i pobrano nowe" : "") + ".", instanceGuid ?? PluginDataInfo.PluginGuid, PluginDataInfo.PluginName);
			return (sumLic > 0 ? 1 : 0);
		}

		public static List<XLKodLicencji> WczytajModulyUzytkownika(string opeIdent, string connStr = null)
		{
			var ret = new List<XLKodLicencji>();

			using (var conn = new SqlConnection(connStr ?? Helpers.GetSettingsValue("ConnectionString", true)))
			{
				string cmdText = @"
					DECLARE @Moduly TABLE (
						KodApiModulu INT,
						Bajt INT
						PRIMARY KEY (KodApiModulu)
					)

					INSERT INTO @Moduly SELECT 3, 1 -- sprzedaz
					INSERT INTO @Moduly SELECT 4, 2 -- ksiegowosc
					INSERT INTO @Moduly SELECT 2, 128 -- administracja
					INSERT INTO @Moduly SELECT 6, 8 -- srodki trwale
					INSERT INTO @Moduly SELECT 8, 32 -- import
					INSERT INTO @Moduly SELECT 101, 524288 -- projekty
					INSERT INTO @Moduly SELECT 11, 512 -- zamowienia
					INSERT INTO @Moduly SELECT 12, 1024 -- serwis
					INSERT INTO @Moduly SELECT 9, 64 -- CRM
					INSERT INTO @Moduly SELECT 5, 4 -- kompletacja
					INSERT INTO @Moduly SELECT 19, 262144 -- produkcja
					INSERT INTO @Moduly SELECT 13, 4096 -- administrator oddzialow

					SELECT DISTINCT
						KodApiModulu
					FROM
						CDN.OpeKarty
						INNER JOIN @Moduly
							ON Ope_Moduly & Bajt > 0
					WHERE
						Ope_Ident = @OpeIdent
				";

				var cmd = new SqlCommand(cmdText, conn);

				cmd.Parameters.AddWithValue("@OpeIdent", opeIdent);

				conn.Open();

				using (var reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						ret.Add((XLKodLicencji)reader.GetInt32(0));
					}
				}
			}

			return ret;
		}

		public static XLInfoOLicencjach NarzedziaPobierzInfoOLicencjach(string opeIdent, string connStr = null)
		{
			var ret = new XLInfoOLicencjach();

			var wlaczoneModuly = WczytajModulyUzytkownika(opeIdent);
			wlaczoneModuly.Add(XLKodLicencji.LicencjaStanowiskowa);

			var xlLic = new XLLicencjaInfo_20141 { Wersja = XLApiWersja, Licencja = (int)XLKodLicencji.LicencjaStanowiskowa, Odswiez = 0 };
			ret.StanKluczaHasp = (XLStanKluczaHasp)cdn_api.cdn_api.XLSprawdzLicencje(xlLic);

			foreach (var mod in wlaczoneModuly)
			{
				xlLic = new XLLicencjaInfo_20141 { Wersja = XLApiWersja, Licencja = (int) mod, Odswiez = 0 };
				cdn_api.cdn_api.XLSprawdzLicencje(xlLic);
				if (xlLic.Stan != 1 && xlLic.Stan != 4)
				{
					ret.NieaktywneLicencje.Add(mod);
				}
			}

			return ret;
		}

		private static string InfoOBledzieXLApi(int errCode)
		{
			string msg = "";

			switch (errCode)
			{
				case -8:
				{
					msg = "nie podano nazwy bazy";
					break;
				}
				case -7:
				{
					msg = "baza niezarejestrowana w systemie";
					break;
				}
				case -6:
				{
					msg = "nie podano hasła lub brak operatora";
					break;
				}
				case -5:
				{
					msg = "nieprawidłowe hasło";
					break;
				}
				case -4:
				{
					msg = "konto operatora zablokowane";
					break;
				}
				case -3:
				{
					msg = "nie podano nazwy programu (pole ProgramID)";
					break;
				}
				case -2:
				{
					msg = "błąd otwarcia pliku tekstowego, do którego mają być zapisywane komunikaty. Nie znaleziono ścieżki lub nazwa pliku jest nieprawidłowa.";
					break;
				}
				case -1:
				{
					msg = "podano niepoprawną wersję API";
					break;
				}
				case 0:
				{
					msg = "logowanie powiodło się";
					break;
				}
				case 1:
				{
					msg = "inicjalizacja nie powiodła się";
					break;
				}
				case 2:
				{
					msg = "istnieje już jedna instancja programu i nastąpi ponowne logowanie (z tego samego komputera i na tego samego operatora)";
					break;
				}
				case 3:
				{
					msg = "istnieje już jedna instancja programu i nastąpi ponowne logowanie z innego komputera i na tego samego operatora, ale operator nie posiada prawa do wielokrotnego logowania";
					break;
				}
				case 5:
				{
					msg = "przy pracy terminalowej w przypadku, gdy operator nie ma prawa do wielokrotnego logowania i na pytanie czy usunąć istniejące sesje terminalowe wybrano odpowiedź ‘Nie’.";
					break;
				}
				case 61:
				{
					msg = "błąd zakładania nowej sesji";
					break;
				}
				default:
				{
					msg = "brak informacji o kodzie błędu";
					break;
				}
			}

			return msg;
		}

		private static string InfoOBledzieWylogowywania(int errCode)
		{
			string msg = "";

			switch (errCode)
			{
				case -2:
				{
					msg = "błąd otwarcia pliku tekstowego, do którego mają być zapisywane komunikaty";
					break;
				}
				case -1:
				{
					msg = "nie zawołano poprawnie XLLogin";
					break;
				}
				case 0:
				{
					msg = "OK.";
					break;
				}
				case 2:
				{
					msg = "istnieje już jedna instancja programu i nastąpi ponowne logowanie (z tego samego komputera i na tego samego operatora)";
					break;
				}
				case 3:
				{
					msg = "istnieje już jedna instancja programu i nastąpi ponowne logowanie z innego komputera i na tego samego operatora, ale operator nie posiada prawa do wielokrotnego logowania";
					break;
				}
				case 5:
				{
					msg = "operator nie ma prawa do wielokrotnego logowania i na pytanie czy usunąć istniejące sesje terminalowe wybrano odpowiedź ‘Nie’.";
					break;
				}
				default:
				{
					msg = "brak informacji o kodzie błędu";
					break;
				}
			}

			return msg;
		}

		private static string InfoOBledzieDodajAtrybut(int errCode)
		{
			string msg = "";

			switch (errCode)
			{
				case -1:
					{
						msg = "brak sesji";
						break;
					}
				case 2:
					{
						msg = "błąd przy zakładaniu logout";
						break;
					}
				case 3:
					{
						msg = "nie znaleziono obiektu";
						break;
					}
				case 4:
					{
						msg = "nie znalezniono klasy atrybutu";
						break;
					}
				case 5:
					{
						msg = "klasa nieprzypisana do definicji obiektu";
						break;
					}
				case 6:
					{
						msg = "atrybut juz istnieje w kolejce";
						break;
					}
				case 7:
					{
						msg = "błąd ADO Connection";
						break;
					}
				case 8:
					{
						msg = "błąd ADO";
						break;
					}
				case 9:
					{
						msg = "brak zdefiniowanego obiektu";
						break;
					}
				default:
					{
						msg = "brak informacji o kodzie błędu";
						break;
					}
			}

			return msg;
		}

		private static string InfoOBledzieModyfikujPozycjeZam(int errCode)
		{
			string msg = "";

			switch (errCode)
			{
				case 8:
					{
						msg = "błąd podczas operacji na tabeli ObiektyObce";
						break;
					}
				case 9:
					{
						msg = "błąd pobierania zamówienia";
						break;
					}
				case 10:
					{
						msg = "pozycje można dodawać tylko do niepotwierdzonego zamówienia lub oferty";
						break;
					}
				case 11:
					{
						msg = "nie podano ilości towaru";
						break;
					}
				case 38:
					{
						msg = "błędnie podana waluta";
						break;
					}
				case 39:
					{
						msg = "błędny kurs waluty";
						break;
					}
				case 41:
					{
						msg = "brak praw wynikająca z definicji dokumentu";
						break;
					}
				default:
					{
						msg = "brak informacji o kodzie błędu";
						break;
					}
			}

			return msg;
		}

		private static string InfoOBledzieUsunPozycjeZam(int errCode)
		{
			string msg = "";

			switch (errCode)
			{
				case 1:
					{
						msg = "błąd usuwania pozycji";
						break;
					}
				case 8:
					{
						msg = "błąd podczas operacji na tabeli ObiektyObce";
						break;
					}
				case 9:
					{
						msg = "błąd pobierania zamówienia";
						break;
					}
				case 10:
					{
						msg = "pozycje można dodawać tylko do niepotwierdzonego zamówienia lub oferty";
						break;
					}
				case 41:
					{
						msg = "brak praw wynikająca z definicji dokumentu";
						break;
					}
				default:
					{
						msg = "brak informacji o kodzie błędu";
						break;
					}
			}

			return msg;
		}


		private static string InfoOBledzieDodajRezerwacje(int errCode)
		{
			string msg = "";

			switch (errCode)
			{
				case 1:
					{
						msg = "Nie podano towaru lub podano błędny towar.";
						break;
					}
				case 2:
					{
						msg = "Nie podano kontrahenta lub podano błędnego kontrahenta.";
						break;
					}
				case 3:
					{
						msg = "Nie podano magazynu lub brak praw do magazynu.";
						break;
					}
				case 4:
					{
						msg = "Podano nie istniejące centrum.";
						break;
					}
				case 5:
					{
						msg = "Nie podano ilości.";
						break;
					}
				case 6:
					{
						msg = "Podano nie istniejący priorytet.";
						break;
					}
				case 7:
					{
						msg = "Podany kontrahent jest archiwalny.";
						break;
					}
				case 8:
					{
						msg = "Błąd dodawania rekordu do bazy.";
						break;
					}
				case 9:
					{
						msg = "Błąd dodawania magazynu do rezerwacji";
						break;
					}
				default:
					{
						msg = "brak informacji o kodzie błędu";
						break;
					}
			}

			return msg;
		}

		private static string InfoOBledzieDodajZasobDoRezerwacji(int errCode)
		{
			string msg = "";

			switch (errCode)
			{
				case 1:
					{
						msg = "Błąd odczytu rezerwacji.";
						break;
					}
				case 2:
					{
						msg = "Błąd przypinania zasobów.";
						break;
					}
				case 3:
					{
						msg = "Rezerwacja nie blokuje zasobów.";
						break;
					}
				case 4:
					{
						msg = "Za mało zasobów.";
						break;
					}
				default:
					{
						msg = "brak informacji o kodzie błędu";
						break;
					}
			}

			return msg;
		}

		private static string InfoOBledzieUsunRezerwacje(int errCode)
		{
			string msg = "";

			switch (errCode)
			{
				case 1:
					{
						msg = "Błąd odczytu rezerwacji.";
						break;
					}
				default:
					{
						msg = "brak informacji o kodzie błędu";
						break;
					}
			}
			return msg;
		}

		private static string PobierzInfoOBledzieXLApi(XLNumerFunkcji functionId, int errorCode)
		{
			var xlError = new XLKomunikatInfo_20141();
			xlError.Wersja = XLApiWersja;
			xlError.Funkcja = (int)functionId;
			xlError.Blad = errorCode;
			xlError.Tryb = 0;
			cdn_api.cdn_api.XLOpisBledu(xlError);
			return xlError.OpisBledu;
		}
	}

	internal enum XLNumerFunkcji
	{
		_Brak = 0,
		NowyDokument = 1,
		DodajPozycje = 2,
		DodajSubPozycje = 3,
		UsunPozycje = 4,
		DodajVat = 5,
		DodajPlatnosc = 6,
		ZamknijDokument = 7,
		NowyDokumentMag = 8,
		DodajPozycjeMag = 9,
		UsunPozycjeMag = 10,
		ZamknijDokumentMag = 11,
		NowyKontrahent = 12,
		ZamknijKontrahent = 13,
		NowyAdres = 14,
		ZmienAdres = 15,
		ZamknijAdres = 16,
		NowyDokumentImp = 17,
		DodajPozycjeImp = 18,
		UsunPozycjeImp = 19,
		ZamknijDokumentImp = 20,
		NowyDokumentSad = 21,
		DodajPozycjeSad = 22,
		DodajSubPozycjeSad = 23,
		UsunPozycjeSad = 24,
		ZamknijDokumentSad = 25,
		NowyTowar = 26,
		DodajCene = 27,
		ZamknijTowar = 28,
		ZmienCene = 29,
		NowaReceptura = 30,
		DodajSkladnikRecpetury = 31,
		ZamknijRecepture = 32,
		DodajDoSpinacza = 33,
		UsunZeSpinacza = 34,
		SAD2PZI = 35,
		DodajKwoteDodSAD = 36,
		OtworzDokumentHan = 37,
		OtworzDokumentMag = 38,
		OtworzDokumentImp = 39,
		OtworzDokumentSad = 40,
		NowyDokumentZam = 41,
		DodajPozycjeZam = 42,
		ZamknijDokumentZam = 43,
		NowyDokumentZlc = 44,
		DodajPozycjeZlc = 45,
		ZamknijDokumentZlc = 46,
		PrzeliczRabat = 47,
		NoweZlecenieSR = 48,
		DodajPozycjeZleceniaSR = 49,
		ZamknijZlecenieSR = 50,
		OtworzZlecenieSR = 51,
		ModyfikujVat = 52,
		NoweZlecenieProd = 53,
		DodajDoZleceniaProd = 54,
		ZamknijZlecenieProd = 55,
		OtworzZlecenieProd = 56,
		DodajSUBPozycjeMap = 58,
		NowyObiektProd = 61,
		NowaFunkcjaProd = 62,
		NowyObiektFunkcjaProd = 63,
		NowaTechnologia = 64,
		NowaCzynnoscTechnologia = 65,
		NowaFunkcjaCzynnoscTechnologia = 66,
		NowyZasobCzynnoscTechnologia = 67,
		DodajCzynnoscDoProcesuProd = 68,
		DodajZasobDoCzynnosciProd = 69,
		DodajObiektDoCzynnosciProd = 70,
		DodajTerminDoCzynnosciProd = 71,
		ModyfikujPozycje = 72,
		NowaPaczka = 75,
		ZamknijPaczke = 76,
		OtworzPaczke = 77,
		DodajDokumentDoPaczki = 78,
		UsunDokumentZPaczki = 79,
		NowaWysylka = 80,
		ZamknijWysylke = 81,
		OtworzWysylke = 82,
		DodajPaczkeDoWysylki = 83,
		UsunPaczkeZWysylki = 84,
		DodajKosztDoWysylki = 85,
		UsunKosztZWysylki = 86,
		WykonajPodanyWydruk = 87,
		PowiazZasobProd = 88,
		ModyfikujPłatność = 89,
		DodajCzynnoscSerwis = 90,
		DodajObiektSerwis = 91,
		DodajParametrUrzadzeniaSerwis = 92,
		DodajSkladnikSerwis = 93,
		DodajTerminCzynnosciSerwis = 94,
		DodajUrzadzenieSerwis = 95,
		DodajWlascicielaUrzadzeniaSerwis = 96,
		NoweUrzadzenieSerwis = 97,
		NoweZlecenieSerwis = 98,
		OtworzZlecenieSerwis = 99,
		ZamknijZlecenieSerwis = 100,
		ModyfikujePozycjeMagazynowa = 101,
		NowyOdczytInw = 102,
		DodajPozycjeOdczytuInw = 103,
		ZamknijOdczytInw = 104,
		OtworzOdczytInw = 105,
		DodajOpiekunaDoMag = 106,
		DodajZasobDoMag = 107,
		RealizujPozycjeMag = 108,
		NowySrt = 109,
		DodajDokumentSrt = 110,
		DodajPozycjeSrt = 111,
		ZamknijDokumentSrt = 112,
		UsunDokumentSrt = 113,
		UsunPozycjeDokSrt = 114,
		DodajDokumentUML = 115,
		ZamknijDokumentUML = 116,
		OtworzDokumentUML = 117,
		UsunDokumentUML = 118,
		DodajAneksUML = 119,
		DodajPrzedmiotUML = 120,
		UsunPrzedmiotUML = 121,
		ModyfikujPrzedmiotUML = 122,
		DodajRateUML = 123,
		UsunRateUML = 124,
		GenerujRatyUML = 125,
		UsunRalizacjeMag = 126,
		ZamknijRealizacjeMag = 127,
		OtworzDokumentZam = 128,
		DolaczDefinicjeParametruDoRodzajuUrzadzeniaSerwis = 129,
		NowyRodzajUrzadzeniaSerwis = 130,
	}

}