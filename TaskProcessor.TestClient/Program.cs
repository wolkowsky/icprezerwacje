﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading;
using TaskProcessor.Common;
using TaskProcessor.Plugin.ERPXL.Common;

namespace TaskProcessor.TestClient
{
    internal class Program
    {
        public static string connStr = ConfigurationManager.ConnectionStrings["MainConnectionString"].ConnectionString;

        public static void Main(string[] args)
        {
            //var typDok = XLApiTypList.DokumentZamowienieSprzedazy;

            //var obj = new XLDokZamowienieNag
            //{
            //    GIDTyp = typDok.GIDTyp,
            //    ApiTyp = typDok.ApiTyp,
            //    KontrahentGlowny = new XLKontrahent { GIDTyp = XLApiTypList.KontrahentOdbiorca.GIDTyp, GIDNumer = 158 },
            //    Elementy = new List<XLDokZamowienieElem>
            //    {
            //        new XLDokZamowienieElem
            //        {
            //            Ilosc = 2,
            //            Towar = new XLTowar { GIDTyp = XLApiTypList.TowarZwykly.GIDTyp, GIDNumer = 789 }
            //        },
            //        new XLDokZamowienieElem
            //        {
            //            Ilosc = 2,
            //            Wartosc = 205.5m,
            //            Towar = new XLTowar { GIDTyp = XLApiTypList.TowarZwykly.GIDTyp, GIDNumer = 789 }
            //        },
            //        new XLDokZamowienieElem
            //        {
            //            Ilosc = 5,
            //            Towar = new XLTowar { GIDTyp = XLApiTypList.TowarZwykly.GIDTyp, GIDNumer = 789 }
            //        }
            //    },
            //    Opis = "Dokument testowy",
            //    DataWaznosci = DateTime.Today.AddDays(30),
            //    TrybZamkniecia = XLTrybZamknieciaZam.Potwierdzenie,
            //};

            //var req = new RequestObject { MethodName = "NarzedziaWykonajPodanyWydruk", ObjJson = Helpers.SerializeJson(new XLDaneWydruku { IdZrodla = 0, IdWydruku = 237, IdFormatu = 1 }) };
            //var result = Helpers.CreateTaskAndWaitForResult(Plugin.ERPXL.Common.PluginDataInfo.PluginGuid, req, connStr: ConfigurationManager.ConnectionStrings["TaskProcessor.MainConnectionString"].ConnectionString);
            //File.WriteAllBytes(@"C:\DB\dupa.pdf", Convert.FromBase64String(result.Obj.ToString()));

            //for (int i = 0; i < 10; i++)
            //{
            //    var req = new RequestObject { MethodName = XLApiTypList.DokumentOfertaSprzedazy.NazwaMetody + XLTypDzialania.Utworz, ObjJson = Helpers.SerializeJson(obj) };
            //    var result = Helpers.CreateTaskAndWaitForResult(Plugin.ERPXL.Common.PluginDataInfo.PluginGuid, req, connStr: ConfigurationManager.ConnectionStrings["TaskProcessor.MainConnectionString"].ConnectionString);
            //    Console.WriteLine("Utworzono dokument: " + (result.TaskResultCode == TaskResultCode.OK ? NarzedziaPobierzNumerDok(960, Int32.Parse(result.Obj.ToString())) : result.Msg));

            //    //var req = new RequestObject { MethodName = XLApiPlugin.MainMethod, MethodParam = "NarzedziaWykonajPodanyWydruk", ObjJson = Helpers.SerializeJson(new XLDaneWydruku { IdZrodla = 0, IdWydruku = 237, IdFormatu = 1 }) };
            //    //var result = Helpers.CreateTaskAndWaitForResult(Plugin.ERPXL.Common.PluginDataInfo.PluginGuid, req);
            //    //Console.WriteLine("PDF: " + Helpers.SerializeJson(result.Obj).Length);
            //}

            // tworzenie zadan wysylki

//            var mailJob = new MailJobRequest();
//            mailJob.MessageSubject = "Wiadomość testowa";
//            mailJob.MessageBody = @"
//            <html>
//                <body>
//                    Witaj <b>Test</b>,<br /><br />
//                    Oto wiadomość testowa.<br /><br />
//                    <i>--<br />
//                    pozdrawiamy,<br /><br />
//                    Redakcja</i>
//                </body>
//            </html>";
//            mailJob.ObjTo.Add(new MailObj { EmailAddress = "tomasz.zielinski@netrix.com.pl", Name = "Tomasz Zieliński" });

//            var taskIds = new List<long>();

//            for (int i = 0; i < 2; i++)
//            {
//                taskIds.Add(Helpers.CreateTask(Plugin.Mailer.PluginDataInfo.PluginGuid, "StartJob", Helpers.SerializeJson(mailJob), "", connStr));
//            }

//            // sprawdzanie wykonania zadan

//            foreach (var t in taskIds)
//            {
//                var result = Helpers.GetTaskResult(t, connStr: connStr);
//                if (result.TaskResultCode == TaskResultCode.FinishedOk)
//                {
//                    Console.WriteLine("Pomyślnie wysłano wiadomość [" + t + "].");
//                }
//            }

            var obj = new XLAtrybut { GIDTyp = 16, GIDNumer = 6, Klasa = "Towar specjalny - N3", Wartosc = "TAK" };
            var ret = Helpers.CreateTaskAndWaitForResult(Plugin.ERPXL.Common.PluginDataInfo.PluginGuid, "AtrybutDodaj", Helpers.SerializeJson(obj), "", connStr: connStr);
            if (ret.TaskResultCode == TaskResultCode.FinishedOk)
            {
                Console.WriteLine("Pomyślnie dodano atrybut [Towar specjalny - N3] = [TAK].");
            }
            else
            {
                Console.WriteLine("Dodawanie atrybutu [Towar specjalny - N3] = [TAK]. " + ret.Msg);
            }
            Console.WriteLine();

            obj = new XLAtrybut { GIDTyp = 16, GIDNumer = 6, Klasa = "Towar specjalny - N3", Wartosc = "NIE" };
            ret = Helpers.CreateTaskAndWaitForResult(Plugin.ERPXL.Common.PluginDataInfo.PluginGuid, "AtrybutModyfikuj", Helpers.SerializeJson(obj), "", connStr: connStr);
            if (ret.TaskResultCode == TaskResultCode.FinishedOk)
            {
                Console.WriteLine("Pomyślnie zmodyfikowano atrybut [Towar specjalny - N3] = [NIE].");
            }
            else
            {
                Console.WriteLine("Modyfikacja atrybutu [Towar specjalny - N3] = [NIE]. " + ret.Msg);
            }
            Console.WriteLine();

            obj = new XLAtrybut { GIDTyp = 16, GIDNumer = 6, Klasa = "Towar specjalny - N3" };
            ret = Helpers.CreateTaskAndWaitForResult(Plugin.ERPXL.Common.PluginDataInfo.PluginGuid, "AtrybutPobierzWartosc", Helpers.SerializeJson(obj), "", connStr: connStr);
            if (ret.TaskResultCode == TaskResultCode.FinishedOk)
            {
                Console.WriteLine("Wartość atrybutu [Towar specjalny - N3] = [{0}].", ret.Obj);
            }
            else
            {
                Console.WriteLine("Wczytywanie wartości atrybutu [Towar specjalny - N3]. " + ret.Msg);
            }
            Console.WriteLine();

            obj = new XLAtrybut { GIDTyp = 16, GIDNumer = 5311 };
            ret = Helpers.CreateTaskAndWaitForResult(Plugin.ERPXL.Common.PluginDataInfo.PluginGuid, "AtrybutWczytajListe", Helpers.SerializeJson(obj), "", connStr: connStr);
            if (ret.TaskResultCode == TaskResultCode.FinishedOk)
            {
                Console.WriteLine("Atrybuty towaru [Twr_GIDNumer = 5311]:");
                var atrs = Helpers.DeserializeJson<List<XLAtrybut>>(ret.Obj.ToString());
                if (atrs.Any())
                {
                    foreach (var el in atrs)
                    {
                        Console.WriteLine("[{0}] = [{1}]", el.Klasa, el.Wartosc);
                    }
                }
                else
                {
                    Console.WriteLine("brak.");
                }
            }
            else
            {
                Console.WriteLine("Wczytywanie atrybutów towaru [Twr_GIDNumer = 5311]. " + ret.Msg);
            }
            Console.WriteLine();

            Console.WriteLine("* naciśnij dowolny klawisz, aby zakończyć...");
            Console.ReadKey();
        }

        //public static string NarzedziaPobierzNumerDok(int gidTyp, int gidNumer, string connStr = null)
        //{
        //    using (var conn = new SqlConnection(connStr))
        //    {
        //        var cmd = new SqlCommand(@"SELECT CDN.NazwaObiektu(@GIDTyp, @GIDNumer, 0, 2)", conn);

        //        cmd.Parameters.AddWithValue("@GIDTyp", gidTyp);
        //        cmd.Parameters.AddWithValue("@GIDNumer", gidNumer);

        //        conn.Open();

        //        using (var reader = cmd.ExecuteReader())
        //        {
        //            reader.Read();
        //            return reader.GetString(0);
        //        }
        //    }
        //}

    }
}