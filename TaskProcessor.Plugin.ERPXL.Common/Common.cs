﻿using System;
using System.Collections.Generic;

namespace TaskProcessor.Plugin.ERPXL.Common
{
    public static class PluginDataInfo
    {
        public static readonly Guid PluginGuid = new Guid("E0000000-6897-4923-AE25-F4701FCEA10B");
        public const String PluginName = "ERP XL";
    }

    public class XLRezerwacja
    {
        public XLRezerwacja()
        {
            Towar = new XLTowar();
            Kontrahent = new XLKontrahent();
            Zasob = new XLZasobRezerwacji();
        }
        public int? GIDTyp { get; set; }
        public int? GIDFirma { get; set; }
        public int? GIDNumer { get; set; }
        public int? GIDLp { get; set; }
        public int? MagTyp { get; set; }
        public int? MagFirma { get; set; }
        public int? MagNumer { get; set; }
        public int? MagLp { get; set; }
        public int? FRSId { get; set; }
        public int? Priorytet { get; set; }
        public int? Rez_ZrdTyp { get; set; }
        public int? Rez_ZrdFirma { get; set; }
        public int? Rez_ZrdNumer { get; set; }
        public int? Rez_ZrdLp { get; set; }
        public int? Rez_OpeTyp { get; set; }
        public int? Rez_OpeFirma { get; set; }
        public int? Rez_OpeNumer { get; set; }
        public int? Rez_OpeLp { get; set; }
        public decimal? Rez_Zrealizowano { get; set; }
        public decimal? Rez_IloscMag { get; set; }
        public decimal? Rez_IloscImp { get; set; }
        public decimal? Rez_IloscSSC { get; set; }
        public decimal? Rez_IloscSAD { get; set; }
        public DateTime? Rez_TStamp { get; set; }
        public bool? Rez_Aktywna { get; set; }
        public XLPochodzenieRezerwacji? Rez_ZrodloRezerwacji { get; set; }
        public string KntAkronim { get; set; }
        public string Magazyn { get; set; }
        public decimal? Ilosc { get; set; }
        public DateTime? DataRealizacji { get; set; }
        public DateTime? DataWaznosci { get; set; }
        public XLTowar Towar { get; set; }
        public XLKontrahent Kontrahent { get; set; }
        public XLZasobRezerwacji Zasob { get; set; }
    }

    public enum XLPochodzenieRezerwacji
    {
        Reczna = 10,
        ZZamZewnetrznego = 9,
        ZZamWewnetrznego = 5,
    }

    public class XLZasobRezerwacji
    {
        public int? GIDTyp { get; set; }
        public int? GIDFirma { get; set; }
        public int? GIDNumer { get; set; }
        public int? GIDLp { get; set; }
        public int? DstTyp { get; set; }
        public int? DstFirma { get; set; }
        public int? DstNumer { get; set; }
        public int? DstLp { get; set; }
        public int? MagNumer { get; set; }
    }

    //public class XLPracownik
    //{
    //    public int? GIDTyp { get; set; }
    //    public int? GIDFirma { get; set; }
    //    public int? GIDNumer { get; set; }
    //    public int? GIDLp { get; set; }
    //    public int? BnkTyp { get; set; }
    //    public int? BnkFirma { get; set; }
    //    public int? BnkNumer { get; set; }
    //    public int? BnkLp { get; set; }
    //}

    //public class XLOperator
    //{
    //    public int? GIDTyp { get; set; }
    //    public int? GIDFirma { get; set; }
    //    public int? GIDNumer { get; set; }
    //    public int? GIDLp { get; set; }
    //}

    //public class XLGIDIdent
    //{
    //    int? GIDTyp { get; set; }
    //    int? GIDFirma { get; set; }
    //    int? GIDNumer { get; set; }
    //    int? GIDLp { get; set; }
    //    string Akronim { get; set; }
    //}

    public class XLZalacznik
    {
        public int? Id { get; set; }
        public int? ObiTyp { get; set; }
        public int? ObiNumer { get; set; }
        public int? ObiLp { get; set; }
        public string NazwaPliku { get; set; }
        public string RozszerzeniePliku { get; set; }
        public string Kod { get; set; }
        public byte[] Dane { get; set; }
        public int? TypDanych { get; set; }
        public DateTime? CzasModyfikacji { get; set; }
        public DateTime? CzasDodania { get; set; }
        public bool? DostepnoscWPulpicieKnt { get; set; }
    }

    public class XLKontrahent
    {
        public XLKontrahent()
        {
            Adresy = new List<XLKontrahentAdres>();
        }
        public int? GIDTyp { get; set; }
        public int? ApiTyp { get; set; }
        public int? GIDNumer { get; set; }
        public int? GIDFirma { get; set; }
        public int? GIDLp { get; set; }
        public int? NumerOsoby { get; set; }
        public string KntAkronim { get; set; }
        public string KntNazwa { get; set; }
        public int? GRPTyp { get; set; }
        public int? GRTFirma { get; set; }
        public int? GRPNumer { get; set; }
        public int? GRPLp { get; set; }
        public bool? Akwizytor { get; set; }
        //public XLPracownik Opiekun { get; set; }
        public bool? FlagaNRB { get; set; }
        public int? RegionCRM { get; set; }
        public bool? Spedytor { get; set; }
        public int? Cena { get; set; }
        //public XLOperator OperatorZakladajacy { get; set; }
        public int? FormaPl { get; set; }
        public bool? PlatnikVat { get; set; }
        public int? ExpoKraj { get; set; }
        public bool? AutoPotwierdzanie { get; set; }
        public int? MagTyp { get; set; }
        public int? MagFirma { get; set; }
        public int? MagNumer { get; set; }
        public int? MagLp { get; set; }
        public int? KarTyp { get; set; }
        public int? KarFirma { get; set; }
        public int? KarNumer { get; set; }
        public int? KarLp { get; set; }
        public int? Knt_KnATyp { get; set; }
        public int? Knt_KnAFirma { get; set; }
        public int? Knt_KnANumer { get; set; }
        public int? Knt_KnALp { get; set; }
        public DateTime? TerminPlatnosciDlaSprzedazy { get; set; }
        public DateTime? TerminPlatnosciZakup { get; set; }
        public int? FormaPlatnosci { get; set; }
        public DateTime? SpodziewanyTerminPlatnoscSprzedaz { get; set; }
        public DateTime? SpodziewanyTerminPlatnosciZakup { get; set; }
        public XLKntLimitTerminowy? LimitTerminowy { get; set; }
        public DateTime? DataWygasnieciaLimituKredytowego { get; set; }
        public int? MaxDniPoTerminie { get; set; }
        public int? Koncesja { get; set; }
        public DateTime? DataKoncesji { get; set; }
        public int? Dzialalnosc { get; set; }
        public int? AkwizytorGIDTyp { get; set; }
        public int? AkwizytorGIDFirma { get; set; }
        public int? AkwizytorGIDNumer { get; set; }
        public int? AkwizytorGIDLp { get; set; }
        public XLKntRozliczeniaDewizowe? Dewizowe { get; set; }
        public bool? BlokadaTransakcji { get; set; }
        public int? RolaPartnera { get; set; }
        public int? Zrodlo { get; set; }
        public int? Branza { get; set; }
        public int? Rodzaj { get; set; }
        public bool? AdresNieaktualny { get; set; }
        public DateTime? DataWaznosciKarty { get; set; }
        public int? TypKarty { get; set; }
        public int? Priorytet { get; set; }
        public bool? NieSprawdzajPodobnychKnt { get; set; }
        public int? PriorytetRezerwacji { get; set; }
        public DateTime? TerminRozliczeniaKaucji { get; set; }
        public int? PlatnoscKaucji { get; set; }
        //public XLGIDIdent Platnik { get; set; }
        public int? CzyFormaITerminZKntCzyKnp { get; set; }
        public int? Powiazany { get; set; }
        public int? KnGGIDTyp { get; set; }
        public int? KnGGIDNumer { get; set; }
        public int? OpiekunCzasowy { get; set; }
        public DateTime? OpiekunDataOd { get; set; }
        public DateTime? OpiekunDataDo { get; set; }
        public int? OpeNumerW { get; set; }
        public int? OpeNumerO { get; set; }
        public int? TwrGrupaNumer { get; set; }
        public DateTime? DataWydaniaDokTozsamosci { get; set; }
        public bool? RolnikRyczaltowy { get; set; }
        public bool? Esklep { get; set; }
        public string Nazwa1 { get; set; }
        public string Nazwa2 { get; set; }
        public string Nazwa3 { get; set; }
        public string KodP { get; set; }
        public string Miasto { get; set; }
        public string Ulica { get; set; }
        public string Adres { get; set; }
        public string NipPrefiks { get; set; }
        public string NipE { get; set; }
        public string Regon { get; set; }
        public string Pesel { get; set; }
        public string Bank { get; set; }
        public string NrRachunku { get; set; }
        public decimal? Odleglosc { get; set; }
        public string Kraj { get; set; }
        public string Wojewodztwo { get; set; }
        public string KontoDostawcy { get; set; }
        public string KontoOdbiorcy { get; set; }
        public int? Knt_LimitOkres { get; set; }
        public int? Knt_NrKursu { get; set; }
        public string Powiat { get; set; }
        public string Gmina { get; set; }
        public string Telefon1 { get; set; }
        public string Telefon2 { get; set; }
        public string Fax { get; set; }
        public string Modem { get; set; }
        public string Telex { get; set; }
        public string Email { get; set; }
        public string URL { get; set; }
        public int? CenaDomyslna { get; set; }
        public string SeriaFaktur { get; set; }
        public string AkronimOpiekuna { get; set; }
        public string MagKod { get; set; }
        public string OutlookUrl { get; set; }
        public string Seria { get; set; }
        public DateTime? DataWygasnieciaStr { get; set; }
        public DateTime? DataKoncesjiStr { get; set; }
        public string SposobDostawy { get; set; }
        public string AkronimAkwizytora { get; set; }
        public string AkronimPlatnika { get; set; }
        public string SymbolWaluty { get; set; }
        public decimal? AkwProwizja { get; set; }
        public string RegionCRMStr { get; set; }
        public string GLN { get; set; }
        public decimal? Rabat { get; set; }
        public decimal? MaxLimitWartosc { get; set; }
        public decimal? LimitPoTerminie { get; set; }
        public string CechaOpis { get; set; }
        public decimal? Marza { get; set; }
        public string RolaPartneraStr { get; set; }
        public string ZrodloStr { get; set; }
        public string BranzaStr { get; set; }
        public string RodzajStr { get; set; }
        public string GrupaSciezka { get; set; }
        public string NumerKarty { get; set; }
        public string DataKartyStr { get; set; }
        public string TypKartyStr { get; set; }
        public string Pin { get; set; }
        public string PriorytetStr { get; set; }
        public string AkronimKntGlownego { get; set; }
        public string Opis { get; set; }
        public string OpiekunCzasStr { get; set; }
        public string FPPKod { get; set; }
        public string SeriaKaP { get; set; }
        public string OpeHaslo { get; set; }
        public string OrganWydajacy { get; set; }
        public string DokumentTozsamosci { get; set; }
        public int? TrybZamkniecia { get; set; }
        public List<XLKontrahentAdres> Adresy { get; set; }
        public int? Knt_BnkTyp { get; set; }
        public int? Knt_BnkFirma { get; set; }
        public int? Knt_BnkNumer { get; set; }
        public int? Knt_BnkLp { get; set; }
        public string Knt_Soundex { get; set; }
    }

    public class XLKontrahentAdres
    {
        public int? GIDTyp { get; set; }
        public int? GIDFirma { get; set; }
        public int? GIDNumer { get; set; }
        public int? GIDLp { get; set; }
        public int? Typ { get; set; }
        public int? Wysylkowy { get; set; }
        public int? Tryb { get; set; }
        public int? Flagi { get; set; }
        public int? KntTyp { get; set; }
        public int? KntFirma { get; set; }
        public int? KntNumer { get; set; }
        public int? KntLp { get; set; }
        public int? NRB { get; set; }
        public int? RegionCRM { get; set; }
        public int? NiePublikuj { get; set; }
        public string Akronim { get; set; }
        public string Nazwa1 { get; set; }
        public string Nazwa2 { get; set; }
        public string Nazwa3 { get; set; }
        public string KodP { get; set; }
        public string Miasto { get; set; }
        public string Ulica { get; set; }
        public string Adres { get; set; }
        public string NipPrefiks { get; set; }
        public string NipE { get; set; }
        public string Regon { get; set; }
        public string Pesel { get; set; }
        public string Bank { get; set; }
        public string NrRachunku { get; set; }
        public string Odleglosc { get; set; }
        public string Kraj { get; set; }
        public string Wojewodztwo { get; set; }
        public string Powiat { get; set; }
        public string Gmina { get; set; }
        public string Telefon1 { get; set; }
        public string Telefon2 { get; set; }
        public string Fax { get; set; }
        public string Modem { get; set; }
        public string Telex { get; set; }
        public string EMail { get; set; }
        public string KontoDostawcy { get; set; }
        public string KontoOdbiorcy { get; set; }
    }

    public class XLTowar
    {
        public XLTowar()
        {
            Cecha = new XLCecha();
            Ceny = new List<XLCena>();
        }
        public int? GIDTyp { get; set; }
        public int? ApiTyp { get; set; }
        public int? GIDNumer { get; set; }
        public int? GIDFirma { get; set; }
        public int? GIDLp { get; set; }
        public string TwrKod { get; set; }
        public string TwrNazwa { get; set; }
        public string TwrEAN { get; set; }
        public int? CenaSpr { get; set; }
        public int? TwGTyp { get; set; }
        public int? TwGNumer { get; set; }
        public int? TwGLp { get; set; }
        public int? Flagi { get; set; }
        public int? KgoId { get; set; }
        public int? Koncesja { get; set; }
        public int? IdSchematuKaucji { get; set; }
        public int? JmCalkowita { get; set; }
        public decimal? Clo { get; set; }
        public decimal? StawkaPodZak { get; set; }
        public decimal? StawkaPodSpr { get; set; }
        public decimal? Akcyza { get; set; }
        public decimal? MarzaMin { get; set; }
        public decimal? KosztUslugi { get; set; }
        public string TwrSWW { get; set; }
        public string Jm { get; set; }
        public string GrupaPodZak { get; set; }
        public string GrupaPodSpr { get; set; }
        public string Symbol { get; set; }
        public string Magazyn { get; set; }
        public string TwrGrupa { get; set; }
        public string Opis { get; set; }
        public string KgoKod { get; set; }
        public string ZrodlowaZak { get; set; }
        public string ZrodlowaSpr { get; set; }
        public string FppKod { get; set; }
        public string SchematKaucji { get; set; }
        public XLTrybZamknieciaTowaru? TrybZamkniecia { get; set; }
        public XLTwrZlom? Zlom { get; set; }
        public XLTwrKaucja? Kaucja { get; set; }
        public XLFlagaVAT? FlagaVATSpr { get; set; }
        public XLFlagaVAT? FlagaVATZak { get; set; }
        public XLTwrRozliczMagazyn? RozliczMag { get; set; }
        public XLTwrKosztUslugiTyp? KosztUTyp { get; set; }
        public XLCecha Cecha { get; set; }
        public List<XLCena> Ceny { get; set; }
    }

    public enum XLKntLimitTerminowy
    {
        Nieograniczony = 0,
        Ograniczony = 1,
        ZWzorca = -1,
    }

    public enum XLKntRozliczeniaDewizowe
    {
        Nie = 0,
        Tak = 1,
        NaPodstawieWzorcaGrupy = 2,
    }

    public enum XLTwrKosztUslugiTyp
    {
        Brak = 0,
        Procentowy = 1,
        Wartosciowy = 2,
    }

    public enum XLTrybZamknieciaTowaru
    {
        Skasuj = -1,
        Zamknij = 0,
    }

    public enum XLTwrRozliczMagazyn
    {
        FIFO = 1,
        LIFO = 2,
        Wybor = 3,
        WgDatyWaznosci = 4,
    }

    public enum XLTwrZlom
    {
        PobierzZeWzorca = -1,
        Nie = 0,
        Tak = 1,
    }

    public enum XLTwrKaucja
    {
        PobierzZeWzorca = -1,
        Nie = 0,
        Tak = 1,
    }

    public class XLCena
    {

        public int? GIDTyp { get; set; }
        public int? GIDFirma { get; set; }
        public int? GIDNumer { get; set; }
        public int? GIDLp { get; set; }
        public int? Numer { get; set; }
        public int? NrKursu { get; set; }
        public bool? Aktualizacja { get; set; }
        public string Kod { get; set; }
        public string Waluta { get; set; }
        public decimal? Wartosc { get; set; }
        public decimal? Marza { get; set; }
        public string Zaok { get; set; }
        public string Offset { get; set; }
    }

    public class XLCecha
    {
        public int? GIDTyp { get; set; }
        public int? GIDNumer { get; set; }
        public int? GIDFirma { get; set; }
        public int? GIDLp { get; set; }
        public string KlasaCechy { get; set; }
        public bool? CechaZakup { get; set; }
        public bool? CechaSprzedaz { get; set; }
    }

    public class XLDokZamowienieNag
    {
        public XLDokZamowienieNag()
        {
            KontrahentGlowny = new XLKontrahent();
            KontrahentDocelowy = new XLKontrahent();
            KontrahentAkwizytor = new XLKontrahent();
            Elementy = new List<XLDokZamowienieElem>();
            Zalaczniki = new List<XLZalacznik>();
            Atrybuty = new List<XLAtrybut>();
            Platnosci = new List<XLDokZamowieniePlatnosc>();
            WspolnaWaluta = true;
            TrybPracy = XLTrybPracy.Wsadowy;
            TrybZamkniecia = XLTrybZamknieciaZam.BezZmian;
        }
        public int? GIDTyp { get; set; }
        public int? ApiTyp { get; set; }
        public int? GIDNumer { get; set; }
        public int? GIDFirma { get; set; }
        public int? GIDLp { get; set; }
        public int? ZamTyp { get; set; }
        public int? Numer { get; set; }
        public string Seria { get; set; }
        public int? Rok { get; set; }
        public int? Miesiac { get; set; }
        public int? RokMiesiac { get; set; }
        public int? MagTyp { get; set; }
        public int? MagFirma { get; set; }
        public int? MagNumer { get; set; }
        public int? MagLp { get; set; }
        public string MagazynW { get; set; }
        public int? FormaPlatnosci { get; set; }
        public string FlagaNB { get; set; }
        public string Waluta { get; set; }
        public int? NrKursu { get; set; }
        public int? TypKursu { get; set; }
        public decimal? KursL { get; set; }
        public int? KursM { get; set; }
        public int? AdrTyp { get; set; }
        public int? AdrNumer { get; set; }
        public int? AdwTyp { get; set; }
        public int? AdwNumer { get; set; }
        public int? AdPNumer { get; set; }
        public DateTime? DataWystawienia { get; set; }
        public DateTime? DataRealizacji { get; set; }
        public DateTime? DataWaznosci { get; set; }
        public DateTime? DataAktywacjiRez { get; set; }
        public XLRodzajZamowienia? RodzajZamowienia { get; set; }
        public int? RealWCalosci { get; set; }
        public int? ExpoNorm { get; set; }
        public int? ZrdTyp { get; set; }
        public int? ZrdFirma { get; set; }
        public int? ZrdNumer { get; set; }
        public XLKontrahent KontrahentGlowny { get; set; }
        public XLKontrahent KontrahentDocelowy { get; set; }
        public XLKontrahent KontrahentAkwizytor { get; set; }
        public int? AkwizytorKntPrc { get; set; }
        public string Opis { get; set; }
        public string Url { get; set; }
        public string DokumentObcy { get; set; }
        public XLStanOferty? PotwierdzenieOferty { get; set; }
        public int? Stan { get; set; }
        public bool? WspolnaWaluta { get; set; }
        public int? TerminPlatnosci { get; set; }
        public XLRezerwacjeNaNiepotwierdzonym? RezerwacjeNaNiepotwierdzonym { get; set; }
        public List<XLDokZamowienieElem> Elementy { get; set; }
        public List<XLZalacznik> Zalaczniki { get; set; }
        public List<XLAtrybut> Atrybuty { get; set; }
        public List<XLDokZamowieniePlatnosc> Platnosci { get; set; }
        public XLTrybPracy? TrybPracy { get; set; }
        public XLTrybZamknieciaZam? TrybZamkniecia { get; set; }
    }

    public class XLDokZamowienieElem
    {
        public XLDokZamowienieElem()
        {
            Towar = new XLTowar();
        }
        public int? GIDTyp { get; set; }
        public int? GIDFirma { get; set; }
        public int? GIDNumer { get; set; }
        public int? GIDLp { get; set; }
        public int? ZrdTyp { get; set; }
        public int? ZrdFirma { get; set; }
        public int? ZrdNumer { get; set; }
        public int? ZrdLp { get; set; }
        public DateTime? DataAktywacjiRezerwacji { get; set; }
        public DateTime? DataWaznosciRezerwacji { get; set; }
        public XLFlagaVAT? FlagaVAT { get; set; }
        public string Waluta { get; set; }
        public XLRownanie? Rownanie { get; set; }
        public XLTowar Towar { get; set; }
        public string Nazwa { get; set; }
        public string Opis { get; set; }
        public decimal? Ilosc { get; set; }
        public decimal? Wartosc { get; set; }
        public int? ModRabat { get; set; }
        public decimal? ModCenaP { get; set; }
        public decimal? ModCena { get; set; }
        public string ModVAT { get; set; }
        public decimal? ModStawkaVAT { get; set; }
    }

    public class XLDokZamowieniePlatnosc
    {
        public int? TerminOffset { get; set; }
        public DateTime? TerminData { get; set; }
        public int? FormaPlatnosci { get; set; }
        public decimal? KwotaProcent { get; set; }
        public decimal? KwotaWartosc { get; set; }
        public bool? CzyZaliczka { get; set; }
        public string Waluta { get; set; }
        public string Opis { get; set; }
    }

    public class XLDokHandlowyNag
    {
        public XLDokHandlowyNag()
        {
            KontrahentGlowny = new XLKontrahent();
            KontrahentDocelowy = new XLKontrahent();
            KontrahentPlatnik = new XLKontrahent();
            KontrahentAkwizytor = new XLKontrahent();
            Elementy = new List<XLDokHandlowyElem>();
            Zalaczniki = new List<XLZalacznik>();
            Atrybuty = new List<XLAtrybut>();
            Platnosci = new List<XLDokHandlowyPlatnosc>();
            DokumentyZwiazane = new List<XLObiektOgolny>();
            TrybPracy = XLTrybPracy.Wsadowy;
            IgnorujZaliczkowe = XLDokHanIgnorujZaliczkowe.NiePokazujOknaUwzglednijZaliczki;
            IgnorujMagazynowe = XLDokHanIgnorujMagazynowe.NiePokazujOknaUwzglednijMag;
            TrybZamkniecia = XLTrybZamknieciaDokHan.ZamkniecieOtwartego;
            Magazynowe = XLTrybZamknieciaDokHanMagazynowe.WgKonfiguracji;
        }
        public int? GIDTyp { get; set; }
        public int? ApiTyp { get; set; }
        public int? GIDNumer { get; set; }
        public int? GIDFirma { get; set; }
        public int? GIDLp { get; set; }
        public int? Numer { get; set; }
        public string Seria { get; set; }
        public int? Rok { get; set; }
        public int? Miesiac { get; set; }
        public int? RokMiesiac { get; set; }
        public DateTime? DataDokumentu { get; set; }
        public DateTime? DataSprzedazyZakupu { get; set; }
        public int? TerminPlatnosci { get; set; }
        public int? FormaPlatnosci { get; set; }
        public int? ZamTyp { get; set; }
        public int? ZamFirma { get; set; }
        public int? ZamNumer { get; set; }
        public int? ZamLp { get; set; }
        public int? ExpoNorm { get; set; }
        public bool? CzyFiskalny { get; set; }
        public bool? CzyUwzgledniacZwiazane { get; set; }
        public XLDokHanIgnorujZaliczkowe? IgnorujZaliczkowe { get; set; }
        public XLDokHanIgnorujMagazynowe? IgnorujMagazynowe { get; set; }
        public XLKontrahent KontrahentGlowny { get; set; }
        public XLKontrahent KontrahentDocelowy { get; set; }
        public XLKontrahent KontrahentPlatnik { get; set; }
        public XLKontrahent KontrahentAkwizytor { get; set; }
        public int? AkwizytorKntPrc { get; set; }
        public string FlagaNB { get; set; }
        public string Waluta { get; set; }
        public string MagazynZ { get; set; }
        public string MagazynD { get; set; }
        public string Opis { get; set; }
        public string Url { get; set; }
        public string DokumentObcy { get; set; }
        public int? AdrTyp { get; set; }
        public int? AdrNumer { get; set; }
        public int? AdPNumer { get; set; }
        public int? AdwTyp { get; set; }
        public int? AdwNumer { get; set; }
        public int? Stan { get; set; }
        public List<XLDokHandlowyElem> Elementy { get; set; }
        public List<XLZalacznik> Zalaczniki { get; set; }
        public List<XLAtrybut> Atrybuty { get; set; }
        public List<XLDokHandlowyPlatnosc> Platnosci { get; set; }
        public List<XLObiektOgolny> DokumentyZwiazane { get; set; }
        public XLTrybPracy? TrybPracy { get; set; }
        public XLTrybZamknieciaDokHanMagazynowe? Magazynowe { get; set; }
        public XLTrybZamknieciaDokHan? TrybZamkniecia { get; set; }
    }

    public class XLDokHandlowyElem
    {
        public XLDokHandlowyElem()
        {
            Towar = new XLTowar();
            CzySprzedawacMimoRezerwacji = false;
            CzyWartoscOstateczna = false;
        }
        public int? GIDTyp { get; set; }
        public int? GIDFirma { get; set; }
        public int? GIDNumer { get; set; }
        public int? GIDLp { get; set; }
        public int? ZamTyp { get; set; }
        public int? ZamFirma { get; set; }
        public int? ZamNumer { get; set; }
        public int? ZamLp { get; set; }
        public XLTowar Towar { get; set; }
        public string Nazwa { get; set; }
        public string Opis { get; set; }
        public string JmZ { get; set; }
        public bool? CzyPobieracTowarPrzezGID { get; set; }
        public bool? CzySprzedawacMimoRezerwacji { get; set; }
        public bool? CzyPodanaIloscJestWymagana { get; set; }
        public decimal? Ilosc { get; set; }
        public XLFlagaVAT? FlagaVAT { get; set; }
        public string Waluta { get; set; }
        public decimal? Wartosc { get; set; }
        public bool? CzyWartoscOstateczna { get; set; }
        public XLRownanie? Rownanie { get; set; }
    }

    public class XLDokHandlowyPlatnosc
    {
        public int? TerminOffset { get; set; }
        public int? FormaPlatnosci { get; set; }
        public decimal? KwotaWartosc { get; set; }
        public string Waluta { get; set; }
        public string Opis { get; set; }
    }

    public class XLDokZlecenieProdNag
    {
        public XLDokZlecenieProdNag()
        {
            KontrahentGlowny = new XLKontrahent();
            KontrahentDocelowy = new XLKontrahent();
            Elementy = new List<XLDokZlecenieProdElem>();
            Zalaczniki = new List<XLZalacznik>();
            TrybPracy = XLTrybPracy.Wsadowy;
            TrybZamkniecia = XLTrybZamknieciaZlecProd.BezZmian;
        }
        public int? ApiTyp { get; set; }
        public int? GIDNumer { get; set; }
        public int? Numer { get; set; }
        public int? Rok { get; set; }
        public int? Miesiac { get; set; }
        public int? Oddzial { get; set; }
        public int? Projekt { get; set; }
        public int? OpWNumer { get; set; }
        public int? PriorytetZlc { get; set; }
        public int? PriorytetRez { get; set; }
        public int? Flagi { get; set; }
        public string OpWIdent { get; set; }
        public string Opis { get; set; }
        public string DokumentObcy { get; set; }
        public string Seria { get; set; }
        public XLDokZlecenieProdPlanowanieOd? DokZlecenieProdPlanowanieOd { get; set; }
        public DateTime? PlanowacOdData { get; set; }
        public DateTime? DataWystawienia { get; set; }
        public bool? RezerwujZasoby { get; set; }
        public XLKontrahent KontrahentGlowny { get; set; }
        public XLKontrahent KontrahentDocelowy { get; set; }
        public List<XLDokZlecenieProdElem> Elementy { get; set; }
        public List<XLZalacznik> Zalaczniki { get; set; }
        public XLTrybPracy? TrybPracy { get; set; }
        public XLTrybZamknieciaZlecProd? TrybZamkniecia { get; set; }

        public int? PZL_Frs_Id { get; set; }
        public DateTime? PZL_DataZamkniecia { get; set; }
        public int? PZL_OpZNumer { get; set; }
        public DateTime? PZL_CzasModyfikacji { get; set; }
        public int? PZL_Lp { get; set; }
        public int? PZL_Zaksiegowano { get; set; }
        public int? PZL_OpMNumer { get; set; }
        public int? PZL_SCHTyp { get; set; }
        public int? PZL_SCHNumer { get; set; }
        public int? PZL_WsSCHTyp { get; set; }
        public int? PZL_WsSCHNumer { get; set; }
        public int? PZL_WsStosujSchemat { get; set; }
        public int? PZL_WsStosujDziennik { get; set; }
        public decimal? PZL_Koszt { get; set; }
        public decimal? PZL_KosztSurowca { get; set; }
        public int? PZL_PrjID { get; set; }
        public int? PZL_KosztUstalony { get; set; }
        public int? PZL_FrmNumer { get; set; }
        public int? PZL_Stan { get; set; }
        public string PZL_Status { get; set; }
        public int? PZL_GenDokZatwierdzone { get; set; }
        public int? PZL_DokPrzyjeciaPoDokWydania { get; set; }
        public int? PZL_ZwolnioneDoProd { get; set; }
        public int? PZL_RealizacjaWgPlanu { get; set; }
    }

    public class XLDokZlecenieProdElem
    {
        public XLDokZlecenieProdElem()
        {
            Towar = new XLTowar();
            KontrahentGlowny = new XLKontrahent();
            KontrahentDocelowy = new XLKontrahent();
        }
        public int? GIDNumer { get; set; }
        public int? GIDLp { get; set; }
        public int? ZrdTyp { get; set; }
        public int? ZrdNumer { get; set; }
        public int? ZrdLp { get; set; }
        public int? Technologia { get; set; }
        public int? Oddzial { get; set; }
        public int? PriorytetRez { get; set; }
        public int? IdZlecenia { get; set; }
        public int? CChTyp { get; set; }
        public int? CChNumer { get; set; }
        public int? CChLp { get; set; }
        public int? Flagi { get; set; }
        public decimal? PZE_ZrownoleglacDo { get; set; }
        public decimal? PZE_IloscPlan { get; set; }
        public XLTowar Towar { get; set; }
        public string Cecha { get; set; }
        public string Opis { get; set; }
        public decimal? Ilosc { get; set; }
        public XLKontrahent KontrahentGlowny { get; set; }
        public XLKontrahent KontrahentDocelowy { get; set; }
        public DateTime? PlanowacOdData { get; set; }
        public XLDokZlecenieProdPlanowanieOd? DokZlecenieProdPlanowanieOd { get; set; }
        public XLZlecenieProdElemTyp? Typ { get; set; } 
    }

    public class XLProdukcjaTechnologia
    {
        public XLProdukcjaTechnologia()
        {
            Towar = new XLTowar();
            KontrahentGlowny = new XLKontrahent();
            KontrahentDocelowy = new XLKontrahent();
            Czynnosci = new List<XLProdukcjaCzynnoscTech>();
        }
        public int? Id { get; set; }
        public int? IdWersji { get; set; }
        public int? Projekt { get; set; }
        public int? Rok { get; set; }
        public int? Numer { get; set; }
        public int? Miesiac { get; set; }
        public int? Typ { get; set; }
        public string Kod { get; set; }
        public string Nazwa { get; set; }
        public string OpisWersji { get; set; }
        public string Jednostka { get; set; }
        public string Opis { get; set; }
        public string Seria { get; set; }
        public decimal? Ilosc { get; set; }
        public decimal? IloscMin { get; set; }
        public decimal? IloscProd { get; set; }
        public decimal? IloscPlan { get; set; }
        public DateTime? DataZatwierdzenia { get; set; }
        public DateTime? DataWystawienia { get; set; }
        public XLKontrahent KontrahentDocelowy { get; set; }
        public XLKontrahent KontrahentGlowny { get; set; }
        public XLTowar Towar { get; set; }
        public List<XLProdukcjaCzynnoscTech> Czynnosci { get; set; }
    }

    public class XLProdukcjaCzynnoscTech
    {
        public XLProdukcjaCzynnoscTech()
        {
            Towar = new XLTowar();
            Zasoby = new List<XLProdukcjaZasobCzynnoscTech>();
        }
        public int? Id { get; set; }
        public int? IdTechnologii { get; set; }
        public int? IdOjca { get; set; }
        public decimal? Ilosc { get; set; }
        public string Kod { get; set; }
        public string Nazwa { get; set; }
        public string Opis { get; set; }
        public string Jednostka { get; set; }
        public XLTowar Towar { get; set; }
        public List<XLProdukcjaZasobCzynnoscTech> Zasoby { get; set; } 
    }

    public class XLProdukcjaZasobCzynnoscTech
    {
        public XLProdukcjaZasobCzynnoscTech()
        {
            Towar = new XLTowar();
        }
        public int? Id { get; set; }
        public int? IdTechnologiaCzynnosc { get; set; }
        public int? TechnologiaZasob { get; set; }
        public int? ZrodloZasobu { get; set; }
        public XLTypZasobuProd? TypZasobu { get; set; }
        public int? Koszt { get; set; }
        public int? WagaIlosc { get; set; }
        public string Kod { get; set; }
        public string Nazwa { get; set; }
        public int? IloscFormat { get; set; }
        public decimal? Cena { get; set; }
        public decimal? Ilosc { get; set; }
        public string Cecha { get; set; }
        public string Jednostka { get; set; }
        public string Magazyn { get; set; }
        public XLTowar Towar { get; set; }
    }

    public class XLAtrybut
    {
        public int? GIDTyp { get; set; }
        public int? GIDFirma { get; set; }
        public int? GIDNumer { get; set; }
        public int? GIDLp { get; set; }
        public int? GIDSubLp { get; set; }
        public int? ZamTyp { get; set; }
        public XLRodzajZamowienia? ZamRodzaj { get; set; }
        public string Klasa { get; set; }
        public string Wartosc { get; set; }
    }

    public enum XLTrybZamknieciaDokHanMagazynowe
    {
        WgKonfiguracji = 0,
        NieGeneruj__NieGenerujWM_GenerujMMP = 1,
        GenerujDoBufora__NieGenerujWM_GenerujMMPDoBufora = 2,
        GenerujZatwierdzone__NieGenerujWM_GenerujMMPZatwierdzony = 3,
        GenerujWMDoBufora_NieGenerujMMP = 11,
        GenerujWMDoBufora_GenerujMMPDoBufora = 12, 
        GenerujWMDoBufora_GenerujMMPZatwierdzony = 13,
        GenerujWMZatwierdzony_NieGenerujMMP = 21, 
        GenerujWMZatwierdzony_GenerujMMPDoBufora = 22, 
        GenerujWMZatwierdzony_GenerujMMPZatwierdzony = 23,
    }

    public enum XLRodzajZamowienia
    {
        Wewnetrzne = 8,
        Zewnetrzne = 4,
    }

    public enum XLFlagaVAT
    {
        Zwolniony = 0,
        Podatek = 1,
        NiePodlega = 2,
    }

    public enum XLTrybPracy
    {
        Interakcyjny = 1,
        Wsadowy = 2,
        DomyslnyZSesji = 3,
    }

    public enum XLRownanie
    {
        Domyslnie = 0,
        NieTrzymajRownania = 1,
        TrzymajCenaRazyIlosc = 2,
    }

    public enum XLZlecenieProdElemTyp
    {
        ZlecenieProdukcyjne = 14343,
        ProcesProdukcyjny = 14344,
    }

    public enum XLRezerwacjeNaNiepotwierdzonym
    {
        ZDefinicjiDok = 0,
        Nie = 1,
        Tak = 2,
    }

    public enum XLDokZlecenieProdPlanowanieOd
    {
        Teraz = 0,
        NaZadanyTermin = 1,
        OdZadanegoTerminu = 2,
    }

    public enum XLDokHanIgnorujZaliczkowe
    {
        PokazujOknoZZaliczkami = 0,
        NiePokazujOknaIIgnorujZaliczki = 1,
        NiePokazujOknaUwzglednijZaliczki = 2,
    }

    public enum XLDokHanIgnorujMagazynowe
    {
        PokazujOknoZMag = 0,
        NiePokazujOknaIIgnorujMag = 1,
        NiePokazujOknaUwzglednijMag = 2,
    }

    public class XLSymbolWaluty
    {
        public const string PLN = "PLN";
        public const string USD = "USD";
        public const string EUR = "EUR";
    }

    public class XLApiTyp
    {
        public int? GIDTyp { get; set; }
        public int? PodTyp { get; set; }
        public int? ApiTyp { get; set; }
        public string NazwaMetody { get; set; }
    }

    public class XLObiektOgolny
    {
        public int? GIDTyp { get; set; }
        public int? PodTyp { get; set; }
        public int? GIDFirma { get; set; }
        public int? GIDNumer { get; set; }
        public int? GIDLp { get; set; }
        public int? GIDSubLp { get; set; }
    }

    public class XLDaneWydruku
    {
        public int? IdZrodla { get; set; }
        public int? IdWydruku { get; set; }
        public int? IdFormatu { get; set; }
        public string FiltrSql { get; set; }
    }

    public class XLDanePrzeliczRabat
    {
        public int? Typ { get; set; }
        public DateTime? Data { get; set; }
        public int? ExpoNorm { get; set; }
        public int? FormaPl { get; set; }
        public int? RabatFormyPlatnosci { get; set; }
        public int? RabatGlobalny { get; set; }
        public int? LiczOdWartosciKsiegowej { get; set; }
        public int? TypDopasowania { get; set; }
        public int? IloscCenaWartosc { get; set; }
        public int? Dokladnosc { get; set; }
        public int? WymusZmianeWartosci { get; set; }
        public int? NumerKursuWaluty { get; set; }
        public int? MianownikKursuWaluty { get; set; }
        public int? LicznikKursuWaluty { get; set; }
        public int? TypRabatuKontrahentaNaTowar { get; set; }
        public int? RabatKontrahentaNaTowar { get; set; }
        public int? TypProgu { get; set; }
        public int? WartoscProgowa { get; set; }
        public int? TypRabatuProgowego { get; set; }
        public int? RabatProgowy { get; set; }
        public int? Flagi { get; set; }
        public int? NumerCenyPoczatkowej { get; set; }
        public int? RabatEfektywny { get; set; }
        public int? GIDTyp { get; set; }
        public int? GIDFirma { get; set; }
        public int? GIDNumer { get; set; }
        public int? GIDLp { get; set; }
        public int? ZstTyp { get; set; }
        public int? ZstFirma { get; set; }
        public int? ZstNumer { get; set; }
        public int? ZstLp { get; set; }
        public string FlagaNB { get; set; }
        public decimal? Ilosc { get; set; }
        public decimal? CenaTowaru { get; set; }
        public decimal? CenaPoRabacie { get; set; }
        public decimal? WartoscPoRabacie { get; set; }
        public decimal? CenaKsiegowa { get; set; }
        public decimal? WartoscKsiegowa { get; set; }
        public string Akronim { get; set; }
        public string TowarKod { get; set; }
        public string WalutaTowaru { get; set; }        
    }

    public class XLFlagaNB
    {
        public const string Netto = "N";
        public const string Brutto = "B";
    }

    public enum XLStanOferty
    {
        Niepotwierdzona = 0,
        Potwierdzona = 1,
        Przyjeta = 2,
        Odrzucona = 3,
        Wyslana = 4,
        WygenerowanoZam = 17,
    }

    public enum XLTrybZamknieciaZam
    {
        BezZmian = 0,
        Usuniecie = 1,
        Potwierdzenie = 2,
        ZlozenieZW = 3,
        Anulowanie = 4,
        ZamknieciePotw = 5,
        Otworzenie = 6,
    }

    public enum XLTrybZamknieciaDokHan
    {
        ZatwierdzenieBezWydruku = -10,
        ZamkniecieOtwartego = -3,
        AnulowanieOtwartego = -2,
        Skasowanie = -1,
        Zatwierdzenie = 0,
        Bufor = 1,
        Drukowanie = 2,
        ZatwierdzenieIDrukowanie = 10,
    }
    
    public enum XLTrybZamknieciaZlecProd
    {
        BezZmian = 0,
        Usuniecie = 1,
        DodanieTylkoTrybInterakcyjny = 2,
        Planowanie = 3,
        PlanowanieCalosciowe = 4,
        Przeplanowywanie = 5,
        Rozliczanie = 6,
        Zamkniecie = 7,
        AktualizacjaRezerwacji = 16,
        PlanowanieZgrubne = 1024,
        PlanowanieUproszczone = 1280,
        PlanowanieSzczegolowe = 1536,
        PlanowanieWszystkieZasoby = 1738,
        PlanowanieCaloscioweZgrubne = 2048,
        PlanowanieCaloscioweUproszczone = 2304,
        PlanowanieCaloscioweSzczegolowe = 2560,
        PlanowanieCaloscioweWszystkieZasoby = 2816,
        PrzeplanowywanieZgrubne = 4096,
        PrzeplanowywanieUproszczone = 4352,
        PrzeplanowywanieSzczegolowe = 4608,
        PrzeplanowywanieWszystkieZasoby = 4864,
        PrzeplanowywanieKonfliktowZgrubne = 8192,
        PrzeplanowywanieKonfliktowUproszczone = 8448,
        PrzeplanowywanieKonfliktowSzczegolowe = 8704,
        PrzeplanowywanieKonfliktowWszystkieZasoby = 8960,
        DobierzZasobySzczegolowo = 16896,
        DobierzZasobyWszystkie = 17152,
    }

    public enum XLTypZasobuProd
    {
        Produkt = 0,
        Surowiec = 1,
        MaterialPomocniczy = 2,
    }

    public enum XLTrybZamknieciaAdresu
    {
        Usuniecie = -1,
        Zapisanie = 0,
    }

    public class XLInfoOWersji
    {
        public string Wersja { get; set; }
        public int Rok { get; set; }
        public int Wydanie { get; set; }
        public int Kompilacja { get; set; }
    }

    public enum XLKodLicencji
    {
        LicencjaStanowiskowa = 1,
        ModulAdministracja = 2,
        ModulSprzedaz = 3,
        ModulKsiegowosc = 4,
        ModulKompletacja = 5,
        ModulSrodkiTrwale = 6,
        ModulControlling = 7,
        ModulImport = 8,
        ModulCRM = 9,
        MenedzerBaz = 10,
        ModulZamowienia = 11,
        ModulSerwis = 12,
        AdministratorOddzialow = 13,
        ModulRemonty = 15,
        InternetowaKsiegaRaportow = 17,
        ModulProdukcja = 19,
        Projekty = 101,
        ECODWspolpracaZDostawcami = 301,
        ECODWspolpracaZOdbiorcami = 302,
        SprzedazDetaliczna = 303,
        ModelowanieProcesow = 304,
        ZarzadzaniePolozeniemWMagazynie = 305,
        Kolektory = 306,
        Ankiety = 406,
        Hydra = 501,
        HydraPlus = 502,
    }

    public enum XLStanKluczaHasp
    {
        NieaktywnaSesjaApi = -1,
        StanKluczaHasp = 0,
        BladKluczaSprzetowego = 1,
    }

    public class XLInfoOLicencjach
    {
        public XLInfoOLicencjach()
        {
            NieaktywneLicencje = new List<XLKodLicencji>();
        }
        public XLStanKluczaHasp StanKluczaHasp { get; set; }
        public List<XLKodLicencji> NieaktywneLicencje { get; set; }
    }

    public class XLDodawanieZalacznikowInfo
    {
        public XLDodawanieZalacznikowInfo()
        {
            Zalaczniki = new List<XLZalacznik>();
        }
        public XLObiektOgolny Obiekt { get; set; }
        public List<XLZalacznik> Zalaczniki { get; set; }
    }

    public static class XLApiTypList
    {
        public static XLApiTyp DokumentOfertaSprzedazy = new XLApiTyp { GIDTyp = 960, PodTyp = 768, ApiTyp = 4, NazwaMetody = "DokZamowienie" };
        public static XLApiTyp DokumentZamowienieSprzedazy = new XLApiTyp { GIDTyp = 960, PodTyp = 1280, ApiTyp = 6, NazwaMetody = "DokZamowienie" };

        public static XLApiTyp DokumentFakturaZakupu = new XLApiTyp { GIDTyp = 1521, ApiTyp = 1521, NazwaMetody = "DokHandlowy" };
        public static XLApiTyp DokumentPrzyjecieZewnetrzne = new XLApiTyp { GIDTyp = 1489, ApiTyp = 1489, NazwaMetody = "DokHandlowy" };
        public static XLApiTyp DokumentFakturaSprzedazy = new XLApiTyp { GIDTyp = 2033, ApiTyp = 2033, NazwaMetody = "DokHandlowy" };
        public static XLApiTyp DokumentWydanieZewnetrzne = new XLApiTyp { GIDTyp = 2001, ApiTyp = 2001, NazwaMetody = "DokHandlowy" };
        public static XLApiTyp DokumentRozchodWewnetrzny = new XLApiTyp { GIDTyp = 1616, ApiTyp = 1616, NazwaMetody = "DokHandlowy" };
        public static XLApiTyp DokumentPrzychodWewnetrzny = new XLApiTyp { GIDTyp = 1617, ApiTyp = 1617, NazwaMetody = "DokHandlowy" };
        public static XLApiTyp DokumentPrzesuniecieMiedzymagazynowe = new XLApiTyp { GIDTyp = 1603, ApiTyp = 1603, NazwaMetody = "DokHandlowy" };
        public static XLApiTyp DokumentParagon = new XLApiTyp { GIDTyp = 2034, ApiTyp = 2034, NazwaMetody = "DokHandlowy" };
        public static XLApiTyp DokumentSpinaczParagonow = new XLApiTyp { GIDTyp = 2035, ApiTyp = 2035, NazwaMetody = "DokHandlowy" };
        public static XLApiTyp DokumentPrzyjecieKaucji = new XLApiTyp { GIDTyp = 2002, ApiTyp = 2002, NazwaMetody = "DokHandlowy" };
        public static XLApiTyp DokumentWydanieKaucji = new XLApiTyp { GIDTyp = 2000, ApiTyp = 2000, NazwaMetody = "DokHandlowy" };
        public static XLApiTyp DokumentFakturaEksportowa = new XLApiTyp { GIDTyp = 2037, ApiTyp = 2037, NazwaMetody = "DokHandlowy" };
        public static XLApiTyp DokumentWydanieZewnetrzneEksportowe = new XLApiTyp { GIDTyp = 2005, ApiTyp = 2005, NazwaMetody = "DokHandlowy" };
        public static XLApiTyp DokumentFakturaWewnetrzna = new XLApiTyp { GIDTyp = 2036, ApiTyp = 2036, NazwaMetody = "DokHandlowy" };
        public static XLApiTyp DokumentRaportSprzedazy = new XLApiTyp { GIDTyp = 2039, ApiTyp = 2039, NazwaMetody = "DokHandlowy" };
        public static XLApiTyp DokumentFakturaVATRR = new XLApiTyp { GIDTyp = 1520, ApiTyp = 1520, NazwaMetody = "DokHandlowy" };
        public static XLApiTyp DokumentTaxFree = new XLApiTyp { GIDTyp = 1968, ApiTyp = 1968, NazwaMetody = "DokHandlowy" };
        public static XLApiTyp DokumentPrzyjecieMiedzymagazynowe = new XLApiTyp { GIDTyp = 1604, ApiTyp = 1604, NazwaMetody = "DokHandlowy" };
        public static XLApiTyp DokumentKorektaZbiorczaFakturySprzedazy = new XLApiTyp { GIDTyp = 2041, ApiTyp = 2041, NazwaMetody = "DokHandlowy" };
        public static XLApiTyp DokumentKorektaZbiorczaFakturyZakupu = new XLApiTyp { GIDTyp = 1529, ApiTyp = 1529, NazwaMetody = "DokHandlowy" };
        public static XLApiTyp DokumentKorektaZbiorczaFakturyEksportowej = new XLApiTyp { GIDTyp = 2045, ApiTyp = 2045, NazwaMetody = "DokHandlowy" };

        public static XLApiTyp ZlecenieProdukcyjne = new XLApiTyp { ApiTyp = 14343, NazwaMetody = "DokZlecenieProd" };
        public static XLApiTyp ProcesProdukcyjny = new XLApiTyp { ApiTyp = 14344, NazwaMetody = "DokZlecenieProd" };

        public static XLApiTyp KontrahentOdbiorca = new XLApiTyp { GIDTyp = 32, ApiTyp = 16 };
        public static XLApiTyp KontrahentDostawca = new XLApiTyp { GIDTyp = 32, ApiTyp = 8 };
        public static XLApiTyp KontrahentDostawcoOdbiorca = new XLApiTyp { GIDTyp = 32, ApiTyp = 24 };

        public static XLApiTyp TowarZwykly = new XLApiTyp { GIDTyp = 16, ApiTyp = 1 };
        public static XLApiTyp TowarProdukt = new XLApiTyp { GIDTyp = 16, ApiTyp = 2 };
        public static XLApiTyp TowarKoszt = new XLApiTyp { GIDTyp = 16, ApiTyp = 3 };
        public static XLApiTyp TowarUsluga = new XLApiTyp { GIDTyp = 16, ApiTyp = 4 };

        // dla funkcji UzupelnijXLApiTyp
        public static List<XLApiTyp> _Types = new List<XLApiTyp>
        {
            DokumentOfertaSprzedazy, DokumentZamowienieSprzedazy,
            ZlecenieProdukcyjne, ProcesProdukcyjny,
            KontrahentOdbiorca, KontrahentDostawca, KontrahentDostawcoOdbiorca,
            TowarZwykly, TowarProdukt, TowarKoszt, TowarUsluga,
            DokumentFakturaZakupu, DokumentPrzyjecieZewnetrzne, DokumentFakturaSprzedazy, DokumentWydanieZewnetrzne, DokumentRozchodWewnetrzny,
            DokumentPrzychodWewnetrzny, DokumentPrzesuniecieMiedzymagazynowe, DokumentParagon, DokumentSpinaczParagonow, DokumentPrzyjecieKaucji,
            DokumentWydanieKaucji, DokumentFakturaEksportowa, DokumentWydanieZewnetrzneEksportowe, DokumentFakturaWewnetrzna, DokumentRaportSprzedazy,
            DokumentFakturaVATRR, DokumentTaxFree, DokumentPrzyjecieMiedzymagazynowe, DokumentKorektaZbiorczaFakturySprzedazy,
            DokumentKorektaZbiorczaFakturyZakupu, DokumentKorektaZbiorczaFakturyEksportowej
        };
    }

    public class XLApiFatalException : Exception
    {
        public XLApiFatalException() { }
        public XLApiFatalException(string message) : base(message) { }
        public XLApiFatalException(string message, Exception inner) : base(message, inner) { }
    }

    public class XLApiRetryException : Exception
    {
        public XLApiRetryException() { }
        public XLApiRetryException(string message) : base(message) { }
        public XLApiRetryException(string message, Exception inner) : base(message, inner) { }
    }

    public class XLApiNonFatalException : Exception
    {
        public XLApiNonFatalException() { }
        public XLApiNonFatalException(string message) : base(message) { }
        public XLApiNonFatalException(string message, Exception inner) : base(message, inner) { }
    }

    public static class DataICzasXLExtensions
    {
        private const string errMsgToXL = "Wystąpił błąd podczas konwersji daty na format ERP XL.";
        private const string errMsgFromXL = "Wystąpił błąd podczas konwersji daty w formacie ERP XL na format standardowy.";

        public static int ToXLCzas(this DateTime dt)
        {
            return (int)dt.Subtract(new DateTime(1990, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
        }

        public static int ToXLCzas(this DateTime? dt)
        {
            if(dt == null)
                throw new ArgumentNullException(errMsgToXL);

            return ToXLCzas((DateTime) dt);
        }

        public static DateTime ToDateTimeFromXLCzas(this int dt)
        {
            return new DateTime(1990, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(dt);
        }

        public static DateTime ToDateTimeFromXLCzas(this int? dt)
        {
            if (dt == null)
                throw new ArgumentNullException(errMsgFromXL);

            return ToDateTimeFromXLCzas((int) dt);
        }

        public static int ToXLDataInt(this DateTime dt)
        {
            return (int)dt.Date.Subtract(new DateTime(1800, 12, 28, 0, 0, 0, DateTimeKind.Utc)).TotalDays;
        }

        public static int ToXLDataInt(this DateTime? dt)
        {
            if (dt == null)
                throw new ArgumentNullException(errMsgToXL);

            return ToXLDataInt((DateTime) dt);
        }
        public static DateTime ToDateTimeFromXLDataInt(this int dt)
        {
            return new DateTime(1800, 12, 28, 0, 0, 0, DateTimeKind.Utc).AddDays(dt);
        }

        public static DateTime ToDateTimeFromXLDataInt(this int? dt)
        {
            if (dt == null)
                throw new ArgumentNullException(errMsgFromXL);

            return ToDateTimeFromXLDataInt((int) dt);
        }

        public static int ToXLDataLong(this DateTime dt)
        {
            return dt.Date.ToXLCzas();
        }

        public static int ToXLDataLong(this DateTime? dt)
        {
            if (dt == null)
                throw new ArgumentNullException(errMsgToXL);

            return ToXLDataLong((DateTime) dt);
        }

        public static DateTime ToDateTimeFromXLDataLong(this int dt)
        {
            return dt.ToDateTimeFromXLCzas();
        }

        public static DateTime ToDateTimeFromXLDataLong(this int? dt)
        {
            if (dt == null)
                throw new ArgumentNullException(errMsgFromXL);

            return ToDateTimeFromXLDataLong((int) dt);
        }

        public static int ToXLDataIntFromNow(this DateTime dt)
        {
            return (int)dt.Date.Subtract(DateTime.Today).TotalDays;
        }

        public static int ToXLDataIntFromNow(this DateTime? dt)
        {
            if (dt == null)
                throw new ArgumentNullException(errMsgToXL);

            return ToXLDataIntFromNow((DateTime)dt);
        }
    }

}