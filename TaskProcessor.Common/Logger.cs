﻿using System;
using System.Globalization;
using System.IO;
using System.Threading;

namespace TaskProcessor.Common
{
    public static class Logger
    {
        public static void LogMessage(string msg, Guid g, string moduleName)
        {
            var dt = DateTime.Now;

            Console.WriteLine("[{0} {1}] [{2}] {3}", dt.ToShortDateString(), dt.ToString("HH:mm:ss", CultureInfo.InvariantCulture), moduleName, msg);

            string logDir = Helpers.GetLogDir();

            if (!Directory.Exists(logDir))
            {
                try
                {
                    Directory.CreateDirectory(logDir);
                }
                catch
                {
                }
            }
            
            // wykorzystanie globalnego dla systemu muteksu w celu eliminacji bledow dostepu
            // przy zapisie do jednego pliku loga z wielu instancji aplikacji jednoczesnie
            using (var mutex = new Mutex(false, "Global\\TaskProcessorLogMutex"))
            {
                var hasHandle = false;
                try
                {
                    try
                    {
                        hasHandle = mutex.WaitOne(5000, false);
                        if (hasHandle == false)
                        {
                            throw new TimeoutException("Przekroczono czas oczekiwania na możliwość zapisu do pliku loga.");
                        }
                    }
                    catch (AbandonedMutexException)
                    {
                        hasHandle = true;
                    }

                    File.AppendAllText(Path.Combine(logDir, string.Format("TP_log_{0}.txt", DateTime.Today.ToShortDateString())), string.Format("[{0} {1}] [{2}] [{3}] {4}{5}", dt.ToShortDateString(), dt.ToString("HH:mm:ss.fff", CultureInfo.InvariantCulture), g, moduleName, msg, Environment.NewLine));
                }
                finally
                {
                    if (hasHandle)
                    {
                        mutex.ReleaseMutex();
                    }
                }
            }
        }

    }
}