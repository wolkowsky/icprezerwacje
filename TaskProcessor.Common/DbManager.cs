﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading;

namespace TaskProcessor.Common
{
    public static class DbManager
    {
        private static Random Rnd = new Random();

        private const int DefaultSqlExceptionRetryCount = 5;
        private const int DefaultSqlExceptionSleepInterval = 150;

        public static long? CreateTask(Guid plugin, string methodName, string requestObject, string webServerRequestIpAddress = null, string connStr = null, Guid? overrideInstanceGuid = null, int? sqlExceptionRetryCount = null)
        {
            try
            {
                using (var conn = new SqlConnection(connStr ?? Helpers.GetSettingsValue("ConnectionString")))
                {
                    var cmd = new SqlCommand(@"
                        INSERT INTO
                            dbo.NX_TaskProcessor (RequestPlugin, RequestMethodName, RequestObj, RequestInfo, WorkerInstanceGuid, ResponseResultCode, RetryCount, CreationDate, LastModDate)
                        SELECT
                            @RequestPlugin, @RequestMethodName, @RequestObj, '{ ""SqlHostName"": ""' + HOST_NAME() + '"", ""SqlLoggedInUser"": ""' + SUSER_NAME() + '"", ""WebServerRequestIP"": ""' + @RequestInfo + '"" }', @WorkerInstanceGuid, @ResponseResultCode, @RetryCount, @CreationDate, @LastModDate
                        ; SELECT SCOPE_IDENTITY();", conn);

                    var dateTimeNow = DateTime.Now;

                    cmd.Parameters.AddWithValue("@RequestPlugin", plugin);
                    cmd.Parameters.AddWithValue("@RequestMethodName", methodName);
                    cmd.Parameters.AddWithValue("@RequestObj", requestObject);
                    cmd.Parameters.AddWithValue("@RequestInfo", webServerRequestIpAddress ?? "");
                    if (overrideInstanceGuid != null)
                    {
                        cmd.Parameters.AddWithValue("@WorkerInstanceGuid", overrideInstanceGuid);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@WorkerInstanceGuid", DBNull.Value);                        
                    }
                    cmd.Parameters.AddWithValue("@ResponseResultCode", TaskResultCode.Created);
                    cmd.Parameters.AddWithValue("@RetryCount", 0);
                    cmd.Parameters.AddWithValue("@CreationDate", dateTimeNow);
                    cmd.Parameters.AddWithValue("@LastModDate", dateTimeNow);

                    conn.Open();

                    object result = cmd.ExecuteScalar();
                    if (result.GetType() != typeof(DBNull))
                    {
                        long tempRet;
                        if (Int64.TryParse(result.ToString(), out tempRet))
                        {
                            return tempRet;
                        }
                    }
                }

                return null;
            }
            catch (SqlException)
            {
                var sqlExceptionMaxRetryCount = Int32.Parse(Helpers.GetSettingsValue("SqlExceptionRetryCount") ?? DefaultSqlExceptionRetryCount.ToString());
                if (sqlExceptionRetryCount != null && sqlExceptionRetryCount >= sqlExceptionMaxRetryCount)
                {
                    throw;
                }

                var sqlExceptionSleepInterval = Int32.Parse(Helpers.GetSettingsValue("SqlExceptionSleepInterval") ?? DefaultSqlExceptionSleepInterval.ToString());
                Thread.Sleep(sqlExceptionSleepInterval + Rnd.Next(50));

                return CreateTask(plugin, methodName, requestObject, webServerRequestIpAddress, connStr, overrideInstanceGuid, ++sqlExceptionRetryCount);
            }
        }

        public static long? ModifyTask(long taskId, TaskResponseInfo responseObject, Guid? instanceGuid = null, int? taskRetryCount = null, string connStr = null, int? sqlExceptionRetryCount = null)
        {
            try
            {
                using (var conn = new SqlConnection(connStr ?? Helpers.GetSettingsValue("ConnectionString")))
                {
                    var sql = "UPDATE dbo.NX_TaskProcessor WITH (UPDLOCK) SET LastModDate = @LastModDate";
                    sql += ", ResponseResultCode = @ResponseResultCode";
                    sql += ", ResponseObj = @ResponseObj";
                    sql += ", ResponseMsg = @ResponseMsg";
                    if (taskRetryCount != null) sql += ", RetryCount = @RetryCount";
                    if (instanceGuid != null) sql += ", WorkerInstanceGuid = @WorkerInstanceGuid";
                    sql += " WHERE (TaskId = @TaskId)";

                    // warunek dziala wtedy kiedy startujemy task
                    // warunkujemy wykonanie tym ze task nie ma juz zajetego WorkerInstanceGuid lub ma zajety WorkerInstanceGuid ale jego ResponseResultCode = DoRetry
                    if ((instanceGuid != null) && (responseObject.TaskResultCode == TaskResultCode.Started))
                    {
                        sql += string.Format(" AND (WorkerInstanceGuid IS NULL)");
                    }

                    var cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@TaskId", taskId);
                    cmd.Parameters.AddWithValue("@LastModDate", DateTime.Now);
                    cmd.Parameters.AddWithValue("@ResponseResultCode", responseObject.TaskResultCode);
                    cmd.Parameters.AddWithValue("@ResponseObj", responseObject.Obj != null ? Helpers.SerializeJson(responseObject.Obj, 1) : "");
                    cmd.Parameters.AddWithValue("@ResponseMsg", responseObject.Msg ?? "");
                    if (taskRetryCount != null) cmd.Parameters.AddWithValue("@RetryCount", taskRetryCount);
                    if (instanceGuid != null)
                    {
                        if (instanceGuid == Guid.Empty)
                        {
                            cmd.Parameters.AddWithValue("@WorkerInstanceGuid", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@WorkerInstanceGuid", instanceGuid);
                        }
                    }

                    conn.Open();

                    return cmd.ExecuteNonQuery();
                }
            }
            catch (SqlException)
            {
                var sqlExceptionMaxRetryCount = Int32.Parse(Helpers.GetSettingsValue("SqlExceptionRetryCount") ?? DefaultSqlExceptionRetryCount.ToString());
                if (sqlExceptionRetryCount != null && sqlExceptionRetryCount >= sqlExceptionMaxRetryCount)
                {
                    throw;
                }

                var sqlExceptionSleepInterval = Int32.Parse(Helpers.GetSettingsValue("SqlExceptionSleepInterval") ?? DefaultSqlExceptionSleepInterval.ToString());
                Thread.Sleep(sqlExceptionSleepInterval + Rnd.Next(50));

                return ModifyTask(taskId, responseObject, instanceGuid, taskRetryCount, connStr, ++sqlExceptionRetryCount);
            }
        }

        public static IEnumerable<TaskDbEntity> GetTasks(long? filterId = null, Guid? pluginGuid = null, Guid? instanceGuid = null, DateTime? filterLastModDate = null, List<TaskResultCode> resultCodeFilter = null, string connStr = null, int? sqlExceptionRetryCount = null)
        {
            try
            {
                var ret = new List<TaskDbEntity>();

                using (var conn = new SqlConnection(connStr ?? Helpers.GetSettingsValue("ConnectionString")))
                {
                    var sql = @"
                        SELECT
                            TaskId, RequestPlugin, RequestMethodName, RequestObj, RequestInfo, WorkerInstanceGuid, ResponseResultCode, ResponseObj, ResponseMsg, RetryCount, CreationDate, LastModDate
                        FROM
                            dbo.NX_TaskProcessor
                        WHERE
                            (1 = 1)
                    ";
                    if (filterId != null) sql += " AND (TaskId = @TaskId)";
                    if (filterLastModDate != null) sql += " AND (LastModDate >= @LastModDate)";
                    if (pluginGuid != null)
                    {
                        if (pluginGuid == Guid.Empty)
                        {
                            sql += " AND (RequestPlugin IS NULL)";
                        }
                        else
                        {
                            sql += " AND (RequestPlugin = @PluginGuid)";
                        }
                    }
                    if (instanceGuid != null)
                    {
                        if (instanceGuid == Guid.Empty)
                        {
                            sql += " AND (WorkerInstanceGuid IS NULL)";
                        }
                        else
                        {
                            sql += " AND (WorkerInstanceGuid = @WorkerInstanceGuid)";
                        }
                    }
                    if (resultCodeFilter != null && resultCodeFilter.Count > 0)
                    {
                        sql += " AND ((1 = 0)";
                        foreach (var rc in resultCodeFilter)
                        {
                            sql += " OR (ResponseResultCode = " + (int) rc + ")";
                        }
                        sql += ")";
                    }

                    var cmd = new SqlCommand(sql, conn);

                    if (filterId != null) cmd.Parameters.AddWithValue("@TaskId", (int) filterId);
                    if (filterLastModDate != null) cmd.Parameters.AddWithValue("@LastModDate", (DateTime) filterLastModDate);
                    if (instanceGuid != null && instanceGuid != Guid.Empty) cmd.Parameters.AddWithValue("@WorkerInstanceGuid", instanceGuid);
                    if (pluginGuid != null && pluginGuid != Guid.Empty) cmd.Parameters.AddWithValue("@PluginGuid", pluginGuid);

                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ret.Add(ProcessTaskDbEntity(reader));
                        }
                    }
                }

                return ret;

            }
            catch (SqlException)
            {
                var sqlExceptionMaxRetryCount = Int32.Parse(Helpers.GetSettingsValue("SqlExceptionRetryCount") ?? DefaultSqlExceptionRetryCount.ToString());
                if (sqlExceptionRetryCount != null && sqlExceptionRetryCount >= sqlExceptionMaxRetryCount)
                {
                    throw;
                }

                var sqlExceptionSleepInterval = Int32.Parse(Helpers.GetSettingsValue("SqlExceptionSleepInterval") ?? DefaultSqlExceptionSleepInterval.ToString());
                Thread.Sleep(sqlExceptionSleepInterval + Rnd.Next(50));

                return GetTasks(filterId, pluginGuid, instanceGuid, filterLastModDate, resultCodeFilter, connStr, ++sqlExceptionRetryCount);
            }
        }

        public static TaskDbEntity GetAndMarkNewTask(Guid pluginGuid, Guid instanceGuid, string connStr = null, int? sqlExceptionRetryCount = null)
        {
            try
            {
                TaskDbEntity ret = null;

                using (var conn = new SqlConnection(connStr ?? Helpers.GetSettingsValue("ConnectionString")))
                {
                    conn.Open();

                    var sql = @"
                        SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
                            
                        BEGIN TRANSACTION;
                            
                        DECLARE @TaskId INT = ISNULL((
                            SELECT TOP 1
                                TaskId
                            FROM
                                dbo.NX_TaskProcessor
                            WHERE
                                (
                                    (WorkerInstanceGuid IS NULL) AND (RequestPlugin = @PluginGuid) AND (ResponseResultCode IN (@ResultCodeCreated, @ResultCodeFinishedErrorRetry))
                                )
                                OR (
                                    (WorkerInstanceGuid = @WorkerInstanceGuid) AND (RequestPlugin = @PluginGuid) AND (ResponseResultCode = @ResultCodeCreated)
                                )
                        ), -1);

                        UPDATE
	                        dbo.NX_TaskProcessor WITH (UPDLOCK)
                        SET
	                        LastModDate = @LastModDate, ResponseResultCode = @ResultCodeStarted, WorkerInstanceGuid = @WorkerInstanceGuid
                        WHERE
	                        (
                                (
                                    (WorkerInstanceGuid IS NULL) AND (RequestPlugin = @PluginGuid) AND (ResponseResultCode IN (@ResultCodeCreated, @ResultCodeFinishedErrorRetry))
                                )
                                OR (
                                    (WorkerInstanceGuid = @WorkerInstanceGuid) AND (RequestPlugin = @PluginGuid) AND (ResponseResultCode = @ResultCodeCreated)
                                )
                            )
                            AND (TaskId = @TaskId);
                    
                        DECLARE @NumRows INT = (
                            SELECT
	                            COUNT(TaskId)
                            FROM
	                            dbo.NX_TaskProcessor
                            WHERE
	                            (TaskId = @TaskId) AND (LastModDate = @LastModDate) AND (ResponseResultCode = @ResultCodeStarted) AND (WorkerInstanceGuid = @WorkerInstanceGuid)
                        )                    
                    
                        IF @NumRows = 1
                        BEGIN
                            COMMIT TRANSACTION;
                        END
                        ELSE BEGIN
                            ROLLBACK TRANSACTION;
                        END

                        SELECT
	                        TaskId, RequestPlugin, RequestMethodName, RequestObj, RequestInfo, WorkerInstanceGuid, ResponseResultCode, ResponseObj, ResponseMsg, ResponseResultCode, RetryCount, CreationDate, LastModDate
                        FROM
	                        dbo.NX_TaskProcessor
                        WHERE
	                        (TaskId = @TaskId) AND (LastModDate = @LastModDate) AND (ResponseResultCode = @ResultCodeStarted) AND (WorkerInstanceGuid = @WorkerInstanceGuid)
                    ";

                    var cmd = new SqlCommand(sql, conn);

                    cmd.Parameters.AddWithValue("@PluginGuid", pluginGuid);
                    cmd.Parameters.AddWithValue("@ResultCodeCreated", (int) TaskResultCode.Created);
                    cmd.Parameters.AddWithValue("@ResultCodeFinishedErrorRetry", (int) TaskResultCode.DoRetry);
                    cmd.Parameters.AddWithValue("@ResultCodeStarted", (int) TaskResultCode.Started);
                    cmd.Parameters.AddWithValue("@LastModDate", DateTime.Now);
                    cmd.Parameters.AddWithValue("@WorkerInstanceGuid", instanceGuid);

                    using (var reader = cmd.ExecuteReader())
                    {
                        if(reader.Read())
                        {
                            ret = ProcessTaskDbEntity(reader);
                        }
                    }
                }

                return ret;
            }
            catch (SqlException)
            {
                var sqlExceptionMaxRetryCount = Int32.Parse(Helpers.GetSettingsValue("SqlExceptionRetryCount") ?? DefaultSqlExceptionRetryCount.ToString());
                if (sqlExceptionRetryCount != null && sqlExceptionRetryCount >= sqlExceptionMaxRetryCount)
                {
                    throw;
                }

                var sqlExceptionSleepInterval = Int32.Parse(Helpers.GetSettingsValue("SqlExceptionSleepInterval") ?? DefaultSqlExceptionSleepInterval.ToString());
                Thread.Sleep(sqlExceptionSleepInterval + Rnd.Next(50));

                return GetAndMarkNewTask(pluginGuid, instanceGuid, connStr, ++sqlExceptionRetryCount);
            }
        }

        private static TaskDbEntity ProcessTaskDbEntity(SqlDataReader reader)
        {
            var ret = new TaskDbEntity();
            ret.TaskId = Int64.Parse(reader["TaskId"].ToString());
            ret.RequestPlugin = Guid.Parse(reader["RequestPlugin"].ToString());
            ret.RequestMethodName = reader["RequestMethodName"].ToString();
            ret.RequestObj = reader["RequestObj"].ToString();
            ret.WorkerInstanceGuid = reader["WorkerInstanceGuid"] == DBNull.Value ? null : (Guid?)reader["WorkerInstanceGuid"];
            ret.ResponseResultCode = (TaskResultCode)(Int32.Parse(reader["ResponseResultCode"].ToString()));
            ret.ResponseObj = Helpers.DeserializeJson<object>(reader["ResponseObj"].ToString());
            ret.ResponseMsg = reader["ResponseMsg"] == DBNull.Value ? "" : reader["ResponseMsg"].ToString();
            ret.RetryCount = Int32.Parse(reader["RetryCount"].ToString());
            ret.CreationDate = DateTime.Parse(reader["CreationDate"].ToString());
            ret.LastModDate = DateTime.Parse(reader["LastModDate"].ToString());
            return ret;
        }

        public static int PrepareDatabase(string connStr = null)
        {
            int ret;

            try
            {
                using (var conn = new SqlConnection(connStr ?? Helpers.GetSettingsValue("ConnectionString")))
                {
                    conn.Open();

                    var sql = @"
                        IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NX_TaskProcessor]') AND type IN (N'U'))
                        BEGIN

                            CREATE TABLE [dbo].[NX_TaskProcessor](
                                [TaskId] [bigint] IDENTITY(1,1) NOT NULL,
                                [RequestPlugin] [uniqueidentifier] NOT NULL,
                                [RequestMethodName] [varchar](max) NOT NULL,
                                [RequestObj] [varchar](max) NOT NULL,
                                [RequestInfo] [varchar](max) NOT NULL,
                                [WorkerInstanceGuid] [uniqueidentifier] NULL,
                                [ResponseResultCode] [int] NOT NULL,
                                [ResponseObj] [varchar](max) NULL,
                                [ResponseMsg] [varchar](max) NULL,
                                [RetryCount] [int] NOT NULL,
                                [CreationDate] [datetime] NOT NULL,
                                [LastModDate] [datetime] NOT NULL,
                             CONSTRAINT [PK_TaskProcessor] PRIMARY KEY CLUSTERED 
                            ([TaskId] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];

                            CREATE NONCLUSTERED INDEX [IDX_NX_TaskProcessor_1] ON [dbo].[NX_TaskProcessor] ([RequestPlugin] ASC) INCLUDE ([TaskId],	[WorkerInstanceGuid], [ResponseResultCode])
                                WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
                        
                            SELECT 1 AS 'Info'
                    
                        END
                        ELSE BEGIN

                            SELECT 0 AS 'Info'

                        END";

                    var cmd = new SqlCommand(sql, conn);

                    using (var reader = cmd.ExecuteReader())
                    {
                        reader.Read();
                        ret = reader.GetInt32(0);
                    }
                }
            }
            catch (Exception)
            {
                ret = -1;
            }

            return ret;
        }

    }
}