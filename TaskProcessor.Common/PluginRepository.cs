﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using TaskProcessor.Common;

namespace TaskProcessor
{
    public interface ITaskProcessorPlugin
    {
        Guid PluginGuid { get; }
        string PluginName { get; }
        Guid InstanceGuid { get; set; }
        void PreparePlugin();
        void UnloadPlugin();
        void DoBeforeLoop();
        void DoAfterLoop();
        TaskResponseInfo WorkDispatcher(string methodName, string objJson);
    }

    public class PluginRepository
    {
        [ImportMany]
        public List<ITaskProcessorPlugin> Plugins { get; set; }

        public PluginRepository()
        {
            var catalog = new DirectoryCatalog(".");
            using (var container = new CompositionContainer(catalog))
            {
                container.ComposeParts(this);
            }
        }
    }
}