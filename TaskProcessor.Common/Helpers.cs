﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using Newtonsoft.Json;

namespace TaskProcessor.Common
{
    public static class Helpers
    {
        public const string SettingsSwitch = "/settings=";

        public static T DeserializeJson<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static string SerializeJson(object instance, int formatting = (int)Formatting.None, int nvH = (int)NullValueHandling.Ignore)
        {
            return JsonConvert.SerializeObject(instance, (Formatting) formatting, new JsonSerializerSettings { NullValueHandling = (NullValueHandling) nvH });
        }

        public static TaskResponseInfo GetTaskResult(long taskId, int? maxWaitTime = null, int? sleepInterval = null, string connStr = null)
        {
            const int defaultGetTaskResultMaxWaitTime = 20000;
            const int defaultGetTaskResultSleepInterval = 150;

            if (maxWaitTime == null) maxWaitTime = Int32.Parse(GetSettingsValue("GetTaskResultMaxWaitTime") ?? defaultGetTaskResultMaxWaitTime.ToString());
            if (sleepInterval == null) sleepInterval = Int32.Parse(GetSettingsValue("GetTaskResultSleepInterval") ?? defaultGetTaskResultSleepInterval.ToString());

            int waited = 0;

            while (true)
            {
                if (waited < (int)maxWaitTime)
                {
                    var taskList = DbManager.GetTasks(taskId, connStr: connStr).ToList();
                    if (taskList.Any())
                    {
                        var task = taskList.First();
                        if ((new List<TaskResultCode> { TaskResultCode.FinishedOk, TaskResultCode.FinishedWarning, TaskResultCode.FinishedError, TaskResultCode.FinishedErrorAfterRetry, TaskResultCode.FinishedInterrupted }).Contains(task.ResponseResultCode))
                        {
                            return new TaskResponseInfo { Msg = task.ResponseMsg, Obj = task.ResponseObj, TaskResultCode = task.ResponseResultCode };
                        }
                    }
                    else
                    {
                        return new TaskResponseInfo { Msg = string.Format("Nie znaleziono tasku o podanym [TaskId = {0}].", taskId), TaskResultCode = TaskResultCode.FinishedError};   
                    }
                    Thread.Sleep((int)sleepInterval);
                    waited += (int)sleepInterval;
                }
                else
                {
                    return new TaskResponseInfo { Msg = "Przekroczono czas oczekiwania na wynik.", Obj = "", TaskResultCode = TaskResultCode.GetResultTimeout };
                }
            }
        }

        public static TaskResponseInfo GetTaskSnapshot(long taskId, string connStr = null)
        {
            var taskList = DbManager.GetTasks(taskId, connStr: connStr).ToList();
            if (taskList.Any())
            {
                var task = taskList.First();
                return new TaskResponseInfo { Msg = task.ResponseMsg, Obj = task.ResponseObj, TaskResultCode = task.ResponseResultCode };
            }
            return new TaskResponseInfo { Msg = string.Format("Nie znaleziono tasku o podanym [TaskId = {0}].", taskId), TaskResultCode = TaskResultCode.FinishedError };
        }

        public static long CreateTask(Guid plugin, string methodName, string requestObject, string requestInfo = null, string connStr = null, Guid? overrideInstanceGuid = null)
        {
            return (long)DbManager.CreateTask(plugin, methodName, requestObject, requestInfo, connStr, overrideInstanceGuid);
        }

        public static TaskResponseInfo CreateTaskAndWaitForResult(Guid plugin, string methodName, string requestObject, string requestInfo = null, int? maxWaitTime = null, int? sleepInterval = null, string connStr = null)
        {
            long taskId = CreateTask(plugin, methodName, requestObject, requestInfo, connStr);
            return GetTaskResult(taskId, maxWaitTime, sleepInterval, connStr);
        }

        private static string PrepareModuleName(string name)
        {
            return name.Replace("TaskProcessor.AppConsole", "TaskProcessor.Core").Replace("TaskProcessor.Common", "TaskProcessor.Core").Replace(".Common", "");
        }

        public static Dictionary<string, string> GetAllSettings(string branch = null, bool forceRefreshSettings = false)
        {
            if (branch == null)
            {
                branch = Assembly.GetCallingAssembly().GetName().Name;
            }

            if (forceRefreshSettings)
            {
                ConfigurationManager.RefreshSection("appSettings");
            }

            return ConfigurationManager.AppSettings.AllKeys
                .Where(key => key.StartsWith(PrepareModuleName(branch) + "."))
                .Select(key => new { Key = key.Replace(PrepareModuleName(branch) + ".", ""), Value = ConfigurationManager.AppSettings.GetValues(key).First() })
                .ToDictionary(d => d.Key, d => d.Value);
        }

        public static string GetSettingsValue(string key, bool tryCoreIfNotFound = false, string branch = null, bool forceRefreshSettings = false)
        {
            if (branch == null)
            {
                branch = Assembly.GetCallingAssembly().GetName().Name;
            }

            if (forceRefreshSettings)
            {
                ConfigurationManager.RefreshSection("appSettings");                
            }

            var d = new Dictionary<string, string>();
            
            var args = Environment.GetCommandLineArgs();

            if (args.Any(m => m.StartsWith(SettingsSwitch)))
            {
                d = DeserializeJson<Dictionary<string, string>>(args.First(m => m.StartsWith(SettingsSwitch)).Replace(SettingsSwitch, ""));
            }

            var fullKey = branch == "" ? key : PrepareModuleName(branch) + "." + key;

            if (d.ContainsKey(fullKey))
            {
                return d[fullKey];
            }
            else
            {
                if (ConfigurationManager.AppSettings[fullKey] != null)
                {
                    return ConfigurationManager.AppSettings[fullKey];
                }
                else
                {
                    if (tryCoreIfNotFound && (PrepareModuleName(branch) != PrepareModuleName(Assembly.GetExecutingAssembly().GetName().Name)))
                    {
                        return GetSettingsValue(key, branch: Assembly.GetExecutingAssembly().GetName().Name);
                    }
                    else
                    {
                        return null;
                    }
                }
            }

        }

        public static string GetLogDir()
        {
            string dir = GetSettingsValue("LogDirectory");
            if (!string.IsNullOrEmpty(dir))
            {
                if (!Path.IsPathRooted(dir))
                {
                    dir = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), dir);
                }
            }
            else
            {
                dir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "NETRIX");
            }
            return dir;
        }

        public static string GetTempDir()
        {
            string dir = GetSettingsValue("TempDirectory");
            if (!string.IsNullOrEmpty(dir))
            {
                if (!Path.IsPathRooted(dir))
                {
                    dir = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), dir);
                }
            }
            else
            {
                dir = Path.GetTempPath();
            }
            return dir;
        }

    }

}