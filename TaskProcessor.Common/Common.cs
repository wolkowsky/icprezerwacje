﻿using System;
using System.ComponentModel;

namespace TaskProcessor.Common
{
    public class PluginInfo
    {
        private readonly object _workerReadyToCloseMonitor = new object();
        private bool _workerReadyToClose;
        public bool WorkerReadyToClose
        {
            get { lock (_workerReadyToCloseMonitor) { return _workerReadyToClose; } }
            set { lock (_workerReadyToCloseMonitor) { _workerReadyToClose = value; } }
        }

        public Guid PluginGuid { get; set; }
        public string PluginName { get; set; }
        public string AssemblyName { get; set; }
        public Type InstanceType { get; set; }
        public Guid InstanceGuid { get; set; }
        public bool IsInitialized { get; set; }
        public BackgroundWorker Worker { get; set; }
    }

    public class TaskDbEntity
    {
        public long TaskId { get; set; }
        public Guid RequestPlugin { get; set; }
        public string RequestMethodName { get; set; }
        public string RequestObj { get; set; }
        public string RequestInfo { get; set; }
        public Guid? WorkerInstanceGuid { get; set; }
        public TaskResultCode ResponseResultCode { get; set; }
        public object ResponseObj { get; set; }
        public string ResponseMsg { get; set; }
        public int RetryCount { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastModDate { get; set; }
    }

    public enum TaskResultCode
    {
        UnknownResultCode = 0, // bledny, nieuzywany, niezainicjalizowany status
        GetResultTimeout = 1, // zbyt dlugi czas oczekiwania na pobranie wyniku
        Created = 2, // utworzono task
        Started = 3, // task jest przetwarzany
        DoRetry = 4, // wystapil blad, ale task zostanie przetworzony ponownie jesli wartosc RetryCount jest mniejsza lub rowna MaxRetryCount z konfiguracji
        FinishedErrorAfterRetry = 5, // przekroczono ilosc powtorzen, task zakonczyl sie bledem
        FinishedInterrupted = 6, // wykryto zbyt dlugi czas przetwarzania tasku, oznaczono task jako przerwany
        FinishedError = 7, // task zakonczyl sie bledem
        FinishedWarning = 8, // task zakonczyl sie pomyslnie, ale istnieja ostrzezenia w ResponseMsg
        FinishedOk = 9, // task zakonczyl sie sukcesem
    }

    public class TaskResponseInfo
    {
        public TaskResponseInfo()
        {
            Msg = "";
        }
        public TaskResultCode TaskResultCode { get; set; }
        public string Msg { get; set; }
        public object Obj { get; set; }
    }
}