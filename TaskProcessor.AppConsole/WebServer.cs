﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Hosting;
using Newtonsoft.Json;
using Owin;
using TaskProcessor.Common;

namespace TaskProcessor.AppConsole
{
    public class WebServerStartup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/{id}",
                defaults: new { controller = "WebServerInfo", id = RouteParameter.Optional }
            );
            config.Formatters.JsonFormatter.SerializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented, NullValueHandling = NullValueHandling.Ignore };
            appBuilder.UseCors(CorsOptions.AllowAll);
            appBuilder.UseWebApi(config);
        }
    }

    public class WebServer
    {
        public static void StartWebApi()
        {
            try
            {
                var addList = Helpers.GetSettingsValue("WebServerAddressList").Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Select(sValue => sValue.Trim()).ToList();

                var retryCount = addList.Count;
                var success = false;

                while (!success && retryCount > 0)
                {
                    try
                    {
                        StartWebApiInternal(addList[addList.Count - retryCount]);
                        success = true;
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException is HttpListenerException)
                        {
                            Logger.LogMessage(string.Format("Wystąpił błąd przy ustawianiu adresu nasłuchu webserwera [Adres = {0}]. {1}", 
                                addList[addList.Count - retryCount],
                                (retryCount - 1 == 0 ? "Webserwer nie zostanie uruchomiony." : "Sprawdzanie kolejnego adresu z listy konfiguracyjnej.")),
                                Program.AppInstanceGuid, Program.PluginName);

                            retryCount--;
                        }
                        else
                        {
                            throw;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LogMessage(ex.ToString(), Program.AppInstanceGuid, Program.PluginName);
            }
        }

        private static void StartWebApiInternal(string webServerAddress)
        {
            int appSleepTime = Int32.Parse(Helpers.GetSettingsValue("AppSleepTime"));
            using (WebApp.Start<WebServerStartup>(webServerAddress))
            {
                Logger.LogMessage(string.Format("Uruchomiono webserwer [Adres = {0}].", webServerAddress), Program.AppInstanceGuid, Program.PluginName);
                while (true)
                {
                    Thread.Sleep(appSleepTime);
                }
            }
        }

        public static HttpResponseMessage CreateJsonResponse(object obj)
        {
            var c = new StringContent(Helpers.SerializeJson(obj, (int)Formatting.Indented, (int)NullValueHandling.Include), Encoding.UTF8, "application/json");
            c.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/json");
            return new HttpResponseMessage(HttpStatusCode.OK) { Content = c };
        }

        public static string GetIpAddress(HttpRequestMessage req)
        {
            string ipAddress = "?";
            try
            {
                var owinContext = (OwinContext)req.Properties["MS_OwinContext"];
                ipAddress = owinContext.Request.RemoteIpAddress;
            }
            catch
            {
            }
            return ipAddress;
        }

        public static string GetPluginName(Guid plugin)
        {
            var reqPluginQuery = Program.PluginsList.Where(m => m.PluginGuid == plugin).ToList();
            var reqPluginName = string.Format("nieznany plugin ({0})", plugin);
            if (reqPluginQuery.Any())
            {
                reqPluginName = reqPluginQuery.First().PluginName;
            }
            return reqPluginName;
        }
    }

    public class WebServerController : ApiController
    {
        public HttpResponseMessage Post(string mode, string plugin, string method, [FromBody] string objJson)
        {
            string ipAddress = WebServer.GetIpAddress(Request);

            var pluginGuid = Guid.Empty;
            try
            {
                pluginGuid = new Guid(plugin);
            }
            catch
            {
            }

            var taskId = Helpers.CreateTask(pluginGuid, method, objJson, ipAddress);

            Logger.LogMessage(string.Format("Zalogowano żądanie \"{0}\" [NumerIP = {1}, Plugin = {2}, Metoda = {3}, TaskId = {4}].", mode, ipAddress, WebServer.GetPluginName(pluginGuid), method, taskId), Program.AppInstanceGuid, Program.PluginName);

            switch (mode)
            {
                case "CreateTask":
                    {
                        return WebServer.CreateJsonResponse(taskId);
                    }
                case "CreateTaskAndWaitForResult":
                    {
                        var result = Helpers.GetTaskResult(taskId);
                        if (result.Obj == null) result.Obj = "";

                        return WebServer.CreateJsonResponse(result);
                    }
                default:
                    {
                        return WebServer.CreateJsonResponse("Błędny parametr \"mode\".");
                    }
            }
        }

        public HttpResponseMessage Post(string mode, [FromBody] int taskId)
        {
            string ipAddress = WebServer.GetIpAddress(Request);

            Logger.LogMessage(string.Format("Zalogowano żądanie \"{0}\" [NumerIP = {1}, TaskId = {2}].", mode, ipAddress, taskId), Program.AppInstanceGuid, Program.PluginName);

            var result = new TaskResponseInfo();

            switch (mode)
            {
                case "GetTaskResult":
                    {
                        result = Helpers.GetTaskResult(taskId);
                        break;
                    }
                case "GetTaskSnapshot":
                    {
                        result = Helpers.GetTaskSnapshot(taskId);
                        break;
                    }
                default:
                    {
                        return WebServer.CreateJsonResponse("Błędny parametr \"mode\".");
                    }
            }

            if (result.Obj == null) result.Obj = "";

            return WebServer.CreateJsonResponse(result);
        }

    }

    public class WebServerInfoController : ApiController
    {
        public HttpResponseMessage Get()
        {
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent("TaskProcessor WebServer.", Encoding.UTF8, "text/plain")
            };
        }
    }
}
