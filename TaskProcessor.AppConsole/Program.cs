﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using TaskProcessor.Common;

namespace TaskProcessor.AppConsole
{
    class Program
    {
        public static Random Rnd = new Random();

        public static Guid AppInstanceGuid;
        public const string PluginName = "CORE";

        private static PluginRepository PluginsRepo;
        public static List<PluginInfo> PluginsList = new List<PluginInfo>();

        static void Main(string[] args)
        {
            var hr = new HandlerRoutine(ConsoleCtrlCheck);
            GC.KeepAlive(hr);
            SetConsoleCtrlHandler(hr, true);

            var overrideAppInstanceGuid = Helpers.GetSettingsValue("OverrideAppInstanceGuid");
            AppInstanceGuid = string.IsNullOrEmpty(overrideAppInstanceGuid) ? Guid.NewGuid() : Guid.Parse(overrideAppInstanceGuid);

            Logger.LogMessage("Uruchamianie programu...", AppInstanceGuid, PluginName);

            var prep = DbManager.PrepareDatabase();
            switch (prep)
            {
                case 1:
                    Logger.LogMessage("Przygotowano bazę danych do działania programu.", AppInstanceGuid, PluginName);
                    break;
                case -1:
                    Logger.LogMessage("Wystąpił błąd dostępu do bazy danych podczas sprawdzania tabeli roboczej programu.", AppInstanceGuid, PluginName);
                    break;
            }

            //Environment.CurrentDirectory = Helpers.GetSettingsValue("Katalog", branch: "TaskProcessor.Plugin.ERPOptima");

            PluginsRepo = new PluginRepository();

            foreach (var p in PluginsRepo.Plugins.Where(m => Helpers.GetSettingsValue("IsPluginDisabled", false, m.GetType().Assembly.GetName().Name) != "1"))
            {
                // oznaczanie instancji pluginu guidem biezacej aplikacji
                p.InstanceGuid = AppInstanceGuid;

                // tworzenie pomocnicznej listy pluginow wraz z kolejkami
                var plugin = new PluginInfo
                {
                    PluginGuid = p.PluginGuid,
                    PluginName = p.PluginName,
                    AssemblyName = p.GetType().Assembly.GetName().Name,
                    InstanceType = p.GetType(),
                    InstanceGuid = p.InstanceGuid,
                    IsInitialized = false,
                };

                var worker = new BackgroundWorker { WorkerSupportsCancellation = true };
                worker.DoWork += (obj, e) => StartWork(plugin, e);

                plugin.Worker = worker;

                PluginsList.Add(plugin);
            }

            Logger.LogMessage(string.Format("Załadowano pluginy [Ilość = {0}].", PluginsList.Count), AppInstanceGuid, PluginName);

            foreach (var p in PluginsList)
            {
                p.Worker.RunWorkerAsync();
            }

            if (Helpers.GetSettingsValue("IsWebServerEnabled") == "1")
            {
                // tworzenie watku webserwera
                var webServerWorker = new BackgroundWorker { WorkerSupportsCancellation = true };
                webServerWorker.DoWork += (obj, e) => WebServer.StartWebApi();
                webServerWorker.RunWorkerAsync();
            }

            int appSleepTime = Int32.Parse(Helpers.GetSettingsValue("AppSleepTime"));
            while (true) { Thread.Sleep(appSleepTime); }
        }

        private static void StartWork(PluginInfo p, DoWorkEventArgs e)
        {
            Logger.LogMessage(string.Format("Uruchomiono wątek przetwarzania pluginu [{0}].", p.PluginName), p.InstanceGuid, p.PluginName);

            int mainLoopSleepTime = Int32.Parse(Helpers.GetSettingsValue("MainLoopSleepTime", true, p.AssemblyName));

            try
            {
                // glowna petla
                while (true)
                {
                    try
                    {
                        if (p.Worker.CancellationPending)
                        {
                            e.Cancel = true;
                            break;
                        }

                        if (!p.IsInitialized)
                        {
                            // plugin jest ladowany po raz pierwszy - wywolujemy jego metode PreparePlugin
                            Logger.LogMessage(string.Format("Przygotowanie do pierwszego uruchomienia pluginu [{0}].", p.PluginName), p.InstanceGuid, p.PluginName);
                            p.InstanceType.GetMethod("PreparePlugin").Invoke(PluginsRepo.Plugins.Single(m => m.PluginGuid == p.PluginGuid), null);
                            p.IsInitialized = true;
                            Logger.LogMessage(string.Format("Plugin [{0}] został włączony.", p.PluginName), p.InstanceGuid, p.PluginName);                            
                        }

                        // oczekiwanie losowego czasu (maks. 20 ms) dla zapobiezenia hipotetycznej
                        // synchronizacji kolejek wielu kopii tego samego pluginu (mozliwy kierunek rozbudowy)
                        Thread.Sleep(TimeSpan.FromMilliseconds(Rnd.Next(20)));

                        p.InstanceType.GetMethod("DoBeforeLoop").Invoke(PluginsRepo.Plugins.Single(m => m.PluginGuid == p.PluginGuid), null);
                        
                        DoWork(p);
                        
                        p.InstanceType.GetMethod("DoAfterLoop").Invoke(PluginsRepo.Plugins.Single(m => m.PluginGuid == p.PluginGuid), null);
                    }
                    catch (Exception ex)
                    {
                        if (ex is TargetInvocationException && ex.InnerException != null)
                        {
                            ex = ex.InnerException;
                        }
                        // InvalidOperationException -> timeout, blad dostepu do bazy
                        string err = ex is InvalidOperationException ? "Wystąpił błąd dostępu do bazy danych. " + ex.Message : ex.ToString();
                        Logger.LogMessage(err, p.InstanceGuid, p.PluginName);
                    }
                    Thread.Sleep(TimeSpan.FromMilliseconds(mainLoopSleepTime));
                }

                // wylaczamy uzyty plugin
                if (p.IsInitialized)
                {
                    Logger.LogMessage(string.Format("Wyłączanie pluginu [{0}].", p.PluginName), p.InstanceGuid, p.PluginName);
                    p.InstanceType.GetMethod("UnloadPlugin").Invoke(PluginsRepo.Plugins.Single(m => m.PluginGuid == p.PluginGuid), null);
                    Logger.LogMessage(string.Format("Plugin [{0}] został wyłączony.", p.PluginName), p.InstanceGuid, p.PluginName);
                }
            }
            catch (Exception ex)
            {
                if (ex is TargetInvocationException && ex.InnerException != null)
                {
                    ex = ex.InnerException;
                }
                Logger.LogMessage(ex.ToString(), p.InstanceGuid, p.PluginName);
            }

            p.WorkerReadyToClose = true;
        }

        private static void DoWork(PluginInfo p)
        {
            // robimy porzadki w taskach ktore z jakis powodow nie zakonczyly sie po uruchomieniu lub potencjalnie przeterminowane
            var oldTasks = DbManager.GetTasks(pluginGuid: p.PluginGuid, resultCodeFilter: new List<TaskResultCode> { TaskResultCode.Started, TaskResultCode.Created });

            int oldTaskDuration = Int32.Parse(Helpers.GetSettingsValue("OldTaskDuration", true, p.AssemblyName));

            foreach (var task in oldTasks)
            {
                // jesli czas pracy nad taskiem przekracza maksimum to flagujemy task jako zakonczony z informacja o bledzie
                if (task.LastModDate.AddMilliseconds(oldTaskDuration) <= DateTime.Now)
                {
                    DbManager.ModifyTask(task.TaskId, new TaskResponseInfo { Msg = "Zbyt długi czas przetwarzania.", TaskResultCode = TaskResultCode.FinishedInterrupted }, p.InstanceGuid);
                    Logger.LogMessage(string.Format("Oznaczanie [TaskId = {0}] jako przerwany.", task.TaskId), p.InstanceGuid, p.PluginName);
                }
            }

            var t = DbManager.GetAndMarkNewTask(p.PluginGuid, p.InstanceGuid);
            if (t != null)
            {
                Logger.LogMessage(string.Format("Rozpoczęto przetwarzanie [TaskId = {0}], [Method = {1}].", t.TaskId, t.RequestMethodName), p.InstanceGuid, p.PluginName);

                // wykonujemy prace na tasku
                var methodResultObject = InvokeStandarizedMethod(p.PluginGuid.ToString(), t.RequestMethodName, t.RequestObj);

                Logger.LogMessage(string.Format("Zakończono przetwarzanie [TaskId = {0}], [ResultCode = {1}]", t.TaskId, methodResultObject.TaskResultCode) + (string.IsNullOrEmpty(methodResultObject.Msg) ? "." : string.Format(", [Msg = {0}].", methodResultObject.Msg)), p.InstanceGuid, p.PluginName);

                if (methodResultObject.TaskResultCode == TaskResultCode.DoRetry)
                {
                    int maxRetryCount = Int32.Parse(Helpers.GetSettingsValue("MaxRetryCount", true, p.AssemblyName));

                    // jesli task zakonczyl sie bledem i ma opcje ponownej powtorki to sprawdzamy czy ilosc prob powtorzen nie przekroczyla maksimum
                    if (t.RetryCount >= maxRetryCount)
                    {
                        DbManager.ModifyTask(t.TaskId, new TaskResponseInfo { Msg = "Przekroczono ilość prób wykonania." + (string.IsNullOrEmpty(t.ResponseMsg) ? "" : " " + t.ResponseMsg), TaskResultCode = TaskResultCode.FinishedErrorAfterRetry }, p.InstanceGuid);
                        Logger.LogMessage(string.Format("Ilość prób dla [TaskId = {0}] przekroczyła maksimum ({1}), [ResultCode = {2}].", t.TaskId, maxRetryCount, TaskResultCode.FinishedError), p.InstanceGuid, p.PluginName);
                    }
                    else
                    {
                        DbManager.ModifyTask(t.TaskId, methodResultObject, Guid.Empty, t.RetryCount + 1);
                    }
                }
                else
                {
                    DbManager.ModifyTask(t.TaskId, methodResultObject, p.InstanceGuid);
                }
            }
        }

        private static void FinishWorkAndExit()
        {
            Logger.LogMessage("Otrzymano informację o końcu pracy.", AppInstanceGuid, PluginName);

            foreach (var p in PluginsList)
            {
                p.Worker.CancelAsync();
            }

            int maxWaitTimeForExit = Int32.Parse(Helpers.GetSettingsValue("MaxWaitTimeForExit"));
            int workersCloseSleepTime = Int32.Parse(Helpers.GetSettingsValue("WorkersCloseSleepTime"));

            int waited = 0;
            while (PluginsList.Any(m => !m.WorkerReadyToClose))
            {
                if (waited >= maxWaitTimeForExit)
                {
                    break;
                }
                Thread.Sleep(workersCloseSleepTime);
                waited += workersCloseSleepTime;
            }

            // jesli czekanie na wylaczenie pluginow nie pomoglo to wychodzimy na ostro, przez bledy z ClaRUN.dll
            if (waited >= maxWaitTimeForExit)
            {
                Logger.LogMessage("Zamykanie programu na ostro.", AppInstanceGuid, PluginName);
                Process.GetCurrentProcess().Kill();
            }

            Logger.LogMessage("Zamykanie programu.", AppInstanceGuid, PluginName);

            Environment.Exit(0);
        }

        public static TaskResponseInfo InvokeStandarizedMethod(string plugin, string method, string objJson)
        {
            var ret = new TaskResponseInfo();

            try
            {
                var p = PluginsRepo.Plugins.Count(m => m.PluginGuid == new Guid(plugin));
                if (p != 1) throw new Exception("Nieprawidłowy parametr [plugin].");

                var type = PluginsRepo.Plugins.Single(m => m.PluginGuid == new Guid(plugin)).GetType();

                //var methodObj = type.GetMethod(method);
                //if (methodObj == null) throw new Exception("Nieprawidłowy parametr [method].");

                ret = (TaskResponseInfo)type.GetMethod("WorkDispatcher").Invoke(PluginsRepo.Plugins.Single(m => m.PluginGuid == new Guid(plugin)), new object[] { method, objJson });
            }
            catch (Exception ex)
            {
                ret.TaskResultCode = TaskResultCode.FinishedError;
                ret.Msg = ex.Message;
            }

            return ret;
        }

        /*
        public static object InvokeCustomMethod(string plugin, string method, object[] parameters)
        {
            var type = PluginsRepo.Plugins.Single(m => m.PluginGuid == new Guid(plugin)).GetType();
            var methodObj = type.GetMethod(method);
            var parTypes = methodObj.GetParameters().Select(m => m.ParameterType).ToList();
            var parOptionalInfo = methodObj.GetParameters().Select(m => m.IsOptional).ToList();
            var refPars = new List<object>();
            for (int i = 0; i < parTypes.Count(); i++)
            {
                if (i < parameters.Count())
                {
                    refPars.Add(Convert.ChangeType(parameters[i], parTypes[i]));
                }
                else
                {
                    if (parOptionalInfo[i])
                    {
                        refPars.Add(Type.Missing);
                    }
                }
            }
            return methodObj.Invoke(PluginsRepo.Plugins.Single(m => m.PluginGuid == new Guid(plugin)), refPars.ToArray());
        }
        */

        private static bool ConsoleCtrlCheck(CtrlTypes ctrlType)
        {
            bool isAppClosing = false;

            switch (ctrlType)
            {
                case CtrlTypes.CTRL_C_EVENT:
                    Logger.LogMessage("Otrzymano sygnał CTRL+C.", AppInstanceGuid, PluginName);
                    isAppClosing = true;
                    break;
                case CtrlTypes.CTRL_BREAK_EVENT:
                    Logger.LogMessage("Otrzymano sygnał CTRL+BREAK.", AppInstanceGuid, PluginName);
                    isAppClosing = true;
                    break;
                case CtrlTypes.CTRL_CLOSE_EVENT:
                    Logger.LogMessage("Otrzymano sygnał zamykania aplikacji.", AppInstanceGuid, PluginName);
                    isAppClosing = true;
                    break;
                case CtrlTypes.CTRL_LOGOFF_EVENT:
                    Logger.LogMessage("Otrzymano sygnał wylogowania użytkownika.", AppInstanceGuid, PluginName);
                    // isAppClosing = true;
                    break;
                case CtrlTypes.CTRL_SHUTDOWN_EVENT:
                    Logger.LogMessage("Otrzymano sygnał zamykania systemu.", AppInstanceGuid, PluginName);
                    isAppClosing = true;
                    break;
            }

            if (isAppClosing)
            {
                FinishWorkAndExit();
            }

            return true;
        }

        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(HandlerRoutine Handler, bool Add);
        private delegate bool HandlerRoutine(CtrlTypes CtrlType);

        private enum CtrlTypes
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT,
            CTRL_CLOSE_EVENT,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT
        }

    }
}
